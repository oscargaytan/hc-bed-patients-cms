import { Subscription } from 'rxjs';
// Angular
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
// Layout
import { LayoutConfigService, SplashScreenService, TranslationService } from './core/_base/layout';
// language list
import { locale as enLang } from './config/i18n/en';
import { locale as chLang } from './config/i18n/ch';
import { locale as esLang } from './config/i18n/es';
import { locale as jpLang } from './config/i18n/jp';
import { locale as deLang } from './config/i18n/de';
import { locale as frLang } from './config/i18n/fr';
import { NgxPermissionsService } from 'ngx-permissions';
import {NotificationsService} from 'angular2-notifications';

import { SwPush, SwUpdate } from '@angular/service-worker';
import { PushNotificationService } from './push-notification.service';
import { PushNotificationsService } from 'ng-push';
import { MatSnackBar} from '@angular/material/snack-bar';
import { SnackbarComponent } from './views/partials/snackbar/snackbar.component';
import { WebSocketService } from './web-socket.service'
//import { NotifierService } from "angular-notifier";

const VAPID_PUBLIC = 'BKUpwkXa1uQtk8dLVHSvXcAPEGuBEBqYzTCd45GqvgRAPtDcgeDWQ3SMdLU7d2QR3ZlhrXtrsG_QlGKeZ44nwDQ';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'body[kt-root]',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit, OnDestroy {
	// Public properties
	title = 'Metronic';
	loader: boolean;
	//private readonly notifier: NotifierService;
	private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	/**
	 * Component constructor
	 *
	 * @param translationService: TranslationService
	 * @param router: Router
	 * @param layoutConfigService: LayoutCongifService
	 * @param splashScreenService: SplashScreenService
	 */
	constructor(private translationService: TranslationService,
				private router: Router,
				private layoutConfigService: LayoutConfigService,
				private service: NotificationsService,
				private splashScreenService: SplashScreenService,
				update: SwUpdate,
				swPush: SwPush,
				pushService: PushNotificationService,
				public snackbar: MatSnackBar,
				private webSocketService: WebSocketService,
				private _pushNotifications: PushNotificationsService
				) {
					this._pushNotifications.requestPermission();
					// this.notifier = notifierService;
					// this.notifier.notify("success", "You are awesome! I mean it!");
					

			if(swPush.isEnabled){
				swPush
					.requestSubscription({
						serverPublicKey: VAPID_PUBLIC,
					})
					.then(subscription =>{
						pushService.sendSubscriptionToTheServer(subscription).subscribe();
					})
					.catch(console.error)
			}

		// register translations
		//this.translationService.loadTranslations(enLang, chLang, esLang, jpLang, deLang, frLang);
		this.translationService.loadTranslations(enLang, esLang);

		// retrieve browser language
		if (localStorage.getItem('languagePref') === null) {
			try {
				const language = window.navigator.language.split('-');
				localStorage.setItem('language', language[0]);
			} catch (error) {
				console.log(error);
			}
		}
	}

	

	  open(){
		// let snackBarRef = this.snackbar.open("Nueva consulta agregada", "Ir a consultorio", {
		//   duration: 3000,
		//   panelClass: ['green-snackbar']
		// });
		// snackBarRef.onAction().subscribe(() => {
		//   console.log('The snack-bar action was triggered!');
		//   this.router.navigate(['/consultations']); 
		// });
		// console.log("123123")


		let snackBarRef = this.snackbar.openFromComponent(SnackbarComponent, {
			duration: 3000,
			panelClass: ['green-snackbar']
		});
	  }
	

	 

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		
		this.webSocketService.listen('sendNotification').subscribe((data)=>{
			console.log(data);
			this.open();

		});

		//this.webSocketService.emit('sendToServer', 'notificacion enviada');
		
		// enable/disable loader
		this.loader = this.layoutConfigService.getConfig('loader.enabled');

		const routerSubscription = this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				// hide splash screen
				this.splashScreenService.hide();

				// scroll to top on every route change
				window.scrollTo(0, 0);

				// to display back the body content
				setTimeout(() => {
					document.body.classList.add('kt-page--loaded');
				}, 500);
			}
		});
		this.unsubscribe.push(routerSubscription);
	}
	

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.unsubscribe.forEach(sb => sb.unsubscribe());
	}
}

