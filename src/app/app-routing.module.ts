// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  /** Enrutamiento principal para acceso y navegación en cms */
  { path: 'auth', loadChildren: './views/pages/auth/auth.module#AuthModule' },
  { path: '', loadChildren: './views/themes/theme.module#ThemeModule' },

  { path: '**', redirectTo: 'error/403', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
