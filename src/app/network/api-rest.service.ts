import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { ResponseModule } from '../models/response/response.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';
import { UserModule } from '../models/user/user.module';
import { AdminUserModule } from '../models/admin-user/admin-user.module';
import { ProjectStatusModule } from '../models/project-status/project-status.module';
import { ProjectCategoryModule } from '../models/project-category/project-category.module';
import { ProjectSectorModule } from '../models/project-sector/project-sector.module';
import { ProjectMarketModule } from '../models/project-market/project-market.module';
import { AddNewModule } from '../models/add-new/add-new.module';
import { PlanModule } from '../models/plan/plan.module';
import { SpecialtiesModule } from '../models/specialties/specialties.module';
import { MedicalCentersModule } from '../models/medical-centers/medical-centers.module';
import { AddEventModule } from '../models/add-event/add-event.module';
import { AddProjectRequestModule } from '../models/add-project-request/add-project-request.module';


const API_ENDPOINT_URL = environment.endpoint;
const API_DASHBOARD_PATH = 'dashboard';
const API_ADMIN_USERS_PATH = 'admin/users';
const API_USERS_PATH = 'users';
const API_DOCTORS_PATH = 'doctors';
const API_CATALOGS = 'catalogs';
const API_NEWS_PATH = 'news';
const API_EVENTS_PATH = 'events';
const API_PROJECTS = 'projects';
const API_PLANS = 'plans';
const API_SPECIALTIES = 'specialties';
const API_MEDICAL_CENTERS = 'medical_centers';
const API_PATIENTS = "patients";
const API_CONSULTATION = "consultations";
const API_CREATE = "create_consultation"
const API_DOCTORS2 = 'doctors2';
const API_INVESTMENT_REQUESTS = 'project/requests';
const API_CATALOGS_PROJECTS = API_CATALOGS + '/project';
const API_CATALOGS_PROJECTS_STATUS = API_CATALOGS_PROJECTS + '/status';
const API_CATALOGS_PROJECTS_CATEGORY = API_CATALOGS_PROJECTS + '/category';
const API_CATALOGS_PROJECTS_SECTOR = API_CATALOGS_PROJECTS + '/sector';
const API_CATALOGS_PROJECTS_MARKET = API_CATALOGS_PROJECTS + '/market';

export const TOKEN_NAME = 'aToken';

@Injectable({
  providedIn: 'root'
})
export class ApiRestService {

  private httpOptions = {};
  private xhrOptions = {};
  private router: Router;
  constructor(private http: HttpClient, private _router: Router) {
    this.router = this._router;
  }

  private setHeaders() {
    let openHeaders = new HttpHeaders();
    openHeaders = openHeaders.append('Content-Type', 'application/json');
    openHeaders = openHeaders.append('Access-Control-Allow-Origin', '*');
    if (this.getToken() !== null) {
      openHeaders = openHeaders.append('Authorization', this.getToken());
    }
    // openHeaders = openHeaders.append('Corporate', environment.corporate);
    this.httpOptions = {
      headers: openHeaders
    };
  }

  private setXhrHeaders() {
    const openHeaders = {};
    // openHeaders['Content-Type'] = 'multipart/form-data';
    openHeaders['Access-Control-Allow-Origin'] = '*';
    if (this.getToken() !== null) {
      openHeaders["Authorization"] = this.getToken();
    }
    // openHeaders = openHeaders.append('Corporate', environment.corporate);
    this.xhrOptions = openHeaders;
  }

  private extractData(res: ResponseModule) {
    const body = res;
    if (res.statusCode === 4) {
      alert(res.statusMessage);
      localStorage.clear();
      this.router.navigate(['/']);
    } else {
      return body;
    }
  }

  // JWT helpers usage
  getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }
  getDataToken() {
    const decoded = jwt_decode(localStorage.getItem(TOKEN_NAME));
    return decoded;
  }
  setToken(token: string): void {
    localStorage.setItem(TOKEN_NAME, token);
  }
  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);
    if (decoded.exp === undefined) { return null; }
    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }
  isTokenExpired(token?: string): boolean {
    if (!token) { token = this.getToken(); }
    if (!token || token === undefined) { return true; }

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) { return false; }
    return !(date.valueOf() > new Date().valueOf());
  }

  // Authentication/Authorization
  login(email: string, pass: string): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + 'login', { email, pass }, this.httpOptions).pipe(map(this.extractData));
  }

  // Authentication/Authorization
  recoveryPassword(email: string): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + 'recovery', { email }, this.httpOptions).pipe(map(this.extractData));
  }


  // Dashboard Module Requests
  getDashboardUsersInfo(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_DASHBOARD_PATH + '/users', this.httpOptions).pipe(map(this.extractData));
  }


  // Users Module Requests
  getUsers(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_USERS_PATH, this.httpOptions).pipe(map(this.extractData));
  }
  getUser(userID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_USERS_PATH + '/' + userID, this.httpOptions).pipe(map(this.extractData));
  }
  addUser(userName: string, userLastName: string, userEmail: string, userType: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_USERS_PATH + '/add', { userName, userLastName, userEmail, userType }, this.httpOptions).pipe(map(this.extractData));
  }
  editUser(user: UserModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_USERS_PATH + '/edit', user, this.httpOptions).pipe(map(this.extractData));
  }
  editUser2(user: UserModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_USERS_PATH + '/edit2', user, this.httpOptions).pipe(map(this.extractData));
  }
  deleteUser(userID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_USERS_PATH + '/delete', { userID }, this.httpOptions).pipe(map(this.extractData));
  }
  enableUser(userID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_USERS_PATH + '/enable', { userID }, this.httpOptions).pipe(map(this.extractData));
  }
  disableUser(userID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_USERS_PATH + '/disable', { userID }, this.httpOptions).pipe(map(this.extractData));
  }
  editUserPass(userID: number,userOldPassword: string, userPassword: string): Observable<any> {
    
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_USERS_PATH + '/editPass', { userID, userOldPassword, userPassword }, this.httpOptions).pipe(map(this.extractData));
  }

  // Doctors Module Requests
  getDoctors(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_DOCTORS_PATH, this.httpOptions).pipe(map(this.extractData));
  }
  getDoctor(userID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_DOCTORS_PATH + '/' + userID, this.httpOptions).pipe(map(this.extractData));
  }
  // addDoctor(userName: string, userLastName: string, userEmail: string, userType: string, userSpecialty:number): Observable<any> {
  addDoctor(data:any): Observable<any> {
    this.setHeaders();
    //console.log("+++++++++"+userName, userLastName, userEmail, userSpecialty);
    // return this.http.post(API_ENDPOINT_URL + API_DOCTORS_PATH + '/add', { userName, userLastName, userEmail, userType, userSpecialty }, this.httpOptions).pipe(map(this.extractData));
    return this.http.post(API_ENDPOINT_URL + API_DOCTORS_PATH + '/add', data, this.httpOptions).pipe(map(this.extractData));
  }
  editDoctor(data: any): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_DOCTORS_PATH + '/edit', data, this.httpOptions).pipe(map(this.extractData));
  }
  deleteDoctor(userID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_DOCTORS_PATH + '/delete', { userID }, this.httpOptions).pipe(map(this.extractData));
  }
  enableDoctor(userID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_DOCTORS_PATH + '/enable', { userID }, this.httpOptions).pipe(map(this.extractData));
  }
  disableDoctor(userID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_DOCTORS_PATH + '/disable', { userID }, this.httpOptions).pipe(map(this.extractData));
  }


  // Catalogs Status Module Requests
  getAllStatus(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_STATUS, this.httpOptions).pipe(map(this.extractData));
  }
  getStatus(statusID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_STATUS + '/' + statusID, this.httpOptions).pipe(map(this.extractData));
  }
  addStatus(proyectStatus: ProjectStatusModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_STATUS + '/add', proyectStatus, this.httpOptions).pipe(map(this.extractData));
  }
  editStatus(proyectStatus: ProjectStatusModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_STATUS + '/edit', proyectStatus, this.httpOptions).pipe(map(this.extractData));
  }
  deleteStatus(statusID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_STATUS + '/delete', { statusID }, this.httpOptions).pipe(map(this.extractData));
  }

  // Catalogs Categories Module Requests
  getAllCategories(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_CATEGORY, this.httpOptions).pipe(map(this.extractData));
  }
  getCategory(categoryID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_CATEGORY + '/' + categoryID, this.httpOptions).pipe(map(this.extractData));
  }
  addCategory(proyectCategory: ProjectCategoryModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_CATEGORY + '/add', proyectCategory, this.httpOptions).pipe(map(this.extractData));
  }
  editCategory(proyectCategory: ProjectCategoryModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_CATEGORY + '/edit', proyectCategory, this.httpOptions).pipe(map(this.extractData));
  }
  deleteCategory(categoryID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_CATEGORY + '/delete', { categoryID }, this.httpOptions).pipe(map(this.extractData));
  }

  // Catalogs Sectors Module Requests
  getAllSectors(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_SECTOR, this.httpOptions).pipe(map(this.extractData));
  }
  getSector(sectorID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_SECTOR + '/' + sectorID, this.httpOptions).pipe(map(this.extractData));
  }
  addSector(proyectSector: ProjectSectorModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_SECTOR + '/add', proyectSector, this.httpOptions).pipe(map(this.extractData));
  }
  editSector(proyectSector: ProjectSectorModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_SECTOR + '/edit', proyectSector, this.httpOptions).pipe(map(this.extractData));
  }
  deleteSector(sectorID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_SECTOR + '/delete', { sectorID }, this.httpOptions).pipe(map(this.extractData));
  }


  // Catalogs Markets Module Requests
  getAllMarkets(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_MARKET, this.httpOptions).pipe(map(this.extractData));
  }
  getMarket(marketID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_MARKET + '/' + marketID, this.httpOptions).pipe(map(this.extractData));
  }
  addMarket(proyectMarket: ProjectMarketModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_MARKET + '/add', proyectMarket, this.httpOptions).pipe(map(this.extractData));
  }
  editMarket(proyectMarket: ProjectMarketModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_MARKET + '/edit', proyectMarket, this.httpOptions).pipe(map(this.extractData));
  }
  deleteMarket(marketID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_CATALOGS_PROJECTS_MARKET + '/delete', { marketID }, this.httpOptions).pipe(map(this.extractData));
  }


  // News Module Requests
  getApprovedNews(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_NEWS_PATH + '/approved', this.httpOptions).pipe(map(this.extractData));
  }
  getAllNews(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_NEWS_PATH, this.httpOptions).pipe(map(this.extractData));
  }
  getNewDetail(newID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_NEWS_PATH + '/' + newID, this.httpOptions).pipe(map(this.extractData));
  }
  publishNew(newID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_NEWS_PATH + '/approve/' + newID, this.httpOptions).pipe(map(this.extractData));
  }
  removeNew(newID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_NEWS_PATH + '/remove/' + newID, this.httpOptions).pipe(map(this.extractData));
  }
  addNew(newData: AddNewModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_NEWS_PATH + '/add', newData, this.httpOptions).pipe(map(this.extractData));
  }
  deleteNew(newID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_NEWS_PATH + '/delete', { newID }, this.httpOptions).pipe(map(this.extractData));
  }

  // Events Module Requests
  getApprovedEvents(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_EVENTS_PATH + '/approved', this.httpOptions).pipe(map(this.extractData));
  }
  getAllEvents(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_EVENTS_PATH, this.httpOptions).pipe(map(this.extractData));
  }
  getEventDetail(eventID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_EVENTS_PATH + '/' + eventID, this.httpOptions).pipe(map(this.extractData));
  }
  publishEvent(eventID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_EVENTS_PATH + '/approve/' + eventID, this.httpOptions).pipe(map(this.extractData));
  }
  removeEvent(eventID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_EVENTS_PATH + '/remove/' + eventID, this.httpOptions).pipe(map(this.extractData));
  }
  addEvent(eventData: AddEventModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_EVENTS_PATH + '/add', eventData, this.httpOptions).pipe(map(this.extractData));
  }
  deleteEvent(eventID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_EVENTS_PATH + '/delete', { eventID }, this.httpOptions).pipe(map(this.extractData));
  }

  // Projects Module Requests
  getAllProjects(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_PROJECTS, this.httpOptions).pipe(map(this.extractData));
  }
  getProject(projectID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_PROJECTS + '/' + projectID, this.httpOptions).pipe(map(this.extractData));
  }

  // Investment Requests Module Requests
  getAllRequests(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_INVESTMENT_REQUESTS, this.httpOptions).pipe(map(this.extractData));
  }
  getRequest(requestID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_INVESTMENT_REQUESTS + '/' + requestID, this.httpOptions).pipe(map(this.extractData));
  }
  approveRequest(projectRequest: AddProjectRequestModule): Observable<any> {
    this.setHeaders();
    console.log(projectRequest);
    return this.http.post(API_ENDPOINT_URL + API_INVESTMENT_REQUESTS + '/approve', projectRequest, this.httpOptions).pipe(map(this.extractData));
  }
  denyRequest(projectRequest: AddProjectRequestModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_INVESTMENT_REQUESTS + '/deny', projectRequest, this.httpOptions).pipe(map(this.extractData));
  }

  // Investment Requests Module Requests
  getAllPlans(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_PLANS, this.httpOptions).pipe(map(this.extractData));
  }
  getPlan(planID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_PLANS + '/' + planID, this.httpOptions).pipe(map(this.extractData));
  }
  addPlan(planData: PlanModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_PLANS + '/add', planData, this.httpOptions).pipe(map(this.extractData));
  }

  // Investment Requests Module Requests
  getAllSpecialties(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_SPECIALTIES, this.httpOptions).pipe(map(this.extractData));
  }
  getSpecialty(specialtyID: number): Observable<any> {
    this.setHeaders();
    console.log(API_ENDPOINT_URL + API_SPECIALTIES + '/' + specialtyID)
    return this.http.get(API_ENDPOINT_URL + API_SPECIALTIES + '/' + specialtyID, this.httpOptions).pipe(map(this.extractData));
  }
  addSpecialty(specialtyName: string): Observable<any> {
    
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_SPECIALTIES + '/add', {specialtyName}, this.httpOptions).pipe(map(this.extractData));
  }
  editSpecialty(specialty: SpecialtiesModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_SPECIALTIES + '/edit', specialty, this.httpOptions).pipe(map(this.extractData));
  }
  disableSpecialty(specialtyID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_SPECIALTIES + '/disable', { specialtyID }, this.httpOptions).pipe(map(this.extractData));
  }
  enableSpecialty(specialtyID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_SPECIALTIES + '/enable', { specialtyID }, this.httpOptions).pipe(map(this.extractData));
  }
  deleteSpecialty(specialtyID: number): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_SPECIALTIES + '/delete', { specialtyID }, this.httpOptions).pipe(map(this.extractData));
  }

   // Investment Requests Module Requests
   getAllDoctors2(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_DOCTORS2, this.httpOptions).pipe(map(this.extractData));
  }
  getDoctorsMedCenters(): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + "doctors-medcenters", this.httpOptions).pipe(map(this.extractData));
  }
  getDoctor2(doctor2ID: number): Observable<any> {
    this.setHeaders();
    return this.http.get(API_ENDPOINT_URL + API_DOCTORS2 + '/' + doctor2ID, this.httpOptions).pipe(map(this.extractData));
  }
  addDoctor2(doctor2Data: PlanModule): Observable<any> {
    this.setHeaders();
    return this.http.post(API_ENDPOINT_URL + API_DOCTORS2 + '/add', doctor2Data, this.httpOptions).pipe(map(this.extractData));
  }


//Medical Centers

getAllMedicalCenters(): Observable<any> {
  this.setHeaders();
  return this.http.get(API_ENDPOINT_URL + API_MEDICAL_CENTERS, this.httpOptions).pipe(map(this.extractData));
}
getMedicalCenter(medicalCenterID: number): Observable<any> {
  this.setHeaders();
  console.log(API_ENDPOINT_URL + API_MEDICAL_CENTERS + '/' + medicalCenterID)
  return this.http.get(API_ENDPOINT_URL + API_MEDICAL_CENTERS + '/' + medicalCenterID, this.httpOptions).pipe(map(this.extractData));
}
addMedicalCenter(medicalCenterName: string, medicalCenterAddress: string, medicalCenterPhone: number): Observable<any> {
    
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_MEDICAL_CENTERS + '/add', {medicalCenterName, medicalCenterAddress, medicalCenterPhone}, this.httpOptions).pipe(map(this.extractData));
}
editMedicalCenter(medicalCenter: MedicalCentersModule): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_MEDICAL_CENTERS + '/edit', medicalCenter, this.httpOptions).pipe(map(this.extractData));
}
disableMedicalCenter(medicalCenterID: number): Observable<any> {
  console.log(medicalCenterID);
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_MEDICAL_CENTERS + '/disable', { medicalCenterID }, this.httpOptions).pipe(map(this.extractData));
}
enableMedicalCenter(medicalCenterID: number): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_MEDICAL_CENTERS + '/enable', { medicalCenterID }, this.httpOptions).pipe(map(this.extractData));
}
deleteMedicalCenter(medicalCenterID: number): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_MEDICAL_CENTERS + '/delete', { medicalCenterID }, this.httpOptions).pipe(map(this.extractData));
}


// getMedicalHistory(patientID: number): Observable<any> {
//   this.setHeaders();
//   console.log(API_ENDPOINT_URL + API_MEDICAL_CENTERS + '/' + patientID)
//   return this.http.get(API_ENDPOINT_URL + API_PATIENTS + '/' + patientID + '/history', this.httpOptions).pipe(map(this.extractData));
// }


// getPatient(patientID: number): Observable<any> {
//   this.setHeaders();
  
//   return this.http.get(API_ENDPOINT_URL + API_PATIENTS + '/' + patientID, this.httpOptions).pipe(map(this.extractData));
// }

getPatients(): Observable<any> {
  this.setHeaders();
  return this.http.get(API_ENDPOINT_URL + API_PATIENTS, this.httpOptions).pipe(map(this.extractData));
}
getPatient(patientID: number): Observable<any> {
  this.setHeaders();
  return this.http.get(API_ENDPOINT_URL + API_PATIENTS + '/' + patientID, this.httpOptions).pipe(map(this.extractData));
}
addPatient(data:any): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_PATIENTS + '/add', data, this.httpOptions).pipe(map(this.extractData));
}
editPatient(data:any): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_PATIENTS + '/edit', data, this.httpOptions).pipe(map(this.extractData));
}
disablePatient(patientID: number): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_PATIENTS + '/disable', { patientID }, this.httpOptions).pipe(map(this.extractData));
}
enablePatient(patientID: number): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_PATIENTS + '/enable', { patientID }, this.httpOptions).pipe(map(this.extractData));
}
deletePatient(patientID: number): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_PATIENTS + '/delete', { patientID }, this.httpOptions).pipe(map(this.extractData));
}



getConsultation(patientID: number, consultationID: number): Observable<any> {
  this.setHeaders();
  return this.http.get(API_ENDPOINT_URL + API_PATIENTS + '/' + patientID + '/consultations/' + consultationID, this.httpOptions).pipe(map(this.extractData));
}



getAllMedicalConsultations(): Observable<any> {
  this.setHeaders();
  return this.http.get(API_ENDPOINT_URL + API_CONSULTATION, this.httpOptions).pipe(map(this.extractData));
}

getMedicalConsultation(consultationID:number): Observable <any>{
  this.setHeaders();
  return this.http.get(API_ENDPOINT_URL + API_CONSULTATION + '/' + consultationID , this.httpOptions).pipe(map(this.extractData));
}

addMedicalConsultation(consultationID:number, data:any): Observable<any> {
  this.setHeaders();
  
  return this.http.post(API_ENDPOINT_URL + API_CONSULTATION + '/add/' + consultationID, data, this.httpOptions).pipe(map(this.extractData));
}

getMedicalConsultationDetail(consultationID:number): Observable <any>{
  this.setHeaders();
  return this.http.get(API_ENDPOINT_URL + 'consultation_detail' + '/' + consultationID , this.httpOptions).pipe(map(this.extractData));
}

updateMedicalConsultation(data:any): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_CONSULTATION + '/update', data, this.httpOptions).pipe(map(this.extractData));
}

updateMedicalConsultationStatus(consultationID: number, newStatus: number): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_CONSULTATION + '/status', { consultationID, newStatus }, this.httpOptions).pipe(map(this.extractData));
}

cancelMedicalConsultation(consultationID: number, userID: number): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_CONSULTATION + '/cancel', { consultationID, userID }, this.httpOptions).pipe(map(this.extractData));
}


getMedicalHistory(patientID: number): Observable<any> {
  this.setHeaders();
  return this.http.get(API_ENDPOINT_URL + API_PATIENTS + '/' + patientID + '/history' , this.httpOptions).pipe(map(this.extractData));
}

checkDoctorExist(doctorEmail: any): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + "create_consultations" + '/check_doctor', doctorEmail, this.httpOptions).pipe(map(this.extractData));
}

checkDoctor(doctorEmail: string): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_CREATE + '/check_doctor', { doctorEmail }, this.httpOptions).pipe(map(this.extractData));
}

checkPatient(patientEmail: string): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_CREATE + '/check_patient', { patientEmail }, this.httpOptions).pipe(map(this.extractData));
}

createConsultations(patientID: number, doctorID: number): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_CREATE , { patientID, doctorID }, this.httpOptions).pipe(map(this.extractData));
}

// createConsultationEdit(data:any): Observable<any>{
//   this.setHeaders();
//   return this.http.post(API_ENDPOINT_URL + API_CREATE + '/edit', { data }, this.httpOptions).pipe(map(this.extractData));
// }

createConsultationEdit(data:any): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_CREATE + '/edit', data, this.httpOptions).pipe(map(this.extractData));
}
createConsultationCancel(consultationID:any, detailID:any): Observable<any> {
  this.setHeaders();
  return this.http.post(API_ENDPOINT_URL + API_CREATE + '/cancel', {consultationID, detailID}, this.httpOptions).pipe(map(this.extractData));
}
}
