import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  
  socket: any;
  readonly uri: string = "http://api-homecare.portafolio.cloud:1337";
  constructor() {
    this.socket = io.connect('http://localhost:1337');
    }


  listen(eventName: string){
    return new Observable((subscriber) =>{
      this.socket.on(eventName, (data) =>{
        subscriber.next(data);
      })
    });
  }

  emit(eventName: string,  data: any){
    this.socket.emit(eventName, data);
  }
}

