import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { EventModule } from '../../../../models/event/event.module';
import { ApiRestService } from '../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ResponseModule } from '../../../../models/response/response.module';
import Swal from 'sweetalert2';
import { map } from 'rxjs/operators';

@Component({
  selector: 'kt-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {
  eventID: number;
  eventDetail$: Observable<EventModule> = null;
  constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router, private route: ActivatedRoute) {
    this.eventID = +route.snapshot.params.eventID;
  }

  ngOnInit() {
    this.getEventDetail();
  }

  publishEvent() {
    this.apiRest.publishEvent(this.eventID).subscribe(
      (response: ResponseModule) => {
        if (response.statusCode === 0) {
          this.getEventDetail();
          Swal.fire(
            response.statusMessage,
            '',
            'success'
          )
        } else {
          Swal.fire(
            response.statusMessage,
            '',
            'warning'
          )
        }
      },
      (error) => {
        Swal.fire(
          error,
          '',
          'warning'
        )
      }
    );
  }

  removeEvent() {
    this.apiRest.removeEvent(this.eventID).subscribe(
      (response: ResponseModule) => {
        if (response.statusCode === 0) {
          this.getEventDetail();
          Swal.fire(
            response.statusMessage,
            '',
            'success'
          )
        } else {
          Swal.fire(
            response.statusMessage,
            '',
            'warning'
          )
        }
      },
      (error) => {
        Swal.fire(
          error,
          '',
          'warning'
        )
      }
    );
  }

  getEventDetail() {
    // Observable para listar información ó refrescar datos
    this.eventDetail$ = this.apiRest.getEventDetail(this.eventID).pipe(
      map((response: ResponseModule): EventModule => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return new EventModule();
        }
      })
    );
  }

}
