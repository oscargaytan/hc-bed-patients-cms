// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialPreviewModule } from './../../partials/content/general/material-preview/material-preview.module';
import { NgbDropdownModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatOptionModule,
  MatSelectModule,
  MatCardModule,
  MatMenuModule,
  MatSortModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventsComponent } from './events.component';
import { FileUploadModule } from 'ng2-file-upload';
import { NgImageSliderModule } from 'ng-image-slider';
import { SlideshowModule } from 'ng-simple-slideshow';
import { EventAddComponent } from './event-add/event-add.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { EventEditComponent } from './event-edit/event-edit.component';

const eventRoutes = [
  {
    path: '',
    component: EventsComponent
  },
  {
    path: 'detail/:eventID',
    component: EventDetailComponent
  },
  {
    path: 'add',
    component: EventAddComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    PartialsModule,
    CoreModule,
    MatPaginatorModule,
    MatTableModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    NgbDropdownModule,
    MatOptionModule,
    MatCardModule,
    MatSelectModule,
    MatMenuModule,
    MaterialPreviewModule,
    FormsModule,
    MatSortModule,
    NgbTabsetModule,
    FileUploadModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild(eventRoutes),
    NgImageSliderModule,
    SlideshowModule
  ],
  providers: [],
  declarations: [
    EventsComponent,
    EventAddComponent,
    EventDetailComponent,
    EventEditComponent
  ]
})
export class EventsModule { }
