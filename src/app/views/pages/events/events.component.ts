import { ResponseModule } from './../../../models/response/response.module';
import { ApiRestService } from './../../../network/api-rest.service';
import { EventModule } from './../../../models/event/event.module';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { map, tap } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'kt-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  events$: Observable<EventModule[]> = null;
  displayedColumns = ['eventName', 'eventPlace', 'eventDate', 'actions'];
  dataSource: MatTableDataSource<EventModule>;
  @ViewChild('eventsPage', { read: MatPaginator, static: false }) paginator: MatPaginator;
  @ViewChild('eventsSort', { read: MatSort, static: false }) sort: MatSort;

  eventsPending$: Observable<EventModule[]> = null;
  displayedColumnsRequest = ['eventName', 'eventPlace', 'eventDate', 'actions'];
  dataSourceRequest: MatTableDataSource<EventModule>;
  @ViewChild('eventsAllPage', { read: MatPaginator, static: false }) paginatorNew: MatPaginator;
  @ViewChild('eventsAllSort', { read: MatSort, static: false }) sortNew: MatSort;

  constructor(private apiRest: ApiRestService, private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.getApprovedEvents();
  }

  beforeChange(event: NgbTabChangeEvent) {
    if (event.nextId === 'tab-user-list') {
      this.getApprovedEvents();
    } else if (event.nextId === 'tab-user-request') {
      this.getAllEvents();
    }
  }

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getApprovedEvents() {
    this.events$ = this.apiRest.getApprovedEvents().pipe(
      map((response: ResponseModule): EventModule[] => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((news: EventModule[]) => {
        this.dataSource = new MatTableDataSource(news);
        this.configDataTable();
      })
    );
  }

  approveEvent(eventID: number) {
    var confirmButton = '';
    var confirmMessage = '';
    var successTitle = '';
    var successMsj = '';
    var cancelBtn = '';
    this.translateService.get('EVENTS.INDEX.ALERTS.APPROVE').subscribe(
      translate => {
        confirmButton = translate;
      }
    );
    this.translateService.get('EVENTS.INDEX.ALERTS.APPROVE_MESSAGE').subscribe(
      translate => {
        confirmMessage = translate;
      }
    );
    this.translateService.get('EVENTS.INDEX.ALERTS.APPROVE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('EVENTS.INDEX.ALERTS.APPROVE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );

    Swal.fire({
      title: confirmMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmButton,
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.publishEvent(eventID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              this.getAllEvents();
              this.getApprovedEvents();
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
            } else {
              Swal.fire(
                response.statusMessage,
                '',
                'warning'
              );
            }
          }, (error) => {
            console.log('Error en ws', error);
          }
        );
      }
    });
  }


  revokeEvent(eventID: number) {
    var confirmButton = '';
    var confirmMessage = '';
    var successTitle = '';
    var successMsj = '';
    var cancelBtn = '';
    this.translateService.get('EVENTS.INDEX.ALERTS.DISABLE').subscribe(
      translate => {
        confirmButton = translate;
      }
    );
    this.translateService.get('EVENTS.INDEX.ALERTS.DISABLE_MESSAGE').subscribe(
      translate => {
        confirmMessage = translate;
      }
    );
    this.translateService.get('EVENTS.INDEX.ALERTS.DISABLE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('EVENTS.INDEX.ALERTS.DISABLE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );

    Swal.fire({
      title: confirmMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmButton,
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.removeEvent(eventID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              this.getAllEvents();
              this.getApprovedEvents();
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
            } else {
              Swal.fire(
                response.statusMessage,
                '',
                'warning'
              );
            }
          }, (error) => {
            console.log('Error en ws', error);
          }
        );
      }
    });
  }

  deleteEvent(eventID: number) {
    var confirmButton = '';
    var confirmMessage = '';
    var successTitle = '';
    var successMsj = '';
    var cancelBtn = '';
    this.translateService.get('EVENTS.INDEX.ALERTS.DELETE').subscribe(
      translate => {
        confirmButton = translate;
      }
    );
    this.translateService.get('EVENTS.INDEX.ALERTS.DELETE_MESSAGE').subscribe(
      translate => {
        confirmMessage = translate;
      }
    );
    this.translateService.get('EVENTS.INDEX.ALERTS.DELETE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('EVENTS.INDEX.ALERTS.DELETE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );

    Swal.fire({
      title: confirmMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmButton,
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.deleteEvent(eventID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              this.getApprovedEvents();
              this.getAllEvents();
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
            } else {
              Swal.fire(
                response.statusMessage,
                '',
                'warning'
              );
            }
          }, (error) => {
            console.log('Error en ws', error);
          }
        );
      }
    });
  }

  getAllEvents() {
    this.eventsPending$ = this.apiRest.getAllEvents().pipe(
      map((response: ResponseModule): EventModule[] => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((news: EventModule[]) => {
        this.dataSourceRequest = new MatTableDataSource(news);
        this.configDataTableRequest();
      })
    );
  }

  applyFilterRequest(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSourceRequest.filter = filterValue;
  }

  configDataTableRequest() {
    this.dataSourceRequest.paginator = this.paginatorNew;
    this.dataSourceRequest.sort = this.sortNew;
  }

}
