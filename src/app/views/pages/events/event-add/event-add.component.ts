import { ResponseModule } from './../../../../models/response/response.module';
import { ApiRestService } from './../../../../network/api-rest.service';
import { environment } from './../../../../../environments/environment.prod';
import { Md5 } from 'ts-md5/dist/md5';
import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs/operators';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { FormGroup, FormBuilder, NgForm } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import * as S3 from 'aws-sdk/clients/s3';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { AddEventModule } from './../../../../models/add-event/add-event.module';

@Component({
  selector: 'kt-event-add',
  templateUrl: './event-add.component.html',
  styleUrls: ['./event-add.component.scss']
})
export class EventAddComponent implements OnInit {

  public filePreviewPath: SafeUrl;
  filesSelected = false;
  @ViewChild('autosize', { static: false }) autosize: CdkTextareaAutosize;
  uploadForm: FormGroup;
  filesToUpload: Array<File>;

  public uploader: FileUploader = new FileUploader({
    isHTML5: true
  });
  percent: any;

  constructor(private ngZone: NgZone, private sanitizer: DomSanitizer, private apiRest: ApiRestService, private translateService: TranslateService, private router: Router) { }

  ngOnInit() {

  }

  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this.ngZone.onStable.pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  makeid(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  addEvent(ngForm: NgForm) {
    if (ngForm.value.eventName !== '' && ngForm.value.eventName.length >= 3 && ngForm.value.eventBody !== '' && ngForm.value.eventBody.length >= 3 && ngForm.value.eventPlace !== '') {
      const md5 = new Md5();
      this.filesSelected = true;
      const urlsD = [];
      let validFiles = true;
      for (let i = 0; i < this.filesToUpload.length; i++) {
        var file = this.filesToUpload[i];
        if (file.size > 5000000) {
          validFiles = false;
          Swal.fire(
            '',
            'La imagen #' + (i + 1) + ' pesa más de 5 MB verifica las imágenes antes de proceder.',
            'warning'
          );
          break;
        }
      }
      if (validFiles) {
        this.filesToUpload.forEach(file => {
          const date = new Date();
          const extension = file.name.split('.');
          const nameTemp = md5.appendStr(environment.enviroment).end() + '/uploads/events/resources/' + md5.appendStr(this.makeid(15) + date.getTime).end() + '.' + extension[(extension.length - 1)];
          const params = {
            Bucket: 'ipulse-aws',
            Key: nameTemp,
            Body: file,
            ACL: 'public-read',
            ContentType: file.type
          };
          const bucket = new S3(environment.awsS3);
          bucket.upload(params, (err, data) => {
            if (err) {
              console.log('There was an error uploading your file: ', err);
              return false;
            }
            console.log('Successfully uploaded file.', data);
            return true;
          });
          urlsD.push(nameTemp);
        });
        const eventData = new AddEventModule();
        eventData.eventTitle = ngForm.value.eventName;
        eventData.eventPlace = ngForm.value.eventPlace;
        eventData.eventBody = ngForm.value.eventBody;
        eventData.eventImages = urlsD;
        this.apiRest.addEvent(eventData).subscribe(
          (response: ResponseModule) => {
            var successTitle = '';
            var successMsj = '';
            var unsuccessTitle = '';
            var unsuccessMsj = '';

            this.translateService.get('EVENTS.ADD.ALERTS.ADD_UNSUCCESS_TITLE').subscribe(
              translate => {
                unsuccessTitle = translate;
              }
            );
            this.translateService.get('EVENTS.ADD.ALERTS.ADD_UNSUCCESS_MESSAGE').subscribe(
              translate => {
                unsuccessMsj = translate;
              }
            );
            this.translateService.get('EVENTS.ADD.ALERTS.ADD_SUCCESS_TITLE').subscribe(
              translate => {
                successMsj = translate;
              }
            );
            this.translateService.get('EVENTS.ADD.ALERTS.ADD_SUCCESS_MESSAGE').subscribe(
              translate => {
                successTitle = translate;
              }
            );
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.router.navigate(['/events']);
            } else {
              Swal.fire(
                unsuccessTitle,
                unsuccessMsj,
                'error'
              );
            }
            this.uploader.clearQueue();
          },
          (error) => {
            console.log("error", error);
            var serverMsj = '';
            var serverTitle = '';
            this.translateService.get('CATALOGS.SERVER_MESSAGE.MESSAGE').subscribe(
              translate => {
                serverMsj = translate;
              }
            );
            this.translateService.get('CATALOGS.SERVER_MESSAGE.TITLE').subscribe(
              translate => {
                serverTitle = translate;
              }
            );
            Swal.fire(
              serverTitle,
              serverMsj,
              'error'
            );
          }
        );
      }
    } else {
      var serverMsj = '';
      var serverTitle = '';
      this.translateService.get('CATALOGS.EMPTY_FIELDS.MSJ').subscribe(
        translate => {
          serverMsj = translate;
        }
      );
      this.translateService.get('CATALOGS.EMPTY_FIELDS.TITLE').subscribe(
        translate => {
          serverTitle = translate;
        }
      );
      Swal.fire(
        serverTitle,
        serverMsj,
        'error'
      );
    }
  }

  fileChangeEvent() {
    const queues = this.uploader.queue;
    var filesTemp: File[] = [];
    for (let i = 0; i < queues.length; i++) {
      var file = queues[i]._file;
      filesTemp.push(file);
    }
    this.filesToUpload = filesTemp;
  }

  getUrlPreview(file: FileItem) {
    const sanitizedUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file._file));
    return sanitizedUrl;
  }

  openFileBrowser(event: any) {
    event.preventDefault();
    const element: HTMLElement = document.getElementById('eventImages') as HTMLElement;
    element.click();
  }

}
