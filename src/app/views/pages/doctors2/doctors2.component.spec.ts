import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Doctors2Component } from './doctors2.component';

describe('Doctors2Component', () => {
  let component: Doctors2Component;
  let fixture: ComponentFixture<Doctors2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Doctors2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Doctors2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
