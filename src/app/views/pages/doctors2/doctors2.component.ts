// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'kt-doctors2',
//   templateUrl: './doctors2.component.html',
//   styleUrls: ['./doctors2.component.scss']
// })
// export class Doctors2Component implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }


import { Doctors2Module } from './../../../models/doctors2/doctors2.module';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, from } from 'rxjs';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ApiRestService } from './../../../network/api-rest.service';
import { ResponseModule } from './../../../models/response/response.module';
import { tap, map } from 'rxjs/operators';

@Component({
  selector: 'kt-doctors2',
  templateUrl: './doctors2.component.html',
  styleUrls: ['./doctors2.component.scss']
})
export class Doctors2Component implements OnInit {
  allDoctors2$: Observable<Doctors2Module[]> = null;
  displayedColumns = ['doctors2Name','doctors2LName','doctors2Specialty'];
  dataSource: MatTableDataSource<Doctors2Module>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private apiRest: ApiRestService) { }

  ngOnInit() {
    this.getAllDoctors2();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAllDoctors2() {
    this.allDoctors2$ = this.apiRest.getAllDoctors2().pipe(
      map((response: ResponseModule): Doctors2Module[] => {
        console.log(response);
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((doctors2: Doctors2Module[]) => {
        this.dataSource = new MatTableDataSource(doctors2);
        this.configDataTable();
      })
    );
  }

}