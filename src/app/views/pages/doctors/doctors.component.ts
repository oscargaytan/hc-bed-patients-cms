import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModule } from 'src/app/models/user/user.module';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ApiRestService } from 'src/app/network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseModule } from 'src/app/models/response/response.module';
import Swal from 'sweetalert2';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'kt-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DoctorsComponent implements OnInit {

  users$: Observable<UserModule[]>;
  displayedColumns = ['userName', 'userEmail', 'userSpecialty', 'userStatus', 'actions'];
  dataSource: MatTableDataSource<UserModule>;
  @ViewChild('usersPage', { read: MatPaginator, static: false }) paginator: MatPaginator;
  @ViewChild('users', { read: MatSort, static: false }) sort: MatSort;

  constructor(private apiRest: ApiRestService, private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.getUsersList();
  }

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getUsersList() {
    this.users$ = this.apiRest.getDoctors().pipe(
      map((response: ResponseModule): UserModule[] => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((users: UserModule[]) => {
        this.dataSource = new MatTableDataSource(users);
        this.configDataTable();
      })
    );
  }

  disableUser(userID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('DOCTORS.INDEX.ALERTS.DISABLE').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('DOCTORS.INDEX.ALERTS.DISABLE_MESSAGE').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('DOCTORS.INDEX.ALERTS.DISABLE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('DOCTORS.INDEX.ALERTS.DISABLE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.disableUser(userID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getUsersList();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }

  enableUser(userID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('DOCTORS.INDEX.ALERTS.ENABLE').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('DOCTORS.INDEX.ALERTS.ENABLE_MESSAGE').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('DOCTORS.INDEX.ALERTS.ENABLE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('DOCTORS.INDEX.ALERTS.ENABLE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.enableUser(userID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getUsersList();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }

  deleteUser(userID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('DOCTORS.INDEX.ALERTS.DELETE').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('DOCTORS.INDEX.ALERTS.DELETE_MESSAGE').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('DOCTORS.INDEX.ALERTS.DELETE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('DOCTORS.INDEX.ALERTS.DELETE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.deleteUser(userID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getUsersList();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }

}
