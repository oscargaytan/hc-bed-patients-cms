// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'kt-doctor-add',
//   templateUrl: './doctor-add.component.html',
//   styleUrls: ['./doctor-add.component.scss']
// })
// export class DoctorAddComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { ApiRestService } from '../../../../network/api-rest.service';
import { ResponseModule } from './../../../../models/response/response.module';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { SpecialtiesModule } from './../../../../models/specialties/specialties.module';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Component({
	selector: 'kt-doctor-add',
	templateUrl: './doctor-add.component.html',
	styleUrls: ['./doctor-add.component.scss']
})
export class DoctorAddComponent implements OnInit {

	specialties: any;
	
	

	doctorForm:FormGroup;

	constructor(private apiRest: ApiRestService,
				private fb: FormBuilder,
				private translateService: TranslateService,
				private router: Router) { }

				
//   toppingList = [1,2,3,4,5,6];
  toppingList = [
	  {
		  name:'xx1',
		  id:1
	  },
	  {
		  name:'xx2',
		  id:2
	  },
	  {
		  name:'xx3',
		  id:3
	  }	  
	];

ngOnInit() {
		this.initForm();
		this.getAllSpecialties();
	}

initForm() {
	this.doctorForm = this.fb.group({
		name: ['', Validators.compose(
				[
					Validators.required,
					Validators.minLength(3)
				]
			)
				],
		lname: ['', Validators.compose(
				[
					Validators.required,
					Validators.minLength(3)
				]
			)
				],
		email: ['', Validators.compose(
				[
					Validators.required,
					Validators.email
				]
			)
				],
		spec: ['', Validators.compose(
					[
						Validators.required
						
					]
				)
					],
		},{

	});
}


	
isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.doctorForm.controls[controlName];
		if (!control) {
			return false;
		}
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}

	

	
getAllSpecialties() {
	this.apiRest.getAllSpecialties().subscribe(
		(response: ResponseModule)=>{
			
			this.specialties = response.resultset;
			console.log(this.specialties[0])
		}
	)	
}


addDoctor() {
		const request = [];

		const controls = this.doctorForm.controls;

		if (this.doctorForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		const userName = controls.name.value;
		const userLastName = controls.lname.value;
		const userEmail = controls.email.value;
		const userType = controls.name.value;
		const userSpecialtyN = controls.spec.value;
		console.log(controls);

		let userSpecialty = [];
		userSpecialty.push(userSpecialtyN);
		let data = {
			userName,
			userLastName,
			userEmail,
			userType,
			userSpecialty
		};
		
		this.apiRest.addDoctor(data).subscribe(
			(response: ResponseModule) => {
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				var shortMsj= '';
				this.translateService.get('Error al añádir doctor').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
				);
				this.translateService.get('Ese correo electrónico ya está registrado.').subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('El nombre del doctor es muy corto.').subscribe(
					translate => {
						shortMsj = translate;
					}
				);
				this.translateService.get('Doctor invitado').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('Se envió la invitación vía email al doctor.').subscribe(
					translate => {
						successMsj = translate;
					}
				);
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/doctors']);
				} else if(response.statusCode === 9){
					Swal.fire(
						unsuccessTitle,
						shortMsj,
						'warning'
					);

				} else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
				console.log(response);
			},
			(error) => {
				console.log(error);
			}
		);
	}
}

