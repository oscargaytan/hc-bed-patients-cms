// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'kt-doctor-edit',
//   templateUrl: './doctor-edit.component.html',
//   styleUrls: ['./doctor-edit.component.scss']
// })
// export class DoctorEditComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

import { UserModule } from './../../../../models/user/user.module';
import { ResponseModule } from './../../../../models/response/response.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiRestService } from './../../../../network/api-rest.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2';


@Component({
  selector: 'kt-doctor-edit',
  templateUrl: './doctor-edit.component.html',
  styleUrls: ['./doctor-edit.component.scss']
})
export class DoctorEditComponent implements OnInit {

  specialties: any;

  userID: number;
  userDetail: UserModule;
  userTypes = [];
  investment = '';
  emprenteneur = '';

  doctorForm:FormGroup;
  constructor(private apiRest: ApiRestService,
              private translateService: TranslateService,
              private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute) {
    this.userID = +route.snapshot.params.userID;
    this.userTypes = [{
      'idType': 2,
      'name': 'CATALOGS.USER_TYPE.DOCTOR'
    }];
  }

  ngOnInit() {
    this.initForm();
    this.getUserDetail(this.userID);
    this.getAllSpecialties();
  }

  initForm() {
    this.doctorForm = this.fb.group({
      name: ['', Validators.compose(
          [
            Validators.required,
            Validators.minLength(3)
          ]
          )
            ],
      lname: ['', Validators.compose(
          [
            Validators.required,
            Validators.minLength(3)
          ]
          )
            ],
      email: ['', Validators.compose(
          [
            Validators.required,
            Validators.email
          ]
          )
            ],
      spec: ['', Validators.compose(
              [
                Validators.required
                
              ]
              )
                ],
    },{});
  }
  
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.doctorForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  getAllSpecialties() {
    this.apiRest.getAllSpecialties().subscribe(
      (response: ResponseModule)=>{
        
        this.specialties = response.resultset;
        
      }
    )	
  }

  getUserDetail(userID: number) {
    this.apiRest.getDoctor(userID).subscribe(
      (response: ResponseModule) => {
        if (response.statusCode === 0) {
          
          this.doctorForm.controls['name'].setValue(response.resultset.userName);
          this.doctorForm.controls['lname'].setValue(response.resultset.userLName);
          this.doctorForm.controls['email'].setValue(response.resultset.userEmail);
          this.doctorForm.controls['spec'].setValue(response.resultset.userSpecialties2);
          console.log(response);
        }
      }, (error) => {
        console.log(error);
      }
    );
  }

  editUser() {

    const controls = this.doctorForm.controls;

		if (this.doctorForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
      );
			return;
	}
    // var userEdit = new UserModule();
    // userEdit.userID = this.userID;
    // userEdit.userName = controls.name.value;
    // userEdit.userLName = controls.lname.value;
    // userEdit.userEmail = controls.email.value;
    // userEdit.userType = 2;
    // console.log(userEdit);

    const userName = controls.name.value;
		const userLastName = controls.lname.value;
		const userEmail = controls.email.value;
		const userType = controls.name.value;
    const userSpecialtyN = controls.spec.value;
    
    let userSpecialty = [];
    userSpecialty.push(userSpecialtyN);
    let userID = this.userID;

    let data = {
      userID,
      userName,
			userLastName,
			userEmail,
			userType,
			userSpecialty
    }

    console.log("+++++++++++"+data.userSpecialty);






    this.apiRest.editDoctor(data).subscribe(
      (response: ResponseModule) => {
        console.log(response);
        var successTitle = '';
        var successMsj = '';
        var unsuccessTitle = '';
        var unsuccessMsj = '';
        var shortMsj= '';
        this.translateService.get('Error al editar doctor').subscribe(
          translate => {
            unsuccessTitle = translate;
          }
        );
        this.translateService.get('Ese correo electrónico ya está registrado.').subscribe(
          translate => {
            unsuccessMsj = translate;
          }
        );
        this.translateService.get('El nombre del doctor es muy corto.').subscribe(
					translate => {
						shortMsj = translate;
					}
				);
        this.translateService.get('Doctor actualizado').subscribe(
          translate => {
            successTitle = translate;
          }
        );
        this.translateService.get('La informacion será actualizada.').subscribe(
          translate => {
            successMsj = translate;
          }
        );
        if (response.statusCode === 0) {
          Swal.fire(
            successTitle,
            successMsj,
            'success'
          );
          this.router.navigate(['/doctors']);
        } else if(response.statusCode === 9){
					Swal.fire(
						unsuccessTitle,
						shortMsj,
						'warning'
					);

				}else {
          Swal.fire(
            unsuccessTitle,
            unsuccessMsj,
            'warning'
          );
        }
      },
      (error) => {
        console.log(error);
        var unsuccessTitle = '';
        this.translateService.get('USERS.EDIT.ALERTS.EDIT_UNSUCCESS_TITLE').subscribe(
          translate => {
            unsuccessTitle = translate;
          }
        );
        Swal.fire(
          unsuccessTitle,
          error,
          'warning'
        );
      }
    );
  }

}
