import { MaterialPreviewModule } from './../../partials/content/general/material-preview/material-preview.module';
import { NgbDropdownModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatOptionModule,
  MatSelectModule,
  MatSortModule
} from '@angular/material';
import { DoctorsComponent } from './doctors.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DoctorAddComponent } from './doctor-add/doctor-add.component';
import { DoctorEditComponent } from './doctor-edit/doctor-edit.component';

const userRoutes = [
  {
    path: '',
    component: DoctorsComponent
  }, {
    path: 'add',
    component: DoctorAddComponent
  }, {
    path: 'edit/:userID',
    component: DoctorEditComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    PartialsModule,
    CoreModule,
    MatPaginatorModule,
    MatTableModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    NgbDropdownModule,
    MatOptionModule,
    MatSortModule,
    MatSelectModule,
    MaterialPreviewModule,
    FormsModule,
    ReactiveFormsModule,
    NgbTabsetModule,
    TranslateModule.forChild(),
    RouterModule.forChild(userRoutes),
  ],
  providers: [],
  declarations: [
    DoctorsComponent,
    DoctorAddComponent,
    DoctorEditComponent,
  ]
})
export class DoctorsModule {
}
