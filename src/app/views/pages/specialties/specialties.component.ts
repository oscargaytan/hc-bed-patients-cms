import { TranslateService } from '@ngx-translate/core';
import { ApiRestService } from '../../../network/api-rest.service';
import { ResponseModule } from './../../../models/response/response.module';
import { SpecialtiesModule } from './../../../models/specialties/specialties.module';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'kt-specialties',
  templateUrl: './specialties.component.html',
  styleUrls: ['./specialties.component.scss']
})
export class SpecialtiesComponent implements OnInit {

  specialties$: Observable<SpecialtiesModule[]>;
  displayedColumns = ['specialtiesName', 'specialtiesStatus', 'actions'];
  dataSource: MatTableDataSource<SpecialtiesModule>;
  @ViewChild('usersPage', { read: MatPaginator, static: false }) paginator: MatPaginator;
  @ViewChild('users', { read: MatSort, static: false }) sort: MatSort;

  constructor(private apiRest: ApiRestService, private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.getAllSpecialties();
  }

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAllSpecialties() {
    this.specialties$ = this.apiRest.getAllSpecialties().pipe(
      map((response: ResponseModule): SpecialtiesModule[] => {
        if (response.statusCode === 0) {
          console.log(response);
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((users: SpecialtiesModule[]) => {
        this.dataSource = new MatTableDataSource(users);
        this.configDataTable();
      })
    );
  }

  disableSpecialty(specialtyID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('Inhabilitar').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('La especialidad será inhabilitada ¿desea continuar con la solicitud?').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('Cancelar').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('Especialidad inhabilitada').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('La información será actulizada.').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        console.log("disable" + specialtyID);
        this.apiRest.disableSpecialty(specialtyID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getAllSpecialties();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }



  enableSpecialty(specialtyID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('Reactivar').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('La especialidad será reactivada ¿desea continuar con la solicitud?').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('Cancelar').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('Especialidad reactivada').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('La información será actulizada.').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        console.log("enable" + specialtyID);
        this.apiRest.enableSpecialty(specialtyID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getAllSpecialties();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }

  deleteSpecialty(specialtyID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('Eliminar').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('La especialidad será eliminada del sistema, ¿desea continuar con la solicitud?').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('Cancelar').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('Especialidad eliminada').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('La información será actulizada.').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        console.log('deleted'+specialtyID);
        this.apiRest.deleteSpecialty(specialtyID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getAllSpecialties();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }

}



//2 . La especialidad será desactivada ¿desea continuar con la solicitud?
//1 . Desactivar especialidad
//3 . Cancelar
//4 . Especialidad desactivada
//5 . La información será actulizada.