import { SpecialtiesModule } from './../../../../models/specialties/specialties.module';
import { ResponseModule } from './../../../../models/response/response.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiRestService } from './../../../../network/api-rest.service';
import { Component, OnInit } from '@angular/core';

import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2';


@Component({
  selector: 'kt-specialty-edit',
  templateUrl: './specialty-edit.component.html',
  styleUrls: ['./specialty-edit.component.scss']
})
export class SpecialtyEditComponent implements OnInit {

  specialtyID: number;
  specialtyDetail: SpecialtiesModule;

  specialtyForm:FormGroup;
 
  constructor(private apiRest: ApiRestService, 
              private translateService: TranslateService,
              private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute) {
    this.specialtyID = +route.snapshot.params.specialtyID;

    console.log("ID "+this.specialtyID)
    
  }

  ngOnInit() {
    this.initForm();
    this.getSpecialtyDetail(this.specialtyID);
    
  }

  initForm() {
    this.specialtyForm = this.fb.group({
      name: ['', Validators.compose(
          [
            Validators.required,
            Validators.minLength(3)
          ]
        )
      ],
    },{});
  }
  
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.specialtyForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  getSpecialtyDetail(specialtyID: number) {
    this.apiRest.getSpecialty(specialtyID).subscribe(
      (response: ResponseModule) => {
        console.log(response);
        if (response.statusCode === 0) {
          console.log(response.resultset)
          this.specialtyForm.controls['name'].setValue(response.resultset.specialtiesName);
        }
      }, (error) => {
        
        console.log(error);
      }
    );
  }


  

  editSpecialty() {

    console.log("xxxxx");

    const controls = this.specialtyForm.controls;

		if (this.specialtyForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
      );
      console.log('yyyyyyy');
			return;
	}
	
	
	//const specialtyName = controls.name.value;
    var specialtyEdit = new SpecialtiesModule();
    
    specialtyEdit.specialtiesID = this.specialtyID;
    specialtyEdit.specialtiesName = controls.name.value;
    
    

    console.log(specialtyEdit.specialtiesName);
    

    
      this.apiRest.editSpecialty(specialtyEdit).subscribe(
        (response: ResponseModule) => {
          var successTitle = '';
          var successMsj = '';
          var unsuccessTitle = '';
          var unsuccessMsj = '';
          var shortMsj= '';
          this.translateService.get('Error al editar especialidad').subscribe(
            translate => {
              unsuccessTitle = translate;
            }
          );
          this.translateService.get('La especialidad ya está registrada.').subscribe(
            translate => {
              unsuccessMsj = translate;
            }
          );
          this.translateService.get('El nombre de la especialidad es muy corto').subscribe(
            translate => {
              shortMsj = translate;
            }
          );
          this.translateService.get('Especialidad actualizada').subscribe(
            translate => {
              successTitle = translate;
            }
          );
          this.translateService.get('La informacion será actualizada.').subscribe(
            translate => {
              successMsj = translate;
            }
          );
          console.log("respuesta esp");
          console.log(response);
          if (response.statusCode === 0 ) {
            
            Swal.fire(
              successTitle,
              successMsj,
              'success'
            );
            this.router.navigate(['/specialties']);
          } 
          else if(response.statusCode === 9){
            Swal.fire(
              unsuccessTitle,
              shortMsj,
              'warning'
            );
  
          }else {
            Swal.fire(
              unsuccessTitle,
              unsuccessMsj,
              'warning'
            );
          }
        },
        (error) => {
          console.log(error);
          var unsuccessTitle = '';
          this.translateService.get('USERS.EDIT.ALERTS.EDIT_UNSUCCESS_TITLE').subscribe(
            translate => {
              unsuccessTitle = translate;
            }
          );
          Swal.fire(
            unsuccessTitle,
            error,
            'warning'
          );
        }
        
      );
   
      
    

    
  }
  

}
