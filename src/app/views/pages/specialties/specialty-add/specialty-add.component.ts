import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { ApiRestService } from '../../../../network/api-rest.service';
import { ResponseModule } from './../../../../models/response/response.module';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
	selector: 'kt-specialty-add',
	templateUrl: './specialty-add.component.html',
	styleUrls: ['./specialty-add.component.scss']
})
export class SpecialtyAddComponent implements OnInit {

	treatmentForm:FormGroup;

	constructor(private apiRest: ApiRestService,
				private fb: FormBuilder,
				private translateService: TranslateService,
				private router: Router) { }

	ngOnInit() {
		this.initForm();
			
  	}

  initForm() {
	this.treatmentForm = this.fb.group({
		name: ['', Validators.compose(
				[
					Validators.required,
					Validators.minLength(3)
				]
			)
		],
	},{});
}

isControlHasError(controlName: string, validationType: string): boolean {
	const control = this.treatmentForm.controls[controlName];
	if (!control) {
		return false;
	}
	const result = control.hasError(validationType) && (control.dirty || control.touched);
	return result;
}
  
 

addSpecialty() {
		console.log('xxxxx');
		const request = [];
		

		const controls = this.treatmentForm.controls;

		if (this.treatmentForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
	}
	console.log('yyyyyyy');
	
	const specialtyName = controls.name.value;

	// let specialtyName = {
	// 	name: controls.name.value
	//   }
		
    
		this.apiRest.addSpecialty(specialtyName).subscribe(
			(response: ResponseModule) => {
        
        
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				var shortMsj = ''
				this.translateService.get('Error al añadir especialidad').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
        );
        
				this.translateService.get('La especialidad ya está registrada.').subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('El nombre de la especialidad es muy corto.').subscribe(
					translate => {
						shortMsj = translate;
					}
				);
				this.translateService.get('Especialidad agregada').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('La informaciòn será actualizada').subscribe(
					translate => {
						successMsj = translate;
					}
        );
        
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/specialties']);
				} else if(response.statusCode === 9){
					Swal.fire(
						unsuccessTitle,
						shortMsj,
						'warning'
					);

				}
				
				
				else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
				
			},
			(error) => {
				console.log(error);
			}
		);
	}
}
