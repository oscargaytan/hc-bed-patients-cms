import { MaterialPreviewModule } from './../../partials/content/general/material-preview/material-preview.module';
import { NgbDropdownModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { TranslateModule } from '@ngx-translate/core';
import {
	MatInputModule,
	MatPaginatorModule,
	MatTableModule,
	MatIconModule,
	MatButtonModule,
	MatOptionModule,
	MatSelectModule,
	MatSortModule
} from '@angular/material';
import { SpecialtiesComponent } from './specialties.component';
// import { UserAddComponent } from './user-add/user-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SpecialtyAddComponent } from './specialty-add/specialty-add.component';
import { SpecialtyEditComponent } from './specialty-edit/specialty-edit.component';
// import { UserEditComponent } from './user-edit/user-edit.component';

const specialtiesRoutes = [
	{
		path: '',
		component: SpecialtiesComponent
	}, {
		path: 'add',
		component: SpecialtyAddComponent
	}, {
		path: 'edit/:specialtyID',
		component: SpecialtyEditComponent
	},
];

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		CoreModule,
		MatPaginatorModule,
		MatTableModule,
		MatInputModule,
		MatIconModule,
		MatButtonModule,
		NgbDropdownModule,
		MatOptionModule,
		MatSortModule,
		MatSelectModule,
		MaterialPreviewModule,
		FormsModule,
		ReactiveFormsModule,
		NgbTabsetModule,
		TranslateModule.forChild(),
		RouterModule.forChild(specialtiesRoutes),
	],
	providers: [],
	declarations: [
		SpecialtiesComponent,
		SpecialtyAddComponent,
		SpecialtyEditComponent
		
	]
})
export class SpecialtiesModule {
}
