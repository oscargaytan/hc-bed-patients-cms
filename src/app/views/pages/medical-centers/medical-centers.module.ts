import { MaterialPreviewModule } from './../../partials/content/general/material-preview/material-preview.module';
import { NgbDropdownModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { TranslateModule } from '@ngx-translate/core';
import {
	MatInputModule,
	MatPaginatorModule,
	MatTableModule,
	MatIconModule,
	MatButtonModule,
	MatOptionModule,
	MatSelectModule,
	MatSortModule
} from '@angular/material';
import { MedicalCentersComponent } from './medical-centers.component';
// import { UserAddComponent } from './user-add/user-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MedicalCenterAddComponent } from './medical-center-add/medical-center-add.component';
import { MedicalCenterEditComponent } from './medical-center-edit/medical-center-edit.component';
// import { SpecialtyAddComponent } from './specialty-add/specialty-add.component';
// import { SpecialtyEditComponent } from './specialty-edit/specialty-edit.component';


const medicalCentersRoutes = [
	{
		path: '',
		component: MedicalCentersComponent
	}, {
		path: 'add',
		component: MedicalCenterAddComponent
	}, {
		path: 'edit/:medicalCenterID',
		component: MedicalCenterEditComponent
	},
];

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		CoreModule,
		MatPaginatorModule,
		MatTableModule,
		MatInputModule,
		MatIconModule,
		MatButtonModule,
		NgbDropdownModule,
		MatOptionModule,
		MatSortModule,
		MatSelectModule,
		MaterialPreviewModule,
		FormsModule,
		ReactiveFormsModule,
		NgbTabsetModule,
		TranslateModule.forChild(),
		RouterModule.forChild(medicalCentersRoutes),
	],
	providers: [],
	declarations: [
		MedicalCentersComponent,
		MedicalCenterAddComponent,
		MedicalCenterEditComponent,
		// SpecialtyAddComponent,
		// SpecialtyEditComponent
		
	]
})
export class MedicalCentersModule {
}
