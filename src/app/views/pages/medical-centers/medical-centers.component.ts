import { TranslateService } from '@ngx-translate/core';
import { ApiRestService } from '../../../network/api-rest.service';
import { ResponseModule } from './../../../models/response/response.module';
import { MedicalCentersModule } from './../../../models/medical-centers/medical-centers.module';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'kt-medical-centers',
  templateUrl: './medical-centers.component.html',
  styleUrls: ['./medical-centers.component.scss']
})
export class MedicalCentersComponent implements OnInit {

  medicalCenters$: Observable<MedicalCentersModule[]>;
  displayedColumns = ['medicalCentersName','medicalCentersAddress','medicalCentersPhone','medicalCentersStatus','actions']; //Aqui poner los nombres de los campos que se van a ver del html
  dataSource: MatTableDataSource<MedicalCentersModule>;
  @ViewChild('usersPage', { read: MatPaginator, static: false }) paginator: MatPaginator;
  @ViewChild('users', { read: MatSort, static: false }) sort: MatSort;

  constructor(private apiRest: ApiRestService, private translateService: TranslateService) { }

  ngOnInit() {
    this.getAllMedicalCenters();
  }

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAllMedicalCenters() {
    this.medicalCenters$ = this.apiRest.getAllMedicalCenters().pipe(
      map((response: ResponseModule): MedicalCentersModule[] => {
        console.log(response);
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((users: MedicalCentersModule[]) => {
        this.dataSource = new MatTableDataSource(users);
        this.configDataTable();
      })
    );
  }

  disableMedicalCenter(medicalCenterID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('Inhabilitar').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('El centro médico será inhabilitado ¿desea continuar con la solicitud?').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('Cancelar').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('Centro médico inhabilitado').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('La información será actualizada.').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        console.log("disable" + medicalCenterID);
        this.apiRest.disableMedicalCenter(medicalCenterID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getAllMedicalCenters();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }

  enableMedicalCenter(medicalCenterID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('Reactivar').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('El centro médico será reactivado ¿desea continuar con la solicitud?').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('Cancelar').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('Centro médico reactivado').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('La información será actulizada.').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        console.log("enable" + medicalCenterID);
        this.apiRest.enableMedicalCenter(medicalCenterID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getAllMedicalCenters();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }

  deleteMedicalCenter(medicalCenterID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('Eliminar').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('El centro médico será eliminado del sistema, ¿desea continuar con la solicitud?').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('Cancelar').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('Centro médico eliminado').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('La información será actulizada.').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        console.log('deleted'+medicalCenterID);
        this.apiRest.deleteMedicalCenter(medicalCenterID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getAllMedicalCenters();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }


}
