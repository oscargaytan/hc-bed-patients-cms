import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalCenterAddComponent } from './medical-center-add.component';

describe('MedicalCenterAddComponent', () => {
  let component: MedicalCenterAddComponent;
  let fixture: ComponentFixture<MedicalCenterAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalCenterAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCenterAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
