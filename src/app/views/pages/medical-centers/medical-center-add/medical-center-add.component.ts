// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'kt-medical-center-add',
//   templateUrl: './medical-center-add.component.html',
//   styleUrls: ['./medical-center-add.component.scss']
// })
// export class MedicalCenterAddComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { ApiRestService } from '../../../../network/api-rest.service';
import { ResponseModule } from './../../../../models/response/response.module';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
	selector: 'kt-medical-center-add',
	templateUrl: './medical-center-add.component.html',
	styleUrls: ['./medical-center-add.component.scss']
})
export class MedicalCenterAddComponent implements OnInit {

	medCenterForm:FormGroup;

	constructor(private apiRest: ApiRestService,
				private fb: FormBuilder,
				private translateService: TranslateService,
				private router: Router) { }

	ngOnInit() {
		this.initForm(); 
  }
  
  initForm() {
	this.medCenterForm = this.fb.group({
		name: ['', Validators.compose(
				[
					Validators.required,
					Validators.minLength(3)
				]
			)
		],
		address: ['', Validators.compose(
				[
					
				]
			)
		],
		phone: ['', Validators.compose(
			[
				
			]
		)
	],
		

	},{});
}

isControlHasError(controlName: string, validationType: string): boolean {
	const control = this.medCenterForm.controls[controlName];
	if (!control) {
		return false;
	}
	const result = control.hasError(validationType) && (control.dirty || control.touched);
	return result;
}


addMedicalCenter() {
		const request = [];

		const controls = this.medCenterForm.controls;

		if (this.medCenterForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
	}



	const medicalCenterName = controls.name.value;
	const medicalCenterAddress = controls.address.value;
    const medicalCenterPhone = controls.phone.value;
		
		this.apiRest.addMedicalCenter(medicalCenterName, medicalCenterAddress, medicalCenterPhone).subscribe(
			(response: ResponseModule) => {
        console.log('apirest ');
        
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				var shortMsj= '';
				this.translateService.get('Error al añadir centro médico').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
        );
        
				this.translateService.get("El centro médico ya está registrado.").subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('El nombre del centro médico es muy corto.').subscribe(
					translate => {
						shortMsj = translate;
					}
				);
				this.translateService.get('Centro médico añadido').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('Se ha añadido el centro médico al sistema.').subscribe(
					translate => {
						successMsj = translate;
					}
        		);
        
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/medical-centers']);
				} else if(response.statusCode === 9){
					Swal.fire(
						unsuccessTitle,
						shortMsj,
						'warning'
					);

				}else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
				
			},
			(error) => {
				console.log(error);
			}
		);
	}
}

