// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'kt-medical-center-edit',
//   templateUrl: './medical-center-edit.component.html',
//   styleUrls: ['./medical-center-edit.component.scss']
// })
// export class MedicalCenterEditComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

import { MedicalCentersModule } from './../../../../models/medical-centers/medical-centers.module';
import { ResponseModule } from './../../../../models/response/response.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiRestService } from './../../../../network/api-rest.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2';


@Component({
  selector: 'kt-medical-center-edit',
  templateUrl: './medical-center-edit.component.html',
  styleUrls: ['./medical-center-edit.component.scss']
})
export class MedicalCenterEditComponent implements OnInit {

  medicalCenterID: number;
  medicalCenterDetail: MedicalCentersModule;

  medCenterForm:FormGroup;
 
  constructor(private apiRest: ApiRestService,
              private translateService: TranslateService,
              private router: Router, 
              private fb: FormBuilder,
              private route: ActivatedRoute) {
    this.medicalCenterID = +route.snapshot.params.medicalCenterID;

    console.log("ID "+this.medicalCenterID)
    
  }

  ngOnInit() {
    this.initForm();
    this.getMedicalCenterDetail(this.medicalCenterID);
  }

  initForm() {
    this.medCenterForm = this.fb.group({
      name: ['', Validators.compose(
          [
            Validators.required,
            Validators.minLength(3)
          ]
        )
      ],
      address: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )
    ],
      phone: ['', Validators.compose(
        [
          
        ]
      )
    ],
    },{});
  }
  
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.medCenterForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  getMedicalCenterDetail(medicalCenterID: number) {
    this.apiRest.getMedicalCenter(medicalCenterID).subscribe(
      (response: ResponseModule) => {
        console.log(this.medCenterForm.controls['name']);
        if (response.statusCode === 0) {
          this.medCenterForm.controls['name'].setValue(response.resultset.medicalCenterName);
          this.medCenterForm.controls['address'].setValue(response.resultset.medicalCenterAddress);
          this.medCenterForm.controls['phone'].setValue(response.resultset.medicalCenterPhone);
          console.log(response);
        }
      }, (error) => {
        
        console.log(error);
      }
    );
  }

  editMedicalCenter() {


    const controls = this.medCenterForm.controls;

		if (this.medCenterForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
      );
      console.log('yyyyyyy');
			return;
	}

    var medicalCenterEdit = new MedicalCentersModule();
    medicalCenterEdit.medicalCenterID = this.medicalCenterID;
    medicalCenterEdit.medicalCenterName = controls.name.value;
    medicalCenterEdit.medicalCenterAddress = controls.address.value;
    medicalCenterEdit.medicalCenterPhone = controls.phone.value;
    console.log("ID "+this.medicalCenterID)

    console.log(medicalCenterEdit);

    

    
    this.apiRest.editMedicalCenter(medicalCenterEdit).subscribe(
      (response: ResponseModule) => {
        var successTitle = '';
        var successMsj = '';
        var unsuccessTitle = '';
        var unsuccessMsj = '';
        var shortMsj = ';'
        this.translateService.get('Error al editar centro médico').subscribe(
          translate => {
            unsuccessTitle = translate;
          }
        );
        this.translateService.get('El centro médico ya está registrado.').subscribe(
          translate => {
            unsuccessMsj = translate;
          }
        );
        this.translateService.get('El nombre del centro médico es muy corto.').subscribe(
					translate => {
						shortMsj = translate;
					}
				);
        this.translateService.get('Centro médico actualizado').subscribe(
          translate => {
            successTitle = translate;
          }
        );
        this.translateService.get('La información será actualizada.').subscribe(
          translate => {
            successMsj = translate;
          }
        );
        console.log("respuesta med");
        console.log(response);
        if (response.statusCode === 0) {
          Swal.fire(
            successTitle,
            successMsj,
            'success'
          );
          this.router.navigate(['/medical-centers']);
          
        } else if(response.statusCode === 9){
					Swal.fire(
						unsuccessTitle,
						shortMsj,
						'warning'
					);

				} else {
          Swal.fire(
            unsuccessTitle,
            unsuccessMsj,
            'warning'
          );
        }
        
      },
      (error) => {
        console.log(error);
        var unsuccessTitle = '';
        this.translateService.get('USERS.EDIT.ALERTS.EDIT_UNSUCCESS_TITLE').subscribe(
          translate => {
            unsuccessTitle = translate;
          }
        );
        Swal.fire(
          unsuccessTitle,
          error,
          'warning'
        );
      }
      
    );
    
  }
  

}
