import { ResponseModule } from './../../../models/response/response.module';
import { ApiRestService } from './../../../network/api-rest.service';
// Angular
import { Component, OnInit } from '@angular/core';
// Services
import { LayoutConfigService, SparklineChartOptions } from '../../../core/_base/layout';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'kt-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };
  usersInfo$: Observable<Array<any>>;

  constructor(private layoutConfigService: LayoutConfigService, private translateService: TranslateService, private apiRest: ApiRestService) {
  }

  ngOnInit(): void {
    this.getUsersInformation();
  }

  getUsersInformation() {
    this.usersInfo$ = this.apiRest.getDashboardUsersInfo().pipe(
      map((response: ResponseModule): Array<any> => {
        let information = [];
        if (response.statusCode === 0) {
          information = [
            {
              name: 'Emprendedores',
              value: response.resultset.enterpreneurs,
              extra: {
                code: 'de'
              }
            },
            {
              name: 'Inversionistas',
              value: response.resultset.investors,
              extra: {
                code: 'it'
              }
            }
          ];
        } else {
          information = [
            {
              name: 'Emprendedores',
              value: 0,
              extra: {
                code: 'de'
              }
            },
            {
              name: 'Inversionistas',
              value: 0,
              extra: {
                code: 'it'
              }
            }
          ];
        }
        return information;
      })
    );
  }
}
