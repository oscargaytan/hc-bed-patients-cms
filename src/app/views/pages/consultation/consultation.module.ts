import { MaterialPreviewModule } from './../../partials/content/general/material-preview/material-preview.module';
import { NgbDropdownModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatOptionModule,
  MatSelectModule,
  MatSortModule
} from '@angular/material';
import { ConsultationComponent } from './consultation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsultationAddComponent } from './consultation-add/consultation-add.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { ConsultationDetailComponent } from './consultation-detail/consultation-detail.component';
// import { DoctorAddComponent } from './doctor-add/doctor-add.component';
// import { DoctorEditComponent } from './doctor-edit/doctor-edit.component';

const userRoutes = [
  {
    path: '',
    component: ConsultationComponent
  }
  , {
    path: 'add/:consultationID',
    component: ConsultationAddComponent
  },
  {
    path: 'detail/:consultationID',
    component: ConsultationDetailComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PartialsModule,
    CoreModule,
    MatPaginatorModule,
    MatTableModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    NgbDropdownModule,
    MatOptionModule,
    MatSortModule,
    MatSelectModule,
    MatExpansionModule,
    MaterialPreviewModule,
    FormsModule,
    ReactiveFormsModule,
    NgbTabsetModule,
    TranslateModule.forChild(),
    RouterModule.forChild(userRoutes),
  ],
  providers: [],
  declarations: [
    ConsultationComponent,
    ConsultationAddComponent,
    ConsultationDetailComponent
    // DoctorAddComponent,
    // DoctorEditComponent,
  ]
})
export class ConsultationModule {
}
