import { TranslateService } from '@ngx-translate/core';
import { ApiRestService } from '../../../../network/api-rest.service';
import { ResponseModule } from './../../../../models/response/response.module';
import { ConsultationModule } from 'src/app/models/consultation/consultation.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'kt-consultation-add',
  templateUrl: './consultation-add.component.html',
  styleUrls: ['./consultation-add.component.scss']
})
export class ConsultationAddComponent implements OnInit {
  resultsetArray = [];
  consultationArray = [];
  fileArray = [];
  data: any;
  dataDetail: any;
  patientGender = '';
  consultationID: number;
  patientID: number;
  bruhid:1;
  fecha = '';
  doctor = '';
  medicalCenter = '';
  consultationForm:FormGroup;
  dataSource: MatTableDataSource<ConsultationModule>;

  constructor(private apiRest: ApiRestService,
              private translateService: TranslateService,
              private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute) { 
  this.consultationID = +route.snapshot.params.consultationID;
              }

  ngOnInit() {
   
    this.initForm();

    this.getMedicalConsultation(this.consultationID);
  }

  initForm() {
    this.consultationForm = this.fb.group({
      name: [{value: '', disabled: true}],
      lname: [{value: '', disabled: true}],
      email: [{value: '', disabled: true}],
      bdate: [{value: '', disabled: true}],
      gender: [{value: '', disabled: true}],
      height: [{value: '', disabled: true}],
      weight: [{value: '', disabled: true}],
      bloodType: [{value: '', disabled: true}],
      allergies: [{value: '', disabled: true}],
      bloodPressure: [{value: '', disabled: true}],
      date: [' '],
      vitals: [' '],
      reason: [' '],
      summary: [' '],
      diagnosis: [' '],
      results: [' '],
      treatment: [' '],
      prognosis: [' '],
      
      
    },{});
  }
  
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.consultationForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getMedicalConsultation(consultationID:number){

    console.log("bruh1")
    var fecha ='';
    var hora = '';
    //ar doctor = 'asdasd';

        this.apiRest.getMedicalConsultation(consultationID).subscribe(
          (response: ResponseModule)=>{
            console.log("bruh2")

            if(response.statusCode === 0){

              this.data = response.resultset[0];
              this.dataDetail = response.resultset[0].consultationDetail;
              if(this.data['patientGender'] === 0){
                this.patientGender = 'Mujer';
              } 
              if(this.data['patientGender'] === 1){
                this.patientGender = 'Hombre';
              }

              this.consultationForm.controls['name'].setValue(response.resultset[0]['patientName']);
              this.consultationForm.controls['lname'].setValue(response.resultset[0]['patientLName']);
              this.consultationForm.controls['bdate'].setValue(response.resultset[0]['patientBDate']);
              this.consultationForm.controls['email'].setValue(response.resultset[0]['patientEmail']);
              
              if(response.resultset[0]['patientGender'] === 0){
                this.consultationForm.controls['gender'].setValue('Mujer');
              } 
              if(response.resultset[0]['patientGender'] === 1){
                this.consultationForm.controls['gender'].setValue('Hombre');
              }
              this.consultationForm.controls['height'].setValue(response.resultset[0]['patientHeight'] + ' cm');
              this.consultationForm.controls['weight'].setValue(response.resultset[0]['patientWeight'] + ' kg');
              this.consultationForm.controls['bloodType'].setValue(response.resultset[0]['patientBloodType']);
              this.consultationForm.controls['allergies'].setValue(response.resultset[0]['patientAllergies']);
              this.consultationForm.controls['bloodPressure'].setValue(response.resultset[0]['patientBloodPressure']);

              //Fecha de la consulta

              fecha = new Date(response.resultset[0].consultationDetail.consultationDate).toLocaleDateString('es-MX', {
                timeZone: 'America/Monterrey',
                year: 'numeric',
                month: '2-digit',
                day: '2-digit'
              });

              hora = new Date(response.resultset[0].consultationDetail.consultationDate).toLocaleTimeString('en-US', {
                hour: '2-digit',
                minute: '2-digit'
              });

              // this.doctor = response.resultset[0].generalDetail.generalDoctor;
              // this.medicalCenter = response.resultset[0].generalDetail.medicalCenter;


              console.log("DATOSS")
              console.log(response.resultset)
              // this.consultationForm.controls['date'].setValue(fecha +' - '+ hora);
              this.consultationForm.controls['vitals'].setValue(response.resultset[0].specialistDetail.consultationVitalSigns);
              this.consultationForm.controls['reason'].setValue(response.resultset[0].specialistDetail.consultationReason);
              this.consultationForm.controls['summary'].setValue(response.resultset[0].specialistDetail.consultationSummary);
              this.consultationForm.controls['diagnosis'].setValue(response.resultset[0].specialistDetail.consultationDiagnosis);
              this.consultationForm.controls['results'].setValue(response.resultset[0].specialistDetail.consultationResults);
              this.consultationForm.controls['prognosis'].setValue(response.resultset[0].specialistDetail.consultationPrognosis);
              this.consultationForm.controls['treatment'].setValue(response.resultset[0].specialistDetail.consultationTreatment);

              
              console.log("bruh3")
              
              console.log(response);


              


            }
            
          }, (error) => {
            
            console.log(error);
          }
          
        )
  }

  addMedicalConsultation(){
    const info = this.apiRest.getDataToken();
    console.log(info)
    const controls = this.consultationForm.controls;

		if (this.consultationForm.invalid) {
        Object.keys(controls).forEach(controlName =>
          controls[controlName].markAsTouched()
        );
        return;
    }
    
    const vitalSigns = controls.vitals.value;
		const reason = controls.reason.value;
		const summary = controls.summary.value;
		const diagnosis = controls.diagnosis.value;
    const results = controls.results.value;
    const prognosis = controls.prognosis.value;
    const treatment = controls.treatment.value;
    

    

    //console.log(data)
    this.apiRest.getMedicalConsultationDetail(this.consultationID).subscribe(
      (response: ResponseModule) =>{
        
        var detailID = response.resultset[1].pk_medical_consultation_detail;
        var consultationID = response.resultset[0].fk_medical_consultation;
        var doctorEmail = response.resultset[0].doctorsMedCenters.doctor_medical_center_email;
        var doctorName = response.resultset[0].doctorsMedCenters.doctors.doctor_name +' '+response.resultset[0].doctorsMedCenters.doctors.doctor_name;
        var consultationDetailDate = (response.resultset[1].consultation_date);
        let data={
          detailID,
          consultationID,
          consultationDetailDate,
          vitalSigns,
          reason,
          summary,
          diagnosis,
          results,
          prognosis,
          treatment,
          doctorName,
          doctorEmail
        };

        
        this.apiRest.updateMedicalConsultation(data).subscribe(
          (response: ResponseModule) =>{
            
        console.log(data)
            var successTitle = '';
            var successMsj = '';
            var unsuccessTitle = '';
            var unsuccessMsj = '';
            var shortMsj= '';
            this.translateService.get('Error al añádir doctor').subscribe(
              translate => {
                unsuccessTitle = translate;
              }
            );
            this.translateService.get('Ese correo electrónico ya está registrado.').subscribe(
              translate => {
                unsuccessMsj = translate;
              }
            );
            
            this.translateService.get('Prediagnóstico enviado').subscribe(
              translate => {
                successTitle = translate;
              }
            );
            this.translateService.get('Se enviará el prediagnóstico al doctor.').subscribe(
              translate => {
                successMsj = translate;
              }
            );
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.router.navigate(['/consultations']);
            } else {
              Swal.fire(
                unsuccessTitle,
                unsuccessMsj,
                'warning'
              );
            }
            console.log(response);
            
            this.apiRest.updateMedicalConsultationStatus(this.consultationID, 5).subscribe();
    
          },
          (error) => {
            console.log(error);
          }
        )
      }
    );

    
    
  }

  endMsj(consultationID: number){

    Swal.fire({
      title: '¿Seguro que quiere enviar la consulta?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Enviar',
      confirmButtonColor: '#77D2D3',
      cancelButtonText: 'Cancelar'
      
    }).then((result) => {
      if(result.value){
        this.addMedicalConsultation();
        //this.apiRest.updateMedicalConsultationStatus(consultationID, 5).subscribe();
        
      } else{
        
        
      }
      // if (result.value) {
      //   this.apiRest.disableUser(userID).subscribe(
      //     (response: ResponseModule) => {
      //       if (response.statusCode === 0) {
      //         Swal.fire(
      //           successTitle,
      //           successMsj,
      //           'success'
      //         );
      //         this.getUsersList();
      //       } else {

      //       }
      //     },
      //     (error) => {
      //       console.log(error);
      //     }
      //   );
      // }
    });
  }

}
