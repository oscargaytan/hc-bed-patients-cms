import { TranslateService } from '@ngx-translate/core';
import { ApiRestService } from '../../../../network/api-rest.service';
import { ResponseModule } from './../../../../models/response/response.module';
import { ConsultationModule } from 'src/app/models/consultation/consultation.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Component, OnInit, ViewChild, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { MatSnackBar} from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './../../../../app.component';

//import { MatSnackBar} from '@angular/material';

import { NotifierService } from "angular-notifier";
import { WebSocketService } from './../../../../web-socket.service'
@Component({
  selector: 'kt-consultation-detail',
  templateUrl: './consultation-detail.component.html',
  styleUrls: ['./consultation-detail.component.scss']
})
export class ConsultationDetailComponent implements OnInit {
  panelOpenState = false;
  historyArray = [];
  consultationArray = [];
  fileArray = [];
  data: any;
  dataDetail: any;
  patientGender = '';
  patientAge = '';
  consultationID: number;
  patientID: number;
  fecha = '';
  doctor = '';
  medicalCenter = '';
  dataID = 1;
  private readonly notifier: NotifierService;

  medicalConsultationID = '';




  consultationForm:FormGroup;
  dataSource: MatTableDataSource<ConsultationModule>;
  constructor(private apiRest: ApiRestService,
              private translateService: TranslateService,
              private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private cd: ChangeDetectorRef,
              notifierService: NotifierService,
              public snackbar: MatSnackBar,
              private webSocketService: WebSocketService) {
  this.consultationID = +route.snapshot.params.consultationID;
  this.notifier = notifierService;
  
              }

  ngOnInit() {
    
    this.getMedicalConsultation(this.consultationID);
  }
 

  getMedicalConsultation(consultationID:number){

    var fecha ='';
    var hora = '';
    
        this.apiRest.getMedicalConsultation(consultationID).subscribe(
          (response: ResponseModule)=>{

            if(response.statusCode === 0){
              this.data = response.resultset[0];
              this.dataDetail = response.resultset[0].consultationDetail;

              if(this.data['patientGender'] === 0){
                this.patientGender = 'Femenino';
              } 
              if(this.data['patientGender'] === 1){
                this.patientGender = 'Masculino';
              }
              
             

              //Fecha de la consulta

              fecha = new Date(response.resultset[0].consultationDetail.consultationDate).toLocaleDateString('es-MX', {
                timeZone: 'America/Monterrey',
                year: 'numeric',
                month: '2-digit',
                day: '2-digit'
              });

              hora = new Date(response.resultset[0].consultationDetail.consultationDate).toLocaleTimeString('en-US', {
                hour: '2-digit',
                minute: '2-digit'
              });


              this.cd.detectChanges();


              this.apiRest.getMedicalHistory(response.resultset[0].patientID).subscribe(
                (response: ResponseModule) => {

                  if (response.statusCode === 0) {

                    console.log(response)
                    
                    this.historyArray = response.resultset;
                    this.historyArray.forEach(element => {
                      if(element['medicalFile']){
                        element['medicalFile']['date'] = new Date(element['medicalFile']['date']).toLocaleDateString('es-MX', {
                          timeZone: 'America/Monterrey',
                          year: 'numeric',
                          month: '2-digit',
                          day: '2-digit'
                        });
                      }
                      if(element['medicalConsultation']){
                        element['medicalConsultation']['date'] = new Date(element['medicalConsultation']['date']).toLocaleDateString('es-MX', {
                          timeZone: 'America/Monterrey',
                          year: 'numeric',
                          month: '2-digit',
                          day: '2-digit'
                        });
                      }
          
                    });
                    this.historyArray.forEach(element => {
                      if(element['medicalFile']){
                        this.fileArray.push(element);
                      }
                      if(element['medicalConsultation']){
                        this.consultationArray.push(element);
                      }
          
                    });

                    this.cd.detectChanges();
          
                    this.consultationArray.shift();

                    
                  }
                }, (error) => {
                  
                  console.log(error);
                }
              );


            }
            
          }, (error) => {
            
            console.log(error);
          }
          
        )
        //this.cd.detectChanges();
  }

  startDiagnostic(consultationID:any){
    //Cuando le das click en Crear prediagnostico, cambia el status a 4
    this.apiRest.updateMedicalConsultationStatus(consultationID, 4).subscribe();

  }

  cancelConsultation(consultationID:number){
    this.apiRest.getMedicalConsultation(consultationID).subscribe(
      (response: ResponseModule)=>{
        const info = this.apiRest.getDataToken();
        const userID = info.userID;

        this.apiRest.cancelMedicalConsultation(consultationID, userID).subscribe(
          (response: ResponseModule) =>{
            if(response.statusCode == 0){
              console.log("cancelada")
              this.router.navigate(['/consultations']); 
            setTimeout(function(){ window.location.reload();
            });
            }
          } , (error) => {
                  
            console.log(error);
          }
        )

      }, (error) => {
            
        console.log(error);
      }
      
    );
      
    
  }

  notificacion(){ //solo para probar el socket
    console.log("notificacion")
    this.webSocketService.emit('sendToServer', 'enviar notificacion');
  }




}
