import { TranslateService } from '@ngx-translate/core';
import { ApiRestService } from '../../../network/api-rest.service';
import { ResponseModule } from './../../../models/response/response.module';
import { ConsultationModule } from 'src/app/models/consultation/consultation.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';


@Component({
  selector: 'kt-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.scss']
})
export class ConsultationComponent implements OnInit {
  consultations$: Observable<ConsultationModule[]>;
  displayedColumns = ['consultationID', 'consultationSpecialty', 'location', 'consultationStatus'];
  makeDisabled = false;
  consultationStatus = 2;
  userID = 0;
  historyArray = [];
  consultationArray = [];
  fileArray = [];
  data = {};
  dataDetail = {};
  patientGender = '';
  consultationID: number;
  patientID: number;
  fecha = '';
  doctor = '';
  medicalCenter = '';
  consultationForm:FormGroup;
  dataSource: MatTableDataSource<ConsultationModule>;
  @ViewChild('usersPage', { read: MatPaginator, static: false }) paginator: MatPaginator;
  @ViewChild('users', { read: MatSort, static: false }) sort: MatSort;
  constructor(private apiRest: ApiRestService,
              private translateService: TranslateService,
              private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute) { 
  this.consultationID = +route.snapshot.params.consultationID;
              }

  ngOnInit() {
      this.getAllMedicalConsultations(); 
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAllMedicalConsultations(){
    const info = this.apiRest.getDataToken();
    this.userID = info.userID;
    console.log(this.userID);
    this.consultations$ = this.apiRest.getAllMedicalConsultations().pipe(
      map((response: ResponseModule): ConsultationModule[] => {
        if (response.statusCode === 0) {
          
          console.log(response.resultset)
          console.log(info);
          response.resultset.forEach(consultation =>{
            
            if(consultation.specialistDoctorID){
              if((consultation.consultationStatus === 3 && consultation.specialistDoctorID === info.userID)||(consultation.consultationStatus === 4 && consultation.specialistDoctorID === info.userID)){
                this.makeDisabled = true;
              } 
            }
          });
          
          return response.resultset;
        } else {
          
          return [];
        }
      }),
      tap((users: ConsultationModule[]) => {
        this.dataSource = new MatTableDataSource(users);
        this.configDataTable();
      })

    )

  }

  startConsultation(consultationID: number){    //Inicia la consulta cuando se clickea el boton de Atender
    this.apiRest.getMedicalConsultation(consultationID).subscribe((response: ResponseModule)=>{
      console.log(response.resultset[0].medicalConsultationID);
      const info = this.apiRest.getDataToken();
      console.log(info)
      const vitalSigns = '';
      const reason = '';
      const summary = '';
      const diagnosis = ''; 
      const results = '';
      const treatment = '';
      const user = info.userID;

      let data={
        vitalSigns,
        reason,
        summary,
        diagnosis,
        results,
        treatment,
        user
      };
      //Se crea el registro de consultaDetalle del doc especialista en la BD y se pone en status 3 la consulta
      this.apiRest.addMedicalConsultation(consultationID, data).subscribe(); //Crear consulta detalle
      this.apiRest.updateMedicalConsultationStatus(consultationID, 3).subscribe(); //Poner en status 3
      this.makeDisabled = true;
      }, (error) => {
            
      console.log(error);
    });

  }

}
