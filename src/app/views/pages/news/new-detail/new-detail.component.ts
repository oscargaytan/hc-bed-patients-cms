import { ResponseModule } from './../../../../models/response/response.module';
import { NewModule } from '../../../../models/new/new.module';
import { ApiRestService } from './../../../../network/api-rest.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'kt-new-detail',
  templateUrl: './new-detail.component.html',
  styleUrls: ['./new-detail.component.scss']
})
export class NewDetailComponent implements OnInit {

  newID: number;
  newDetail$: Observable<NewModule> = null;
  constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router, private route: ActivatedRoute) {
    this.newID = +route.snapshot.params.newID;
  }

  ngOnInit() {
    this.getNewDetail();
  }

  publishNew() {
    this.apiRest.publishNew(this.newID).subscribe(
      (response: ResponseModule) => {
        if (response.statusCode === 0) {
          this.getNewDetail();
          Swal.fire(
            response.statusMessage,
            '',
            'success'
          )
        } else {
          Swal.fire(
            response.statusMessage,
            '',
            'warning'
          )
        }
      },
      (error) => {
        Swal.fire(
          error,
          '',
          'warning'
        )
      }
    );
  }

  removeNew() {
    this.apiRest.removeNew(this.newID).subscribe(
      (response: ResponseModule) => {
        if (response.statusCode === 0) {
          this.getNewDetail();
          Swal.fire(
            response.statusMessage,
            '',
            'success'
          )
        } else {
          Swal.fire(
            response.statusMessage,
            '',
            'warning'
          )
        }
      },
      (error) => {
        Swal.fire(
          error,
          '',
          'warning'
        )
      }
    );
  }

  getNewDetail() {
    // Observable para listar información ó refrescar datos
    this.newDetail$ = this.apiRest.getNewDetail(this.newID).pipe(map(this.formatResponse));
  }

  private formatResponse(response: ResponseModule): NewModule {
    if (response.statusCode === 0) {
      return response.resultset;
    } else {
      console.log(response.statusMessage);
      return new NewModule();
    }
  }

}
