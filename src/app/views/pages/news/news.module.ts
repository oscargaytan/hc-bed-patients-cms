// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialPreviewModule } from './../../partials/content/general/material-preview/material-preview.module';
import { NgbDropdownModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatOptionModule,
  MatMenuModule,
  MatCardModule,
  MatSelectModule,
  MatSortModule,
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewsComponent } from './news.component';
import { NewDetailComponent } from './new-detail/new-detail.component';
import { NewAddComponent } from './new-add/new-add.component';
import { NewEditComponent } from './new-edit/new-edit.component';
import { FileUploadModule } from 'ng2-file-upload';
import { NgImageSliderModule } from 'ng-image-slider';
import { SlideshowModule } from 'ng-simple-slideshow';


const eventRoutes = [
  {
    path: '',
    component: NewsComponent,
  }, {
    path: 'detail/:newID',
    component: NewDetailComponent
  }, {
    path: 'add',
    component: NewAddComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    PartialsModule,
    CoreModule,
    MatPaginatorModule,
    MatTableModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    NgbDropdownModule,
    MatOptionModule,
    MatCardModule,
    MatSelectModule,
    MatMenuModule,
    MaterialPreviewModule,
    FormsModule,
    MatSortModule,
    NgbTabsetModule,
    FileUploadModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild(eventRoutes),
    NgImageSliderModule,
    SlideshowModule
  ],
  providers: [

  ],
  declarations: [
    NewsComponent,
    NewDetailComponent,
    NewAddComponent,
    NewEditComponent
  ]
})
export class NewsModule { }
