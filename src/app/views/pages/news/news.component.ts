import { ResponseModule } from './../../../models/response/response.module';
import { NewModule } from '../../../models/new/new.module';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ApiRestService } from './../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'kt-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  news$: Observable<NewModule[]> = null;
  displayedColumns = ['newTitle', 'newBody', 'newDate', 'actions'];
  dataSource: MatTableDataSource<NewModule>;
  @ViewChild('newsPage', { read: MatPaginator, static: false }) paginator: MatPaginator;
  @ViewChild('newsSort', { read: MatSort, static: false }) sort: MatSort;

  newsPending$: Observable<NewModule[]> = null;
  displayedColumnsRequest = ['newTitle', 'newBody', 'newDate', 'actions'];
  dataSourceRequest: MatTableDataSource<NewModule>;
  @ViewChild('newsAllPage', { read: MatPaginator, static: false }) paginatorNew: MatPaginator;
  @ViewChild('newsAllSort', { read: MatSort, static: false }) sortNew: MatSort;

  constructor(private apiRest: ApiRestService, private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.getApprovedNews();
  }

  beforeChange(event: NgbTabChangeEvent) {
    if (event.nextId === 'tab-user-list') {
      this.getApprovedNews();
    } else if (event.nextId === 'tab-user-request') {
      this.getAllNews();
    }
  }

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getApprovedNews() {
    this.news$ = this.apiRest.getApprovedNews().pipe(
      map((response: ResponseModule): NewModule[] => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((news: NewModule[]) => {
        this.dataSource = new MatTableDataSource(news);
        this.configDataTable();
      })
    );
  }

  approveNew(newID: number) {
    var confirmButton = '';
    var confirmMessage = '';
    var successTitle = '';
    var successMsj = '';
    var cancelBtn = '';
    this.translateService.get('NEWS.INDEX.ALERTS.APPROVE').subscribe(
      translate => {
        confirmButton = translate;
      }
    );
    this.translateService.get('NEWS.INDEX.ALERTS.APPROVE_MESSAGE').subscribe(
      translate => {
        confirmMessage = translate;
      }
    );
    this.translateService.get('NEWS.INDEX.ALERTS.APPROVE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('NEWS.INDEX.ALERTS.APPROVE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );

    Swal.fire({
      title: confirmMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmButton,
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.publishNew(newID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              this.getAllNews();
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              )
            } else {
              Swal.fire(
                response.statusMessage,
                '',
                'warning'
              )
            }
          }, (error) => {
            console.log('Error en ws', error);
          }
        );
      }
    });
  }


  revokeNew(newID: number) {
    var confirmButton = '';
    var confirmMessage = '';
    var successTitle = '';
    var successMsj = '';
    var cancelBtn = '';
    this.translateService.get('NEWS.INDEX.ALERTS.DISABLE').subscribe(
      translate => {
        confirmButton = translate;
      }
    );
    this.translateService.get('NEWS.INDEX.ALERTS.DISABLE_MESSAGE').subscribe(
      translate => {
        confirmMessage = translate;
      }
    );
    this.translateService.get('NEWS.INDEX.ALERTS.DISABLE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('NEWS.INDEX.ALERTS.DISABLE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );

    Swal.fire({
      title: confirmMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmButton,
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.removeNew(newID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              this.getApprovedNews();
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              )
            } else {
              Swal.fire(
                response.statusMessage,
                '',
                'warning'
              )
            }
          }, (error) => {
            console.log('Error en ws', error);
          }
        );
      }
    });
  }

  deleteNew(newID: number) {
    var confirmButton = '';
    var confirmMessage = '';
    var successTitle = '';
    var successMsj = '';
    var cancelBtn = '';
    this.translateService.get('NEWS.INDEX.ALERTS.DELETE').subscribe(
      translate => {
        confirmButton = translate;
      }
    );
    this.translateService.get('NEWS.INDEX.ALERTS.DELETE_MESSAGE').subscribe(
      translate => {
        confirmMessage = translate;
      }
    );
    this.translateService.get('NEWS.INDEX.ALERTS.DELETE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('NEWS.INDEX.ALERTS.DELETE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );

    Swal.fire({
      title: confirmMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmButton,
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.deleteNew(newID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              this.getApprovedNews();
              this.getAllNews();
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
            } else {
              Swal.fire(
                response.statusMessage,
                '',
                'warning'
              );
            }
          }, (error) => {
            console.log('Error en ws', error);
          }
        );
      }
    });
  }

  getAllNews() {
    this.newsPending$ = this.apiRest.getAllNews().pipe(
      map((response: ResponseModule): NewModule[] => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((news: NewModule[]) => {
        this.dataSourceRequest = new MatTableDataSource(news);
        this.configDataTableRequest();
      })
    );
  }

  applyFilterRequest(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSourceRequest.filter = filterValue;
  }

  configDataTableRequest() {
    this.dataSourceRequest.paginator = this.paginatorNew;
    this.dataSourceRequest.sort = this.sortNew;
  }

}
