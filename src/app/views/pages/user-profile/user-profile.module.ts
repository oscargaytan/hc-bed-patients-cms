// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialPreviewModule } from './../../partials/content/general/material-preview/material-preview.module';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatOptionModule,
  MatMenuModule,
  MatTabsModule,
  MatSelectModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserProfileComponent } from './user-profile.component';
import { UserProfilePasswordComponent } from './user-profile-password/user-profile-password.component';


const eventRoutes = [
  {
    path: '',
    component: UserProfileComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    PartialsModule,
    CoreModule,
    MatPaginatorModule,
    MatTableModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatTabsModule,
    MatButtonModule,
    NgbDropdownModule,
    MatOptionModule,
    MatSelectModule,
    MaterialPreviewModule,
    FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild(eventRoutes),
    ReactiveFormsModule
  ],
  entryComponents: [
  ],
  providers: [],
  declarations: [
    UserProfileComponent,
    UserProfilePasswordComponent,
  ]
})
export class UserProfileModule { }
