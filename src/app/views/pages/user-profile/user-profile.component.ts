// Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// Layout
import { LayoutConfigService } from './../../../core/_base/layout/services/layout-config.service';
import { LayoutUtilsService, MessageType } from './../../../core/_base/crud';
import { UserProfileModule } from '../../../models/user-profile/user-profile.module';
import { ApiRestService } from '../../../network/api-rest.service';
import { AdminUserModule } from '../../../models/admin-user/admin-user.module';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2';
import { ResponseModule } from '../../../models/response/response.module';
import { UserModule } from 'src/app/models/user/user.module';

//import { UserProfileXComponent } from './../../partials/layout/topbar/user-profile/user-profile.component';

@Component({
  selector: 'kt-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  selectedTab = 0;
  userTypes = [];
  userType1 = '';
  user: UserProfileModule;
  userDetail: UserModule;
  userForm: FormGroup;
  constructor(private router: Router,
              private layoutUtilsService: LayoutUtilsService,
              private apiRest: ApiRestService,
              private fb: FormBuilder,
              private translateService: TranslateService) {
    this.userTypes = [{
      'idType': 1,
      'name': 'CATALOGS.USER_TYPE.ADMINISTRATOR'
    }, {
      'idType': 2,
      'name': 'CATALOGS.USER_TYPE.INVESTOR'
    }];
  }

  

	/**
	 * On init
	 */
  ngOnInit() {
    this.initForm();
    this.getUserInfo();
  }

  ngOnDestroy() {
  }

  resetUser() {
    this.getUserInfo();
  }

  initForm() {
    this.userForm = this.fb.group({
      name: ['', Validators.compose(
          [
            Validators.required,
            Validators.minLength(3)
          ]
        )
      ],
      lname: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )
    ],
      email: ['', Validators.compose(
        [
          Validators.required,
            Validators.minLength(3)
        ]
      )
    ],
      type: ['', Validators.compose(
        [
          Validators.required,
            Validators.minLength(3)
        ]
      )
    ],
      date: ['', Validators.compose(
        [
          Validators.required,
            Validators.minLength(3)
        ]
      )
    ],
    },{});
  }
  
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.userForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }



  getUserInfo() {
    this.user = this.apiRest.getDataToken();
    this.user.userCreatedString = new Date(this.user.userCreated).toLocaleDateString('es-MX', {
      timeZone: 'America/Monterrey',
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    });

    console.log("====???")
    console.log(this.user);

    this.userDetail = {
      userID: 1,
      userName: '',
      userLName: '',
      userEmail: '',
      userType: 1
    };

    

    this.getUserDetail(this.user.userID);

    
  }

  getUserDetail(userID: number){
    if (this.user.userType === 1) {
      this.userType1 = 'Administrador';
    };
    
    if(this.user.userType === 2){
      this.userType1 = 'Doctor Especialista';
    }
    this.apiRest.getUser(userID).subscribe(
      (response: ResponseModule) => {
        if (response.statusCode === 0) {
          this.userDetail = response.resultset;
          this.userForm.controls['name'].setValue(response.resultset.userName);
          this.userForm.controls['lname'].setValue(response.resultset.userLName);
          this.userForm.controls['email'].setValue(response.resultset.userEmail);
          this.userForm.controls['email'].disable();
          this.userForm.controls['type'].setValue(this.userType1);
          this.userForm.controls['type'].disable();
          this.userForm.controls['date'].setValue(this.user.userCreatedString);
          this.userForm.controls['date'].disable();
        }
      }
    );
  }

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
  
  editUser(form: NgForm) {
    const controls = this.userForm.controls;

    if (this.userForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
      );
			return;
	}
    
    
    console.log("editado")
    const user: UserProfileModule = this.apiRest.getDataToken();
    const tempUser = new UserModule();
    tempUser.userID = user.userID;
    tempUser.userName = controls.name.value;
    tempUser.userLName = controls.lname.value;
    tempUser.userEmail = user.userEmail;
    tempUser.userType = user.userType;
    this.apiRest.editUser2(tempUser).subscribe(
      (response: ResponseModule) => {
        if (response.statusCode === 0) {
          const token = response.resultset.auth;
          
          // Almacenamiento de jwt
      
          console.log("------")
          console.log(token);
          this.apiRest.setToken(token);
          console.log(response)
          //window.location.reload();
          //this.router.navigate(['/']);
          var successTitle = '';
          var successMsj = '';
          this.translateService.get('PROFILE.DETAIL.ALERTS.SUCCESS_TITLE').subscribe(
            translate => {
              successTitle = translate;
            }
          );
          this.translateService.get('PROFILE.DETAIL.ALERTS.SUCCESS_MSJ').subscribe(
            translate => {
              successMsj = translate;
            }
          );
          Swal.fire({
            title: successTitle,
            text: successMsj,
            type: 'success',
          }).then((result) => {
            
            
            console.log("CAMBIAR")
            //Volver a dashboard
            this.router.navigate(['/dashboard']); 
            setTimeout(function(){ window.location.reload();
            });
            //window.location.reload(); // Volver a la misma página
            
            
          });
        } else {
          var unsuccessTitle = '';
          var unsuccessMsj = '';
          this.translateService.get('PROFILE.DETAIL.ALERTS.ERROR_IN_UPDATE_TITLE').subscribe(
            translate => {
              unsuccessTitle = translate;
            }
          );
          this.translateService.get('PROFILE.DETAIL.ALERTS.ERROR_IN_UPDATE_MSJ').subscribe(
            translate => {
              unsuccessMsj = translate;
            }
          );
          Swal.fire(
            unsuccessTitle,
            unsuccessMsj,
            'warning'
          );
        }
      },
      (error) => {
        var unsuccessTitle = '';
        var unsuccessMsj = '';
        this.translateService.get('PROFILE.DETAIL.ALERTS.SERVER_ERROR_TITLE').subscribe(
          translate => {
            unsuccessTitle = translate;
          }
        );
        this.translateService.get('PROFILE.DETAIL.ALERTS.SERVER_ERROR_MSJ').subscribe(
          translate => {
            unsuccessMsj = translate;
          }
        );
        Swal.fire(
          unsuccessTitle,
          unsuccessMsj,
          'warning'
        );
      }
    );
    
  }


}
