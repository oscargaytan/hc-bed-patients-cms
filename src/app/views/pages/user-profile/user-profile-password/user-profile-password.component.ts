import { ApiRestService } from './../../../../network/api-rest.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { ResponseModule } from './../../../../models/response/response.module';

import { UserProfileModule } from '../../../../models/user-profile/user-profile.module';

@Component({
  selector: 'kt-user-profile-password',
  templateUrl: './user-profile-password.component.html',
  styleUrls: ['./user-profile-password.component.scss']
})
export class UserProfilePasswordComponent implements OnInit {
  user: UserProfileModule;
  userPassForm: FormGroup;

  constructor(private apiRest: ApiRestService,
              private fb: FormBuilder,
              private translateService: TranslateService) { }

  ngOnInit() {
    this.initForm();
    this.getUserInfo();
  }

  initForm() {
    this.userPassForm = this.fb.group({
      old: ['', Validators.compose(
          [
            Validators.required
            
          ]
        )
      ],
      new: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(8)
        ]
      )
    ],
      newCheck: ['', Validators.compose(
        [
          Validators.required,
            Validators.minLength(8)
        ]
      )
    ]
    },{});
  }
  
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.userPassForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  getUserInfo() {
    this.user = this.apiRest.getDataToken();
    this.user.userCreatedString = new Date(this.user.userCreated).toLocaleDateString('es-MX', {
      timeZone: 'America/Monterrey',
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    });
    console.log("EEEEEE");
    console.log(this.user);
  }

  changePassword() {
    const controls = this.userPassForm.controls;

    if (this.userPassForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
      );
			return;
	}
    
    const userID = this.user.userID;
    console.log(userID);
    const OldPass = controls.old.value;
    const NewPass = controls.new.value;
    const NewPassCheck = controls.newCheck.value;
    if (NewPass === NewPassCheck) {
      this.apiRest.editUserPass(userID, OldPass, NewPass).subscribe(
        (response: ResponseModule) => {
          if (response.statusCode === 0) {
            var successTitle = '';
            var successMsj = '';
            this.translateService.get('PROFILE.CHANGE_PASSWORD.ALERTS.SUCCESS_TITLE').subscribe(
              translate => {
                successTitle = translate;
              }
            );
            this.translateService.get('PROFILE.CHANGE_PASSWORD.ALERTS.SUCCESS_MSJ').subscribe(
              translate => {
                successMsj = translate;
              }
            );
            Swal.fire(
              successTitle,
              successMsj,
              'warning'
            );
          } else {
            var errTitle = '';
            var errMsj = '';
            this.translateService.get('PROFILE.CHANGE_PASSWORD.ALERTS.PASSWORD_INVALID_TITLE').subscribe(
              translate => {
                errTitle = translate;
              }
            );
            this.translateService.get('PROFILE.CHANGE_PASSWORD.ALERTS.PASSWORD_INVALID_MSJ').subscribe(
              translate => {
                errMsj = translate;
              }
            );
            Swal.fire(
              errTitle,
              errMsj,
              'warning'
            );
          }
        },
        (error) => {
          var serverTitle = '';
          var serverMsj = '';
          this.translateService.get('PROFILE.CHANGE_PASSWORD.ALERTS.SERVER_ERROR_TITLE').subscribe(
            translate => {
              serverTitle = translate;
            }
          );
          this.translateService.get('PROFILE.CHANGE_PASSWORD.ALERTS.SERVER_ERROR_MSJ').subscribe(
            translate => {
              serverMsj = translate;
            }
          );
          Swal.fire(
            serverTitle,
            serverMsj,
            'warning'
          );
        }
      );
    } else {
      var unsuccessTitle = '';
      var unsuccessMsj = '';
      this.translateService.get('PROFILE.CHANGE_PASSWORD.ALERTS.PASSWORDS_MATCH_TITLE').subscribe(
        translate => {
          unsuccessTitle = translate;
        }
      );
      this.translateService.get('PROFILE.CHANGE_PASSWORD.ALERTS.PASSWORDS_MATCH_MSJ').subscribe(
        translate => {
          unsuccessMsj = translate;
        }
      );
      Swal.fire(
        unsuccessTitle,
        unsuccessMsj,
        'warning'
      );
    }
  }

}
