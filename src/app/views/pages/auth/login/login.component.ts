import { ResponseModule } from '../../../../models/response/response.module';
// Angular
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// RxJS
import { Observable, Subject } from 'rxjs';
import { finalize, takeUntil, tap } from 'rxjs/operators';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Store
import { Store } from '@ngrx/store';
import { AppState } from '../../../../core/reducers';
// Auth
import { AuthNoticeService, AuthService, Login } from '../../../../core/auth';
import { ApiRestService } from '../../../../network/api-rest.service';
import { NgxPermissionsService } from 'ngx-permissions';

/**
 * ! Just example => Should be removed in development
 */
const DEMO_PARAMS = {
	EMAIL: 'admin@demo.com',
	PASSWORD: 'demo'
};

@Component({
	selector: 'kt-login',
	templateUrl: './login.component.html',
	encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, OnDestroy {
	// Public params
	loginForm: FormGroup;
	loading = false;
	isLoggedIn$: Observable<boolean>;
	errors: any = [];

	private unsubscribe: Subject<any>;

	private returnUrl: any;

	/**
	   * Component constructor
	   *
	   * @param router: Router
	   * @param auth: AuthService
	   * @param authNoticeService: AuthNoticeService
	   * @param translate: TranslateService
	   * @param store: Store<AppState>
	   * @param fb: FormBuilder
	   * @param cdr
	   * @param route
	   */
	constructor(
		private router: Router,
		private apiRest: ApiRestService,
		private authNoticeService: AuthNoticeService,
		private translate: TranslateService,
		private permissionsService: NgxPermissionsService,
		private store: Store<AppState>,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute
	) {
		this.unsubscribe = new Subject();
	}

	/**
	   * On init
	   */
	ngOnInit(): void {
		this.initLoginForm();
	}

	/**
	   * On destroy
	   */
	ngOnDestroy(): void {
		this.authNoticeService.setNotice(null);
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}

	/**
	   * Form initalization
	   * Default params, validators
	   */
	initLoginForm() {
		// demo message to show
		/*if (!this.authNoticeService.onNoticeChanged$.getValue()) {
			const initialNotice = `Use account
			<strong>${DEMO_PARAMS.EMAIL}</strong> and password
			<strong>${DEMO_PARAMS.PASSWORD}</strong> to continue.`;
			this.authNoticeService.setNotice(initialNotice, 'info');
		}*/

		this.loginForm = this.fb.group({
			email: ['', Validators.compose([
				Validators.required,
				Validators.email,
				Validators.minLength(3),
				Validators.maxLength(320)
			])
			],
			password: ['', Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(100)
			])
			]
		});
	}

	/**
	   * Form Submit
	   */
	submit() {
		const controls = this.loginForm.controls;
		/** check form */
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		this.loading = true;

		const authData = {
			email: controls.email.value,
			password: controls.password.value
		};
		this.apiRest.login(authData.email, authData.password).subscribe(
			(response: ResponseModule) => {
				if (response.statusCode === 0) {
					console.log(response)
					this.translate.get('AUTH.LOGIN.MESSAGES.SUCCESS').subscribe(
						translateT => {
							const initialNotice = translateT;
							this.authNoticeService.setNotice('Login correcto', 'success');
							this.loading = false;
						}
					);
					const token = response.resultset.token;
					// Almacenamiento de jwt
					
					console.log(response.resultset);
					
					this.apiRest.setToken(token);
					console.log('arriba');
					this.router.navigate(['/']);
					console.log('abajo');
				} else {
					this.translate.get(response.statusMessage).subscribe(
						translateT => {
							const initialNotice = translateT;
							this.authNoticeService.setNotice(initialNotice, 'danger');
							this.loading = false;
						}
					);
				}
			}, (error) => {
				this.translate.get('AUTH.LOGIN.MESSAGES.SERVER_DIE').subscribe(
					translateT => {
						const initialNotice = translateT;
						this.authNoticeService.setNotice(initialNotice, 'danger');
						this.loading = false;
					}
				);
			}
		);
	}

	/**
	   * Checking control validation
	   *
	   * @param controlName: string => Equals to formControlName
	   * @param validationType: string => Equals to valitors name
	   */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}
}
