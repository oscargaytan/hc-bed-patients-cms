import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiRestService } from '../../../../network/api-rest.service';
import { ResponseModule } from './../../../../models/response/response.module';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
	selector: 'kt-user-add',
	templateUrl: './user-add.component.html',
	styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {

	userForm: FormGroup;

	constructor(private apiRest: ApiRestService,
				private fb: FormBuilder,
				private translateService: TranslateService,
				private router: Router) { }

	ngOnInit() {
		this.initForm();
	}

	initForm(){
		this.userForm = this.fb.group({
			name: ['', Validators.compose(
				[
					Validators.required,
					Validators.minLength(3)
				]
				)],
			lname: ['', Validators.compose(
				[
					Validators.required,
					Validators.minLength(3)
				]
				)],
			email: ['', Validators.compose(
				[
					Validators.required,
					Validators.email
				]
				)]
		})
	}

	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.userForm.controls[controlName];
		if (!control) {
			return false;
		}
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}

	addUser() {
		const request = [];

		const controls = this.userForm.controls;
		if (this.userForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const userName = controls.name.value;
		const userLastName = controls.lname.value;
		const userEmail = controls.email.value;
		const userType = 1;
		console.log(controls);


		this.apiRest.addUser(userName, userLastName, userEmail, userType).subscribe(
			(response: ResponseModule) => {
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				this.translateService.get('USERS.ADD.ALERTS.ADD_UNSUCCESS_TITLE').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
				);
				this.translateService.get(response.statusMessage).subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('USERS.ADD.ALERTS.ADD_SUCCESS_TITLE').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('USERS.ADD.ALERTS.ADD_SUCCESS_MESSAGE').subscribe(
					translate => {
						successMsj = translate;
					}
				);
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/users']);
				} else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
				console.log(response);
			},
			(error) => {
				console.log(error);
			}
		);
	}
}
