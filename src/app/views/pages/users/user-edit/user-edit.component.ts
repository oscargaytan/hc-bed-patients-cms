import { UserModule } from './../../../../models/user/user.module';
import { ResponseModule } from './../../../../models/response/response.module';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiRestService } from './../../../../network/api-rest.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2';


@Component({
  selector: 'kt-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  userID: number;
  userDetail: UserModule;
  userForm:FormGroup;
  
  userType1 = '';
  investment = '';
  emprenteneur = '';
//   userTypes = [
//     {
//     idType: 1,
//     name: 'CATALOGS.USER_TYPE.ADMINISTRATOR'
//     },
//     {
//       idType: 2,
//       name: 'CATALOGS.USER_TYPE.DOCTOR'
//       },
// ];
userTypes=[{
  id:1,
  name:'Administrador'
},{
  id:2,
  name:'Doctor especialista'
}]
  constructor(private apiRest: ApiRestService,
              private translateService: TranslateService,
              private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute) {
    this.userID = +route.snapshot.params.userID;
    
  }

  ngOnInit() {
    this.initForm();
    this.getUserDetail(this.userID);
  }

  initForm() {
    this.userForm = this.fb.group({
      name: ['', Validators.compose(
          [
            Validators.required,
            Validators.minLength(3)
          ]
        )
      ],
      lname: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )
    ],
      email: ['', Validators.compose(
        [
          Validators.required,
          Validators.email
          
        ]
      )
    ],
    type: ['', Validators.compose(
      [
        Validators.required,
        
        
      ]
    )
  ],
    },{});
  }
  
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.userForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  getUserDetail(userID: number) {
    
    
    this.apiRest.getUser(userID).subscribe(
      (response: ResponseModule) => {
        console.log("response"+response);
        if (response.statusCode === 0) {
          this.userDetail = response.resultset;
          
          this.userForm.controls['name'].setValue(response.resultset.userName);
          this.userForm.controls['lname'].setValue(response.resultset.userLName);
          this.userForm.controls['email'].setValue(response.resultset.userEmail);
          
          if (this.userDetail.userType === 1) {
            this.userType1 = 'Administrador';
          };
          
          if(this.userDetail.userType === 2){
            this.userType1 = 'Doctor Especialista';
          }
          //const defaultType = this.userTypes.find(c => c.idType==1);
          
          const toSelect = this.userTypes.find(c => c.id == this.userDetail.userType);
          console.log("fffffffff");
          console.log(toSelect);
          this.userForm.get('type').setValue(toSelect.id);
          //if()
          
          
        }
      }, (error) => {
        console.log(error);
      }
    );
  }

  getValue(){
    const controls = this.userForm.controls;
    console.log(controls.type.value)
  }

  editUser() {
    const controls = this.userForm.controls;

    if (this.userForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
      );
			return;
	}


    var userEdit = new UserModule();
    userEdit.userID = this.userID;
    userEdit.userName = controls.name.value;
    userEdit.userLName = controls.lname.value;
    userEdit.userEmail = controls.email.value;
    userEdit.userType = controls.type.value;


    

    this.apiRest.editUser(userEdit).subscribe(
      (response: ResponseModule) => {
        var successTitle = '';
        var successMsj = '';
        var unsuccessTitle = '';
        var unsuccessMsj = '';
        this.translateService.get('USERS.EDIT.ALERTS.EDIT_UNSUCCESS_TITLE').subscribe(
          translate => {
            unsuccessTitle = translate;
          }
        );
        this.translateService.get('USERS.EDIT.ALERTS.EDIT_UNSUCCESS_MESSAGE').subscribe(
          translate => {
            unsuccessMsj = translate;
          }
        );
        this.translateService.get('Usuario actualizado.').subscribe(
          translate => {
            successTitle = translate;
          }
        );
        this.translateService.get('La información será actualizada.').subscribe(
          translate => {
            successMsj = translate;
          }
        );
        if (response.statusCode === 0) {
          Swal.fire(
            successTitle,
            successMsj,
            'success'
          );
          this.router.navigate(['/users']);
        } else {
          Swal.fire(
            unsuccessTitle,
            unsuccessMsj,
            'warning'
          );
        }
      },
      (error) => {
        console.log(error);
        var unsuccessTitle = '';
        this.translateService.get('USERS.EDIT.ALERTS.EDIT_UNSUCCESS_TITLE').subscribe(
          translate => {
            unsuccessTitle = translate;
          }
        );
        Swal.fire(
          unsuccessTitle,
          error,
          'warning'
        );
      }
    );
  }

}
