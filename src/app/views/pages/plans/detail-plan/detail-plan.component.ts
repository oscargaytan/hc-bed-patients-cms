import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { PlanModule } from '../../../../models/plan/plan.module';
import { ApiRestService } from '../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ResponseModule } from '../../../../models/response/response.module';
import { map, tap } from 'rxjs/operators';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SubscriptionModule } from 'src/app/models/subscription/subscription.module';

@Component({
  selector: 'kt-detail-plan',
  templateUrl: './detail-plan.component.html',
  styleUrls: ['./detail-plan.component.scss']
})
export class DetailPlanComponent implements OnInit {

  planDetail$: Observable<PlanModule>;
  displayedColumns = ['userName', 'userEmail', 'subscriptionCreated'];
  dataSource: MatTableDataSource<SubscriptionModule>;
  @ViewChild('newsPage', { read: MatPaginator, static: false }) paginator: MatPaginator;
  @ViewChild('newsSort', { read: MatSort, static: false }) sort: MatSort;
  planID: number;
  constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router, private route: ActivatedRoute) {
    this.planID = +route.snapshot.params.planID;
  }
  ngOnInit() {
    this.getPlanDetail(this.planID);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getPlanDetail(planID: number) {
    this.planDetail$ = this.apiRest.getPlan(planID).pipe(
      map((response: ResponseModule): PlanModule => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return new PlanModule();
        }
      }),
      tap((plan: PlanModule) => {
        this.dataSource = new MatTableDataSource(plan.planSubscriptions);
        this.configDataTable();
      })
    );
  }

}
