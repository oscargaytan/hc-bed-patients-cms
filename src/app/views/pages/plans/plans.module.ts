// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialPreviewModule } from './../../partials/content/general/material-preview/material-preview.module';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatOptionModule,
  MatSelectModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { PlansComponent } from './plans.component';
import { AddPlanComponent } from './add-plan/add-plan.component';
import { EditPlanComponent } from './edit-plan/edit-plan.component';
import { DetailPlanComponent } from './detail-plan/detail-plan.component';

const eventRoutes = [
  {
    path: '',
    component: PlansComponent
  },
  {
    path: 'add',
    component: AddPlanComponent
  },
  {
    path: 'edit/:planID',
    component: EditPlanComponent
  },
  {
    path: 'detail/:planID',
    component: DetailPlanComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    PartialsModule,
    CoreModule,
    MatPaginatorModule,
    MatTableModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    NgbDropdownModule,
    MatOptionModule,
    MatSelectModule,
    MaterialPreviewModule,
    FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild(eventRoutes),
  ],
  providers: [],
  declarations: [
    PlansComponent,
    AddPlanComponent,
    EditPlanComponent,
    DetailPlanComponent,
  ]
})
export class PlansModule { }
