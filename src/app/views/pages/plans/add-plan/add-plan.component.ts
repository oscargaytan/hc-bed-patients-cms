import { PlanModule } from './../../../../models/plan/plan.module';
import { Component, OnInit } from '@angular/core';
import { ResponseModule } from './../../../../models/response/response.module';
import { ApiRestService } from './../../../../network/api-rest.service';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Md5 } from 'ts-md5/dist/md5';
import { Router } from '@angular/router';


@Component({
  selector: 'kt-add-plan',
  templateUrl: './add-plan.component.html',
  styleUrls: ['./add-plan.component.scss']
})
export class AddPlanComponent implements OnInit {

  constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router) { }

  ngOnInit() {
  }

  makeid(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  addPlan(ngForm: NgForm) {
    const md5 = new Md5();
    const planTemp = new PlanModule();
    const date = new Date();
    planTemp.planInternalID = md5.appendStr(this.makeid(12) + date.getTime).end().toString();
    planTemp.planName = ngForm.value.planName;
    planTemp.planAmount = ngForm.value.planAmount;
    planTemp.planCurrency = ngForm.value.planCurrency;
    planTemp.planInterval = 'month';
    planTemp.planFrecuency = ngForm.value.planInterval;
    planTemp.planExpiredCount = 1;
    planTemp.planTrialDays = 0;

    if(planTemp.planAmount <= 3000){
      this.apiRest.addPlan(planTemp).subscribe(
            (response: ResponseModule) => {
              var successTitle = '';
              var successMsj = '';
              var unsuccessTitle = '';
              var unsuccessMsj = '';

              this.translateService.get('PLANS.ADD.ALERTS.ADD_UNSUCCESS_TITLE').subscribe(
                translate => {
                  unsuccessTitle = translate;
                }
              );
              this.translateService.get(response.statusMessage).subscribe(
                translate => {
                  unsuccessMsj = translate;
                }
              );
              /**this.translateService.get('PLANS.ADD.ALERTS.ADD_UNSUCCESS_MESSAGE').subscribe(
                translate => {
                  unsuccessMsj = translate;
                }
              ); */
              this.translateService.get('PLANS.ADD.ALERTS.ADD_SUCCESS_TITLE').subscribe(
                translate => {
                  successMsj = translate;
                }
              );
              this.translateService.get('PLANS.ADD.ALERTS.ADD_SUCCESS_MESSAGE').subscribe(
                translate => {
                  successTitle = translate;
                }
              );
              if (response.statusCode === 0) {
                Swal.fire(
                  successTitle,
                  successMsj,
                  'success'
                );
                this.router.navigate(['/plans']);
              } else {
                Swal.fire(
                  unsuccessTitle,
                  unsuccessMsj,
                  'error'
                );
              }
            }, (err) => {
              var serverMsj = '';
              var serverTitle = '';
              this.translateService.get('CATALOGS.SERVER_MESSAGE.MESSAGE').subscribe(
                translate => {
                  serverMsj = translate;
                }
              );
              this.translateService.get('CATALOGS.SERVER_MESSAGE.TITLE').subscribe(
                translate => {
                  serverTitle = translate;
                }
              );
              Swal.fire(
                serverTitle,
                serverMsj,
                'error'
              );
            }
          );
    } else {
      var serverMsj = '';
      var serverTitle = '';
      this.translateService.get('PLANS.ADD.ALERTS.AMOUNT_TITLE').subscribe(
        translate => {
          serverTitle = translate;
        }
      );
      this.translateService.get('PLANS.ADD.ALERTS.AMOUNT_MSJ').subscribe(
        translate => {
          serverMsj = translate;
        }
      );
      Swal.fire(
        serverTitle,
        serverMsj,
        'error'
      );
    }
    
  }

}
