import { PlanModule } from './../../../models/plan/plan.module';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ApiRestService } from './../../../network/api-rest.service';
import { ResponseModule } from './../../../models/response/response.module';
import { tap, map } from 'rxjs/operators';

@Component({
  selector: 'kt-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {
  allPlans$: Observable<PlanModule[]> = null;
  displayedColumns = ['planName', 'planAmount', 'planSuscriptions', 'planDuration'];
  dataSource: MatTableDataSource<PlanModule>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private apiRest: ApiRestService) { }

  ngOnInit() {
    this.getAllPlans();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAllPlans() {
    this.allPlans$ = this.apiRest.getAllPlans().pipe(
      map((response: ResponseModule): PlanModule[] => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((plans: PlanModule[]) => {
        this.dataSource = new MatTableDataSource(plans);
        this.configDataTable();
      })
    );
  }

}
