import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectStatusAddComponent } from './proyect-status-add.component';

describe('ProyectStatusAddComponent', () => {
  let component: ProyectStatusAddComponent;
  let fixture: ComponentFixture<ProyectStatusAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectStatusAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectStatusAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
