import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApiRestService } from '../../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { ResponseModule } from '../../../../../models/response/response.module';
import Swal from 'sweetalert2';
import { ProjectStatusModule } from '../../../../../models/project-status/project-status.module';

@Component({
	selector: 'kt-proyect-status-add',
	templateUrl: './proyect-status-add.component.html',
	styleUrls: ['./proyect-status-add.component.scss']
})
export class ProyectStatusAddComponent implements OnInit {

	constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router) { }

	ngOnInit() {
	}

	addStatus(form: NgForm) {
		const request = [];
		var newStatus = new ProjectStatusModule();
		newStatus.statusName = form.value.statusName;
		this.apiRest.addStatus(newStatus).subscribe(
			(response: ResponseModule) => {
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.ADD.ALERTS.ADD_UNSUCCESS_TITLE').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.ADD.ALERTS.ADD_UNSUCCESS_MESSAGE').subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.ADD.ALERTS.ADD_SUCCESS_TITLE').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.ADD.ALERTS.ADD_SUCCESS_MESSAGE').subscribe(
					translate => {
						successMsj = translate;
					}
				);
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/catalogs/status']);
				} else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
			},
			(error) => {
				console.log(error);
			}
		);
	}
}
