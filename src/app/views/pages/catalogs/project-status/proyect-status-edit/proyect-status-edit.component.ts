import { Component, OnInit } from '@angular/core';
import { ProjectStatusModule } from '../../../../../models/project-status/project-status.module';
import { ApiRestService } from '../../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ResponseModule } from '../../../../../models/response/response.module';
import { NgForm } from '@angular/forms';
import { UserModule } from '../../../../../models/user/user.module';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
	selector: 'kt-proyect-status-edit',
	templateUrl: './proyect-status-edit.component.html',
	styleUrls: ['./proyect-status-edit.component.scss']
})
export class ProyectStatusEditComponent implements OnInit {

	statusDetail$: Observable<ProjectStatusModule>;
	statusID: number;

	constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router, private route: ActivatedRoute) {
		this.statusID = +route.snapshot.params.statusID;
	}

	ngOnInit() {
		this.getStatusDetail(this.statusID);
	}

	getStatusDetail(statusID: number) {
		this.statusDetail$ = this.apiRest.getStatus(statusID).pipe(
			map((response: ResponseModule): ProjectStatusModule => {
				if (response.statusCode === 0) {
					return response.resultset;
				} else {
					return new ProjectStatusModule();
				}
			})
		)
	}

	editStatus(form: NgForm) {
		const request = [];
		var statusEdit = new ProjectStatusModule();
		statusEdit.statusID = this.statusID;
		statusEdit.statusName = form.value.statusName;
		this.apiRest.editStatus(statusEdit).subscribe(
			(response: ResponseModule) => {
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.EDIT.ALERTS.EDIT_UNSUCCESS_TITLE').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.EDIT.ALERTS.EDIT_UNSUCCESS_MESSAGE').subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.EDIT.ALERTS.EDIT_SUCCESS_TITLE').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.EDIT.ALERTS.EDIT_SUCCESS_MESSAGE').subscribe(
					translate => {
						successMsj = translate;
					}
				);
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/catalogs/status']);
				} else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
			},
			(error) => {
				console.log(error);
			}
		);
	}

}
