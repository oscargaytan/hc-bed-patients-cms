import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectStatusEditComponent } from './proyect-status-edit.component';

describe('ProyectStatusEditComponent', () => {
  let component: ProyectStatusEditComponent;
  let fixture: ComponentFixture<ProyectStatusEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectStatusEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectStatusEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
