import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiRestService } from '../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseModule } from '../../../../models/response/response.module';
import { ProjectStatusModule } from '../../../../models/project-status/project-status.module';
import Swal from 'sweetalert2';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'kt-project-status',
  templateUrl: './project-status.component.html',
  styleUrls: ['./project-status.component.scss']
})
export class ProjectStatusComponent implements OnInit {

  allStatus$: Observable<ProjectStatusModule[]>;
  displayedColumns = ['statusName', 'actions'];
  dataSource: MatTableDataSource<ProjectStatusModule>;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  constructor(private apiRest: ApiRestService, private translateService: TranslateService) {
  }

  ngOnInit() {
    this.getAllStatus();
  }

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAllStatus() {
    this.allStatus$ = this.apiRest.getAllStatus().pipe(
      map((response: ResponseModule): ProjectStatusModule[] => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((status: ProjectStatusModule[]) => {
        this.dataSource = new MatTableDataSource(status);
        this.configDataTable();
      })
    );
  }

  deleteStatus(statusID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.INDEX.ALERTS.DELETE').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.INDEX.ALERTS.DELETE_MESSAGE').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.INDEX.ALERTS.DELETE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_STATUS.INDEX.ALERTS.DELETE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.deleteStatus(statusID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getAllStatus();
            } else {
              Swal.fire(
                response.statusMessage,
                '',
                'warning'
              );
            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }

}
