import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectSectorAddComponent } from './proyect-sector-add.component';

describe('ProyectSectorAddComponent', () => {
  let component: ProyectSectorAddComponent;
  let fixture: ComponentFixture<ProyectSectorAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectSectorAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectSectorAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
