import { Component, OnInit } from '@angular/core';
import { ProjectSectorModule } from '../../../../../models/project-sector/project-sector.module';
import { ApiRestService } from '../../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ResponseModule } from '../../../../../models/response/response.module';
import Swal from 'sweetalert2';

@Component({
	selector: 'kt-proyect-sector-add',
	templateUrl: './proyect-sector-add.component.html',
	styleUrls: ['./proyect-sector-add.component.scss']
})
export class ProyectSectorAddComponent implements OnInit {

	constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router) { }

	ngOnInit() {
	}

	addSector(form: NgForm) {
		const request = [];
		var newSector = new ProjectSectorModule();
		newSector.sectorName = form.value.sectorName;
		this.apiRest.addSector(newSector).subscribe(
			(response: ResponseModule) => {
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.ADD.ALERTS.ADD_UNSUCCESS_TITLE').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.ADD.ALERTS.ADD_UNSUCCESS_MESSAGE').subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.ADD.ALERTS.ADD_SUCCESS_TITLE').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.ADD.ALERTS.ADD_SUCCESS_MESSAGE').subscribe(
					translate => {
						successMsj = translate;
					}
				);
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/catalogs/sectors']);
				} else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
			},
			(error) => {
				console.log(error);
			}
		);
	}
}
