import { Component, OnInit, ViewChild } from '@angular/core';
import { ProjectSectorModule } from '../../../../models/project-sector/project-sector.module';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ApiRestService } from '../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseModule } from '../../../../models/response/response.module';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'kt-project-sector',
  templateUrl: './project-sector.component.html',
  styleUrls: ['./project-sector.component.scss']
})
export class ProjectSectorComponent implements OnInit {
  allSector$: Observable<ProjectSectorModule[]>;
  displayedColumns = ['sectorName', 'actions'];
  dataSource: MatTableDataSource<ProjectSectorModule>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private apiRest: ApiRestService, private translateService: TranslateService) {
  }

  ngOnInit() {
    this.getAllSectors();
  }

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAllSectors() {
    this.allSector$ = this.apiRest.getAllSectors().pipe(
      map((response: ResponseModule): ProjectSectorModule[] => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((sectors: ProjectSectorModule[]) => {
        this.dataSource = new MatTableDataSource(sectors);
        this.configDataTable();
      })
    );
  }

  deleteSector(sectorID) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.INDEX.ALERTS.DELETE').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.INDEX.ALERTS.DELETE_MESSAGE').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.INDEX.ALERTS.DELETE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.INDEX.ALERTS.DELETE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.deleteSector(sectorID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getAllSectors();
            } else {
              Swal.fire(
                response.statusMessage,
                '',
                'warning'
              );
            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }
}
