import { Component, OnInit } from '@angular/core';
import { ProjectSectorModule } from '../../../../../models/project-sector/project-sector.module';
import { ApiRestService } from '../../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ResponseModule } from '../../../../../models/response/response.module';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
	selector: 'kt-proyect-sector-edit',
	templateUrl: './proyect-sector-edit.component.html',
	styleUrls: ['./proyect-sector-edit.component.scss']
})
export class ProyectSectorEditComponent implements OnInit {

	sectorDetail$: Observable<ProjectSectorModule>;
	sectorID: number;

	constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router, private route: ActivatedRoute) {
		this.sectorID = +route.snapshot.params.sectorID;
	}

	ngOnInit() {
		this.getSectorDetail(this.sectorID);
	}

	getSectorDetail(sectorID: number) {
		this.sectorDetail$ = this.apiRest.getSector(sectorID).pipe(
			map((response: ResponseModule): ProjectSectorModule => {
				if (response.statusCode === 0) {
					return response.resultset;
				} else {
					return new ProjectSectorModule();
				}
			})
		)
	}

	editSector(form: NgForm) {
		const request = [];
		var sectorEdit = new ProjectSectorModule();
		sectorEdit.sectorID = this.sectorID;
		sectorEdit.sectorName = form.value.sectorName;
		this.apiRest.editSector(sectorEdit).subscribe(
			(response: ResponseModule) => {
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.EDIT.ALERTS.EDIT_UNSUCCESS_TITLE').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.EDIT.ALERTS.EDIT_UNSUCCESS_MESSAGE').subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.EDIT.ALERTS.EDIT_SUCCESS_TITLE').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_SECTOR.EDIT.ALERTS.EDIT_SUCCESS_MESSAGE').subscribe(
					translate => {
						successMsj = translate;
					}
				);
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/catalogs/sectors']);
				} else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
			},
			(error) => {
				console.log(error);
			}
		);
	}

}
