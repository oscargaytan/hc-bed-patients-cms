import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectSectorEditComponent } from './proyect-sector-edit.component';

describe('ProyectSectorEditComponent', () => {
  let component: ProyectSectorEditComponent;
  let fixture: ComponentFixture<ProyectSectorEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectSectorEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectSectorEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
