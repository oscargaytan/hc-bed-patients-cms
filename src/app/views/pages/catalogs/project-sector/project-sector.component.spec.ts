import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectSectorComponent } from './project-sector.component';

describe('ProjectSectorComponent', () => {
  let component: ProjectSectorComponent;
  let fixture: ComponentFixture<ProjectSectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectSectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectSectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
