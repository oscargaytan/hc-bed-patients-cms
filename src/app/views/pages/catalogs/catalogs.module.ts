// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialPreviewModule } from './../../partials/content/general/material-preview/material-preview.module';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { TranslateModule } from '@ngx-translate/core';
import {
	MatInputModule,
	MatPaginatorModule,
	MatTableModule,
	MatIconModule,
	MatButtonModule,
	MatOptionModule,
	MatSelectModule,
	MatSortModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ProjectStatusComponent } from './project-status/project-status.component';
import { ProjectCategoryComponent } from './project-category/project-category.component';
import { ProjectSectorComponent } from './project-sector/project-sector.component';
import { ProjectMarketComponent } from './project-market/project-market.component';
import { ProyectStatusAddComponent } from './project-status/proyect-status-add/proyect-status-add.component';
import { ProyectStatusEditComponent } from './project-status/proyect-status-edit/proyect-status-edit.component';
import { ProyectCategoryAddComponent } from './project-category/proyect-category-add/proyect-category-add.component';
import { ProyectCategoryEditComponent } from './project-category/proyect-category-edit/proyect-category-edit.component';
import { ProyectSectorEditComponent } from './project-sector/proyect-sector-edit/proyect-sector-edit.component';
import { ProyectSectorAddComponent } from './project-sector/proyect-sector-add/proyect-sector-add.component';
import { ProjectMarketAddComponent } from './project-market/project-market-add/project-market-add.component';
import { ProjectMarketEditComponent } from './project-market/project-market-edit/project-market-edit.component';

const eventRoutes = [
	{
		path: 'status',
		children: [
			{
				path: '',
				component: ProjectStatusComponent
			}, {
				path: 'add',
				component: ProyectStatusAddComponent
			}, {
				path: 'edit/:statusID',
				component: ProyectStatusEditComponent
			}
		]
	},
	{
		path: 'categories',
		children: [
			{
				path: '',
				component: ProjectCategoryComponent
			}, {
				path: 'add',
				component: ProyectCategoryAddComponent
			}, {
				path: 'edit/:categoryID',
				component: ProyectCategoryEditComponent
			}
		]
	},
	{
		path: 'sectors',
		children: [
			{
				path: '',
				component: ProjectSectorComponent
			}, {
				path: 'add',
				component: ProyectSectorAddComponent
			}, {
				path: 'edit/:sectorID',
				component: ProyectSectorEditComponent
			}
		]
	},
	{
		path: 'markets',
		children: [
			{
				path: '',
				component: ProjectMarketComponent
			}, {
				path: 'add',
				component: ProjectMarketAddComponent
			}, {
				path: 'edit/:marketID',
				component: ProjectMarketEditComponent
			}
		]
	},
];

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		CoreModule,
		MatPaginatorModule,
		MatTableModule,
		MatInputModule,
		MatIconModule,
		MatButtonModule,
		NgbDropdownModule,
		MatOptionModule,
		MatSelectModule,
		MatSortModule,
		MaterialPreviewModule,
		FormsModule,
		TranslateModule.forChild(),
		RouterModule.forChild(eventRoutes),
	],
	providers: [],
	declarations: [
		ProjectStatusComponent,
		ProjectCategoryComponent,
		ProjectSectorComponent,
		ProjectMarketComponent,
		ProyectStatusAddComponent,
		ProyectStatusEditComponent,
		ProyectCategoryAddComponent,
		ProyectCategoryEditComponent,
		ProyectSectorEditComponent,
		ProyectSectorAddComponent,
		ProjectMarketAddComponent,
		ProjectMarketEditComponent
	]
})
export class CatalogsModule { }
