import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectCategoryAddComponent } from './proyect-category-add.component';

describe('ProyectCategoryAddComponent', () => {
  let component: ProyectCategoryAddComponent;
  let fixture: ComponentFixture<ProyectCategoryAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectCategoryAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectCategoryAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
