import { Component, OnInit } from '@angular/core';
import { ProjectCategoryModule } from '../../../../../models/project-category/project-category.module';
import { ApiRestService } from '../../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ResponseModule } from '../../../../../models/response/response.module';
import Swal from 'sweetalert2';

@Component({
	selector: 'kt-proyect-category-add',
	templateUrl: './proyect-category-add.component.html',
	styleUrls: ['./proyect-category-add.component.scss']
})
export class ProyectCategoryAddComponent implements OnInit {
	constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router) { }

	ngOnInit() {
	}

	addCategory(form: NgForm) {
		const request = [];
		var newCategory = new ProjectCategoryModule();
		newCategory.categoryName = form.value.categoryName;
		this.apiRest.addCategory(newCategory).subscribe(
			(response: ResponseModule) => {
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.ADD.ALERTS.ADD_UNSUCCESS_TITLE').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.ADD.ALERTS.ADD_UNSUCCESS_MESSAGE').subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.ADD.ALERTS.ADD_SUCCESS_TITLE').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.ADD.ALERTS.ADD_SUCCESS_MESSAGE').subscribe(
					translate => {
						successMsj = translate;
					}
				);
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/catalogs/categories']);
				} else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
			},
			(error) => {
				console.log(error);
			}
		);
	}
}
