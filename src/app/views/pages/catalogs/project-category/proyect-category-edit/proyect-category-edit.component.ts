import { Component, OnInit } from '@angular/core';
import { ProjectCategoryModule } from '../../../../../models/project-category/project-category.module';
import { ApiRestService } from '../../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ResponseModule } from '../../../../../models/response/response.module';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
	selector: 'kt-proyect-category-edit',
	templateUrl: './proyect-category-edit.component.html',
	styleUrls: ['./proyect-category-edit.component.scss']
})
export class ProyectCategoryEditComponent implements OnInit {

	categoryDetail$: Observable<ProjectCategoryModule>;
	categoryID: number;

	constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router, private route: ActivatedRoute) {
		this.categoryID = +route.snapshot.params.categoryID;
	}

	ngOnInit() {
		this.getCategoryDetail(this.categoryID);
	}

	getCategoryDetail(categoryID: number) {
		this.categoryDetail$ = this.apiRest.getCategory(categoryID).pipe(
			map((response: ResponseModule): ProjectCategoryModule => {
				if (response.statusCode === 0) {
					return response.resultset;
				} else {
					return new ProjectCategoryModule();
				}
			})
		)
	}

	editCategory(form: NgForm) {
		const request = [];
		var categoryEdit = new ProjectCategoryModule();
		categoryEdit.categoryID = this.categoryID;
		categoryEdit.categoryName = form.value.categoryName;
		this.apiRest.editCategory(categoryEdit).subscribe(
			(response: ResponseModule) => {
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.EDIT.ALERTS.EDIT_UNSUCCESS_TITLE').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.EDIT.ALERTS.EDIT_UNSUCCESS_MESSAGE').subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.EDIT.ALERTS.EDIT_SUCCESS_TITLE').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.EDIT.ALERTS.EDIT_SUCCESS_MESSAGE').subscribe(
					translate => {
						successMsj = translate;
					}
				);
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/catalogs/categories']);
				} else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
			},
			(error) => {
				console.log(error);
			}
		);
	}

}
