import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectCategoryEditComponent } from './proyect-category-edit.component';

describe('ProyectCategoryEditComponent', () => {
  let component: ProyectCategoryEditComponent;
  let fixture: ComponentFixture<ProyectCategoryEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectCategoryEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectCategoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
