import { Component, OnInit, ViewChild } from '@angular/core';
import { ProjectCategoryModule } from '../../../../models/project-category/project-category.module';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ApiRestService } from '../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseModule } from '../../../../models/response/response.module';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'kt-project-category',
  templateUrl: './project-category.component.html',
  styleUrls: ['./project-category.component.scss']
})
export class ProjectCategoryComponent implements OnInit {
  allCategory$: Observable<ProjectCategoryModule[]>;
  displayedColumns = ['categoryName', 'actions'];
  dataSource: MatTableDataSource<ProjectCategoryModule>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private apiRest: ApiRestService, private translateService: TranslateService) {
  }

  ngOnInit() {
    this.getAllCategories();
  }

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAllCategories() {
    this.allCategory$ = this.apiRest.getAllCategories().pipe(
      map((response: ResponseModule): ProjectCategoryModule[] => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((categories: ProjectCategoryModule[]) => {
        this.dataSource = new MatTableDataSource(categories);
        this.configDataTable();
      })
    );
  }

  deleteProjectCategory(categoryID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.INDEX.ALERTS.DELETE').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.INDEX.ALERTS.DELETE_MESSAGE').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.INDEX.ALERTS.DELETE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_CATEGORY.INDEX.ALERTS.DELETE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.deleteCategory(categoryID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getAllCategories();
            } else {
              Swal.fire(
                response.statusMessage,
                '',
                'warning'
              );
            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }

}
