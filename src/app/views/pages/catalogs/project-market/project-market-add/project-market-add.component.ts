import { Component, OnInit } from '@angular/core';
import { ProjectMarketModule } from '../../../../../models/project-market/project-market.module';
import { ApiRestService } from '../../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ResponseModule } from '../../../../../models/response/response.module';
import Swal from 'sweetalert2';

@Component({
	selector: 'kt-project-market-add',
	templateUrl: './project-market-add.component.html',
	styleUrls: ['./project-market-add.component.scss']
})
export class ProjectMarketAddComponent implements OnInit {

	constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router) { }

	ngOnInit() {
	}

	addMarket(form: NgForm) {
		const request = [];
		var newMarket = new ProjectMarketModule();
		newMarket.marketName = form.value.marketName;
		this.apiRest.addMarket(newMarket).subscribe(
			(response: ResponseModule) => {
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.ADD.ALERTS.ADD_UNSUCCESS_TITLE').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.ADD.ALERTS.ADD_UNSUCCESS_MESSAGE').subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.ADD.ALERTS.ADD_SUCCESS_TITLE').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.ADD.ALERTS.ADD_SUCCESS_MESSAGE').subscribe(
					translate => {
						successMsj = translate;
					}
				);
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/catalogs/markets']);
				} else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
			},
			(error) => {
				console.log(error);
			}
		);
	}
}
