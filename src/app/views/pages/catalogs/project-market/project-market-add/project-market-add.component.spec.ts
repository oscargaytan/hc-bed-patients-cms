import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectMarketAddComponent } from './project-market-add.component';

describe('ProjectMarketAddComponent', () => {
  let component: ProjectMarketAddComponent;
  let fixture: ComponentFixture<ProjectMarketAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectMarketAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectMarketAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
