import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ApiRestService } from '../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { ProjectMarketModule } from '../../../../models/project-market/project-market.module';
import { ResponseModule } from '../../../../models/response/response.module';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import Swal from 'sweetalert2';


@Component({
  selector: 'kt-project-market',
  templateUrl: './project-market.component.html',
  styleUrls: ['./project-market.component.scss']
})
export class ProjectMarketComponent implements OnInit {
  allMarket$: Observable<ProjectMarketModule[]>;
  displayedColumns = ['marketName', 'actions'];
  dataSource: MatTableDataSource<ProjectMarketModule>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private apiRest: ApiRestService, private translateService: TranslateService) {
  }

  ngOnInit() {
    this.getAllMarkets();
  }

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAllMarkets() {
    this.allMarket$ = this.apiRest.getAllMarkets().pipe(
      map((response: ResponseModule): ProjectMarketModule[] => {
        if (response.statusCode === 0) {
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((markets: ProjectMarketModule[]) => {
        this.dataSource = new MatTableDataSource(markets);
        this.configDataTable();
      })
    );
  }

  deleteMarket(marketID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.INDEX.ALERTS.DELETE').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.INDEX.ALERTS.DELETE_MESSAGE').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('CATALOGS.ALERTS.CANCEL_BUTTON').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.INDEX.ALERTS.DELETE_SUCCESS_TITLE').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.INDEX.ALERTS.DELETE_SUCCESS_MESSAGE').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.deleteMarket(marketID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getAllMarkets();
            } else {
              Swal.fire(
                response.statusMessage,
                '',
                'warning'
              );
            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }
}
