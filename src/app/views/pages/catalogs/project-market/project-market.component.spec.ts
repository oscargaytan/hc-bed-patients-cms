import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectMarketComponent } from './project-market.component';

describe('ProjectMarketComponent', () => {
  let component: ProjectMarketComponent;
  let fixture: ComponentFixture<ProjectMarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectMarketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectMarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
