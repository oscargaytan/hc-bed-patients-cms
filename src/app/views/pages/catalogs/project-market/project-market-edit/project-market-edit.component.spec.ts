import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectMarketEditComponent } from './project-market-edit.component';

describe('ProjectMarketEditComponent', () => {
  let component: ProjectMarketEditComponent;
  let fixture: ComponentFixture<ProjectMarketEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectMarketEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectMarketEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
