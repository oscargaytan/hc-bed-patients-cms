import { Component, OnInit } from '@angular/core';
import { ProjectMarketModule } from '../../../../../models/project-market/project-market.module';
import { ApiRestService } from '../../../../../network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ResponseModule } from '../../../../../models/response/response.module';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
	selector: 'kt-project-market-edit',
	templateUrl: './project-market-edit.component.html',
	styleUrls: ['./project-market-edit.component.scss']
})
export class ProjectMarketEditComponent implements OnInit {

	marketDetail$: Observable<ProjectMarketModule>;
	marketID: number;

	constructor(private apiRest: ApiRestService, private translateService: TranslateService, private router: Router, private route: ActivatedRoute) {
		this.marketID = +route.snapshot.params.marketID;
	}

	ngOnInit() {
		this.getMarketDetail(this.marketID);
	}

	getMarketDetail(marketID: number) {
		this.marketDetail$ = this.apiRest.getMarket(marketID).pipe(
			map((response: ResponseModule): ProjectMarketModule => {
				if (response.statusCode === 0) {
					return response.resultset;
				} else {
					return new ProjectMarketModule();
				}
			})
		)
	}

	editMarket(form: NgForm) {
		const request = [];
		var marketEdit = new ProjectMarketModule();
		marketEdit.marketID = this.marketID;
		marketEdit.marketName = form.value.marketName;
		this.apiRest.editMarket(marketEdit).subscribe(
			(response: ResponseModule) => {
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.EDIT.ALERTS.EDIT_UNSUCCESS_TITLE').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.EDIT.ALERTS.EDIT_UNSUCCESS_MESSAGE').subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.EDIT.ALERTS.EDIT_SUCCESS_TITLE').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('CATALOGS_LIST.PROJECT_MARKET.EDIT.ALERTS.EDIT_SUCCESS_MESSAGE').subscribe(
					translate => {
						successMsj = translate;
					}
				);
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/catalogs/markets']);
				} else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
			},
			(error) => {
				console.log(error);
			}
		);
	}

}
