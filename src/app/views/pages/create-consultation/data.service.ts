import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  consultationID = '';
  private messageSource = new BehaviorSubject<any>(this.consultationID);
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  redirectConsultation(consultationID: any){
    this.messageSource.next(consultationID);
  }
}
