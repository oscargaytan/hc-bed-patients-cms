import { Component, OnInit, Input } from '@angular/core';
import { DataService } from "./../data.service";
import { ResponseModule } from './../../../../models/response/response.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiRestService } from './../../../../network/api-rest.service';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, tap, startWith} from 'rxjs/operators';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'kt-create-consultation-edit',
  templateUrl: './create-consultation-edit.component.html',
  styleUrls: ['./create-consultation-edit.component.scss']
})
export class CreateConsultationEditComponent implements OnInit {

  consultationID: any;
  detailID: any;
  //@Input() consultationID: any
  consultationDetailForm:FormGroup;
  constructor(private apiRest: ApiRestService, 
              private translateService: TranslateService,
              private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private data: DataService) {
      this.consultationID = +route.snapshot.params.id;
      this.detailID = +route.snapshot.params.detail;
               }

  ngOnInit() {
    // this.data.currentMessage.subscribe(consultationID => this.consultationID = consultationID);
    // console.log(this.consultationID)
    this.initForm();
  }

  initForm() {
    this.consultationDetailForm = this.fb.group({
      priority: ['', Validators.compose([Validators.required])],
      comment: ['', Validators.compose([Validators.required])],
      height: ['', Validators.compose([Validators.required])],
      weight: ['', Validators.compose([Validators.required])],
      blood_pressure: ['', Validators.compose([Validators.required])],
      vital_signs: ['', Validators.compose([Validators.required])],
      reason: ['', Validators.compose([Validators.required])],
      summary: ['', Validators.compose([Validators.required])],
      diagnosis: ['', Validators.compose([Validators.required])],
      prognosis: ['', Validators.compose([Validators.required])],
      treatment: ['', Validators.compose([Validators.required])]
     
      
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.consultationDetailForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  editConsultation(){
    const controls = this.consultationDetailForm.controls;
    if (this.consultationDetailForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
      );
			return;
    }
    console.log(this.consultationID)
    console.log(this.detailID);


    const consultationID = this.consultationID;
    const detailID = this.detailID;

    const priority = controls.priority.value;
    const comment = controls.comment.value;
    const height = controls.height.value;
    const weight = controls.weight.value;
    const blood_pressure = controls.blood_pressure.value;
    const vital_signs = controls.vital_signs.value;
    const reason = controls.reason.value;
    const summary = controls.summary.value;
    const diagnosis = controls.diagnosis.value;
    const prognosis = controls.prognosis.value;
    const treatment = controls.treatment.value;

    let data = {
      consultationID,
      detailID,
      priority,
      comment,
      height,
      weight,
      blood_pressure,
      vital_signs,
      reason,
      summary,
      diagnosis,
      prognosis,
      treatment
    }

    console.log(data)

    

    this.apiRest.createConsultationEdit(data).subscribe(
      (response: ResponseModule) => {
        console.log(response);
        var successTitle = '';
        var successMsj = '';
        var unsuccessTitle = '';
        var unsuccessMsj = '';
        var shortMsj= '';
        this.translateService.get('Error al crear la consulta').subscribe(
          translate => {
            unsuccessTitle = translate;
          }
        );
        this.translateService.get('Un campo está incorrecto.').subscribe(
          translate => {
            unsuccessMsj = translate;
          }
        );
        
        this.translateService.get('Consulta enviada').subscribe(
          translate => {
            successTitle = translate;
          }
        );
        this.translateService.get('La consula se mostrará en el sistema de Homecare.').subscribe(
          translate => {
            successMsj = translate;
          }
        );
        if (response.statusCode === 0) {
          Swal.fire(
            successTitle,
            successMsj,
            'success'
          );
          this.router.navigate(['/create-consultation']);
        } else {
          Swal.fire(
            unsuccessTitle,
            unsuccessMsj,
            'warning'
          );
        }
      },
      (error) => {
        console.log(error);
        var unsuccessTitle = '';
        this.translateService.get('USERS.EDIT.ALERTS.EDIT_UNSUCCESS_TITLE').subscribe(
          translate => {
            unsuccessTitle = translate;
          }
        );
        Swal.fire(
          unsuccessTitle,
          error,
          'warning'
        );
      }
    );

  }

  cancelConsultation(){
    const consultationID = this.consultationID;
    const detailID = this.detailID;

    this.apiRest.createConsultationCancel(consultationID, detailID).subscribe(
      (response: ResponseModule) => {
        
        Swal.fire(
          'Consulta cancelada',
          'No se enviará la consulta al sistema.',
          'warning'
        );

      })


  }




}
