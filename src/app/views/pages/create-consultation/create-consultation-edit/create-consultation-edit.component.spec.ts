import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateConsultationEditComponent } from './create-consultation-edit.component';

describe('CreateConsultationEditComponent', () => {
  let component: CreateConsultationEditComponent;
  let fixture: ComponentFixture<CreateConsultationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateConsultationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateConsultationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
