import { Component, OnInit, ViewChild } from '@angular/core';
import { ResponseModule } from './../../../models/response/response.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiRestService } from './../../../network/api-rest.service';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, tap, startWith} from 'rxjs/operators';
import Swal from 'sweetalert2';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { PatientModule } from 'src/app/models/patients/patients.module';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { DataService } from "./data.service";


@Component({
  selector: 'kt-create-consultation',
  templateUrl: './create-consultation.component.html',
  styleUrls: ['./create-consultation.component.scss']
})
export class CreateConsultationComponent implements OnInit {
    myControl = new FormControl();
    myControlD = new FormControl();

    options: string[] = [];
    filteredOptions: Observable<string[]>;

    doctors: string[] = [];
    filteredDoctors: Observable<string[]>;
  
    patients=[];

    patientEmail = '';
    doctorEmail = '';

    checkPatientList = false;
    checkDoctorList = false;

    message:string;
    consultationID:any;

    selectedPatient = 'Paciente Uno --- pac.uno111@gmail.com';
    

    consultationForm:FormGroup;

  constructor(private apiRest: ApiRestService, 
              private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private data: DataService) { }

    

  ngOnInit() {
    this.initForm();
    this.getPatients();
    this.getDoctors();
    this.data.currentMessage.subscribe(consultationID => this.consultationID = consultationID);
    console.log(this.consultationID);
    
    
      
  }
  initForm() {
    this.consultationForm = this.fb.group({
      patient: ['', Validators.compose(
                    [
                      Validators.required
                      //Validators.minLength(3)
                    ]
                  )
                ],
      doctor: ['', Validators.compose(
                  [
                    Validators.required
                    //Validators.minLength(3)
                  ]
                )
              ]
    },{});
  }
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.consultationForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  private _filterDoctors(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.doctors.filter(doctor => doctor.toLowerCase().includes(filterValue));
  }
  

  checkPatientOptions(){


    var checkPatientList = false;
    const controls = this.consultationForm.controls;
    const patient = controls.patient.value;
    this.options.forEach(patientOptions => {
      if(patient == patientOptions){
        checkPatientList = true;
      }
    });

    if(checkPatientList != false){
      console.log("continue");
    } else {
      console.log("back")
      this.consultationForm.controls['patient'].setValue(null);
      this.options = [];
      this.getPatients();
    }
   
    
  }
  checkDoctorOptions(){
    var checkDoctorList = false;
    const controls = this.consultationForm.controls;
    const doctor = controls.doctor.value;

    this.doctors.forEach(doctorOptions => {
      if(doctor == doctorOptions){
        checkDoctorList = true;
      } 
    });
    if(checkDoctorList != false){
      console.log("continue");
    } else {
      console.log("back")
      this.consultationForm.controls['doctor'].setValue(null);
      this.doctors = [];
      this.getDoctors();
    }
  }

  getPatients(){
   this.apiRest.getPatients().subscribe(
    (response: ResponseModule)=>{
      
      response.resultset.forEach(patient => {
        this.options.push(patient.patientName +' '+ patient.patientLName +' --- '+ patient.patientEmail);
      });

      this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
        );

        //console.log(this.playerName)
    }
    )
  }


  getDoctors(){
    this.apiRest.getDoctorsMedCenters().subscribe(
      (response: ResponseModule)=>{
 
        response.resultset.forEach(doctor => {
          this.doctors.push(doctor.doctors.doctor_name +' '+ doctor.doctors.doctor_lname +' -- '+ doctor.medical_centers.medical_center_name+' --- '+ doctor.doctor_medical_center_email);
        });
  
        this.filteredDoctors = this.myControlD.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterDoctors(value))
          );

          console.log(this.filteredDoctors);
  
          
      }
      )
  }

  startConsultation(){
    const controls = this.consultationForm.controls;

		if (this.consultationForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
      );
			return;
    }

    
    const patient = controls.patient.value;
    const doctor = controls.doctor.value;

    
   

        
    var array = patient.split(' --- ');
    var array2 = doctor.split(' --- ');
    

    this.patientEmail = array[1];
    this.doctorEmail = array2[1];
   
    this.apiRest.checkPatient(this.patientEmail).subscribe(
      (response: ResponseModule)=>{
        console.log("response")
        console.log(response)
        console.log(this.patientEmail);
        if(response.statusCode == 0){
          var patientID = response.resultset[0].pk_patient;
          
          this.apiRest.checkDoctor(this.doctorEmail).subscribe(
            (response: ResponseModule)=>{
              if(response.statusCode == 0){
                var doctorID = response.resultset.pk_doctor_medical_center;

                this.apiRest.createConsultations(patientID, doctorID).subscribe(
                  (response) =>{
                    console.log("createConsultations")
                    console.log(response);

                    //this.data.redirectConsultation(response.fk_medical_consultation)
                    this.router.navigate(['/create-consultation/edit', {id: response.fk_medical_consultation, detail: response.pk_medical_consultation_detail}]); 
                    

                  }
                )
              } else{
                Swal.fire(
                  'Doctor no encontrado',
                  'No se encontro a un doctor con ese correo electrónico.',
                  'warning'
                );
              }
            });
        } else{
         
          Swal.fire(
            'Paciente no encontrado',
            'No se encontro a un paciente con ese correo electrónico.',
            'warning'
          );
        }
      });

    

    

    // this.apiRest.checkDoctorExist('oscar@mail.com').subscribe(
    //   (response: ResponseModule)=>{
    //     console.log(response)
    //   }
    // )

    

    

      

    console.log("correos "+ this.patientEmail + " " + this.doctorEmail);



    console.log(patient);
    console.log(doctor);
   
    
  }
  


}
