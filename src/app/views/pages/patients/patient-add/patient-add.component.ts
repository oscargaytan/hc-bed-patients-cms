import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiRestService } from '../../../../network/api-rest.service';
import { ResponseModule } from './../../../../models/response/response.module';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
@Component({
  selector: 'kt-patient-add',
  templateUrl: './patient-add.component.html',
  styleUrls: ['./patient-add.component.scss']
})
export class PatientAddComponent implements OnInit {

  patientForm:FormGroup;

  constructor(private apiRest: ApiRestService,
              private fb: FormBuilder,
              private translateService: TranslateService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    this.patientForm = this.fb.group({
      name: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )],
      lname: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )],
      email: ['', Validators.compose(
        [
          Validators.required,
          Validators.email
        ]
      )],
      phone: [''],
      state: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )],
      gender: [''],
      blood: [''],
      allergies: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )],
      bdate: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )],
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.patientForm.controls[controlName];
		if (!control) {
			return false;
		}
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}

  addPatient() {
		const request = [];

		const controls = this.patientForm.controls;

		if (this.patientForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		const name = controls.name.value;
		const last_name = controls.lname.value;
    const email = controls.email.value;
    const phone = controls.phone.value;
		const state = controls.state.value;
    const gender = controls.gender.value;
    const blood_type = controls.blood.value;
		const birth_date = controls.bdate.value;
    const allergies = controls.allergies.value;
    const password = "1234";
		
		console.log(controls);

		let userSpecialty = [];
		
		let data = {
			name,
			last_name,
			email,
      phone,
      state,
      gender,
      blood_type,
      birth_date,
      allergies,
      password
		};
		
		this.apiRest.addPatient(data).subscribe(
			(response: ResponseModule) => {
        console.log("bruh")
        console.log(response);
				var successTitle = '';
				var successMsj = '';
				var unsuccessTitle = '';
				var unsuccessMsj = '';
				var shortMsj= '';
				this.translateService.get('Error al añádir al paciente').subscribe(
					translate => {
						unsuccessTitle = translate;
					}
				);
				this.translateService.get('Ese correo electrónico ya está registrado.').subscribe(
					translate => {
						unsuccessMsj = translate;
					}
				);
				this.translateService.get('El nombre del paciente es muy corto.').subscribe(
					translate => {
						shortMsj = translate;
					}
				);
				this.translateService.get('Paciente añadido').subscribe(
					translate => {
						successTitle = translate;
					}
				);
				this.translateService.get('Se agregó la informacion del paciente al sistema.').subscribe(
					translate => {
						successMsj = translate;
					}
				);
				if (response.statusCode === 0) {
					Swal.fire(
						successTitle,
						successMsj,
						'success'
					);
					this.router.navigate(['/patients']);
				} else if(response.statusCode === 9){
					Swal.fire(
						unsuccessTitle,
						shortMsj,
						'warning'
					);

				} else {
					Swal.fire(
						unsuccessTitle,
						unsuccessMsj,
						'warning'
					);
				}
				console.log(response);
			},
			(error) => {
				console.log(error);
			}
		);
	}

}
