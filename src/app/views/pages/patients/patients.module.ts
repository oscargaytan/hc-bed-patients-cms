import { MaterialPreviewModule } from './../../partials/content/general/material-preview/material-preview.module';
import { NgbDropdownModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { TranslateModule } from '@ngx-translate/core';
import {
	MatInputModule,
	MatPaginatorModule,
	MatTableModule,
	MatIconModule,
	MatButtonModule,
	MatOptionModule,
	MatSelectModule,
	MatSortModule
} from '@angular/material';
import { PatientsComponent } from './patients.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PatientAddComponent } from './patient-add/patient-add.component';
import { PatientEditComponent } from './patient-edit/patient-edit.component';

const patientRoutes = [
	{
		path: '',
		component: PatientsComponent
	}, {
		path: 'add',
		component: PatientAddComponent
	}, {
		path: 'edit/:patientID',
		component: PatientEditComponent
	},
];

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		CoreModule,
		MatPaginatorModule,
		MatTableModule,
		MatInputModule,
		MatIconModule,
		MatButtonModule,
		NgbDropdownModule,
		MatOptionModule,
		MatSortModule,
		MatSelectModule,
		MaterialPreviewModule,
		FormsModule,
		ReactiveFormsModule,
		NgbTabsetModule,
		TranslateModule.forChild(),
		RouterModule.forChild(patientRoutes),
	],
	providers: [],
	declarations: [
		PatientsComponent,
		PatientAddComponent,
		PatientEditComponent,
		
		
	]
})
export class PatientsModule {
}