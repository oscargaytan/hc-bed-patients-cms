import { PatientModule } from './../../../../models/patients/patients.module';
import { ResponseModule } from './../../../../models/response/response.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiRestService } from './../../../../network/api-rest.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'kt-patient-edit',
  templateUrl: './patient-edit.component.html',
  styleUrls: ['./patient-edit.component.scss']
})
export class PatientEditComponent implements OnInit {

  patientID: number;
  patientForm:FormGroup;

  constructor(private apiRest: ApiRestService,
              private translateService: TranslateService,
              private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute) {
      this.patientID = +route.snapshot.params.patientID;
               }

  ngOnInit() {
    this.initForm();
    
    this.getPatientDetail(this.patientID);
  }

  initForm(){
    this.patientForm = this.fb.group({
      name: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )],
      lname: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )],
      email: ['', Validators.compose(
        [
          Validators.required,
          Validators.email
        ]
      )],
      phone: [''],
      state: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )],
      gender: [''],
      blood: [''],
      allergies: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )],
      bdate: ['', Validators.compose(
        [
          Validators.required,
          Validators.minLength(3)
        ]
      )],
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.patientForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  getPatientDetail(patientID: number) {
    console.log("bruh" + patientID);
    this.apiRest.getPatient(patientID).subscribe(
      (response: ResponseModule) => {
        console.log("bruh2");
        if (response.statusCode === 0) {
          this.patientForm.controls['name'].setValue(response.resultset.patientName);
          this.patientForm.controls['lname'].setValue(response.resultset.patientLName);
          this.patientForm.controls['email'].setValue(response.resultset.patientEmail);
          this.patientForm.controls['phone'].setValue(response.resultset.patientPhone);
          this.patientForm.controls['state'].setValue(response.resultset.patientState);
          this.patientForm.controls['gender'].setValue(response.resultset.patientGender);
          this.patientForm.controls['blood'].setValue(response.resultset.patientBloodType);
          this.patientForm.controls['bdate'].setValue(response.resultset.patientBDate);
          if(response.resultset.patientAllergies){
            this.patientForm.controls['allergies'].setValue(response.resultset.patientAllergies);
          } else{
            this.patientForm.controls['allergies'].setValue('Ninguna');
          } 
          console.log("paciente")
          console.log(response);
        }
      }, (error) => {
        
        console.log(error);
      }
    );
  }

  editPatient(){
    const controls = this.patientForm.controls;

    if (this.patientForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
      );
			return;
  }
  
    const name = controls.name.value;
    const last_name = controls.lname.value;
    const email = controls.email.value;
    const phone = controls.phone.value;
    const state = controls.state.value;
    const gender = controls.gender.value;
    const blood_type = controls.blood.value;
    const birth_date = controls.bdate.value;
    const allergies = controls.allergies.value;

    const patientID = 1;
    const password = "1234";
    const type = 1;
    const status = 0;

    let data = {
      name,
      last_name,
      email,
      phone,
      state,
      gender,
      blood_type,
      birth_date,
      allergies,
      patientID,
      password,
      type,
      status
    }

    this.apiRest.editPatient(data).subscribe(
      (response: ResponseModule) => {
        console.log(response);
        var successTitle = '';
        var successMsj = '';
        var unsuccessTitle = '';
        var unsuccessMsj = '';
        var shortMsj= '';
        this.translateService.get('Error al editar al paciente').subscribe(
          translate => {
            unsuccessTitle = translate;
          }
        );
        this.translateService.get('Ese correo electrónico ya está registrado.').subscribe(
          translate => {
            unsuccessMsj = translate;
          }
        );
        this.translateService.get('El nombre del paciente es muy corto.').subscribe(
					translate => {
						shortMsj = translate;
					}
				);
        this.translateService.get('Paciente actualizado').subscribe(
          translate => {
            successTitle = translate;
          }
        );
        this.translateService.get('La informacion será actualizada.').subscribe(
          translate => {
            successMsj = translate;
          }
        );
        if (response.statusCode === 0) {
          Swal.fire(
            successTitle,
            successMsj,
            'success'
          );
          this.router.navigate(['/patients']);
        } else if(response.statusCode === 9){
					Swal.fire(
						unsuccessTitle,
						shortMsj,
						'warning'
					);

				}else {
          Swal.fire(
            unsuccessTitle,
            unsuccessMsj,
            'warning'
          );
        }
      },
      (error) => {
        console.log(error);
        var unsuccessTitle = '';
        this.translateService.get('USERS.EDIT.ALERTS.EDIT_UNSUCCESS_TITLE').subscribe(
          translate => {
            unsuccessTitle = translate;
          }
        );
        Swal.fire(
          unsuccessTitle,
          error,
          'warning'
        );
      }
    );
  


  }

}
