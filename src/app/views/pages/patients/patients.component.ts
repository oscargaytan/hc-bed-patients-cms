import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { PatientModule } from 'src/app/models/patients/patients.module';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ApiRestService } from 'src/app/network/api-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseModule } from 'src/app/models/response/response.module';
import Swal from 'sweetalert2';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'kt-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {

  genderArray = [];
  data: any;
  patients$: Observable<PatientModule[]>;
  displayedColumns = ['patientID', 'patientName', 'patientLName', 'patientEmail', 'patientBdate', 'patientGender', 'patientStatus', 'actions'];
  dataSource: MatTableDataSource<PatientModule>;
  @ViewChild('usersPage', { read: MatPaginator, static: false }) paginator: MatPaginator;
  @ViewChild('users', { read: MatSort, static: false }) sort: MatSort;
  

  constructor(private apiRest: ApiRestService, private translateService: TranslateService) { }

  ngOnInit() {
    this.getPatients();
  }
  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  configDataTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getPatients() {
    this.patients$ = this.apiRest.getPatients().pipe(
      map((response: ResponseModule): PatientModule[] => {

        
        if (response.statusCode === 0) {
          this.data = response.resultset;

          console.log("data");
          console.log(this.data);

          this.data.forEach(element => {
            console.log(element.patientGender);
            if (element.patientGender == 0) {
              
            }
          });

          // if(this.data['patientGender'] === 0){
          //   this.genderString = 'Femenino';
          // } 
          // if(this.data['patientGender'] === 1){
          //   this.genderString = 'Masculino';
          // }
          return response.resultset;
        } else {
          return [];
        }
      }),
      tap((patients: PatientModule[]) => {
        this.dataSource = new MatTableDataSource(patients);
        this.configDataTable();
      })
    );
  }

  disablePatient(patientID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('Inhabilitar').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('El paciente será inhabilitado ¿desea continuar con la solicitud?').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('Cancelar').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('Paciente inhabilitado').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('La información será actualizada.').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.disablePatient(patientID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getPatients();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }
  enablePatient(patientID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('Reactivar').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('El paciente será reactivado ¿desea continuar con la solicitud?').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('Cancelar').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('Paciente reactivado').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('La información será actulizada.').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.enablePatient(patientID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getPatients();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }
  deletePatient(patientID: number) {
    var confirmBtn = '';
    var actionMessage = '';
    var cancelBtn = '';
    var successTitle = '';
    var successMsj = '';
    this.translateService.get('Eliminar').subscribe(
      translate => {
        confirmBtn = translate;
      }
    );
    this.translateService.get('El paciente será eliminado del sistema, ¿desea continuar con la solicitud?').subscribe(
      translate => {
        actionMessage = translate;
      }
    );
    this.translateService.get('Cancelar').subscribe(
      translate => {
        cancelBtn = translate;
      }
    );
    this.translateService.get('Paciente eliminado').subscribe(
      translate => {
        successTitle = translate;
      }
    );
    this.translateService.get('La información será actualizada').subscribe(
      translate => {
        successMsj = translate;
      }
    );
    Swal.fire({
      title: actionMessage,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmBtn,
      confirmButtonColor: '#77D2D3',
      cancelButtonText: cancelBtn
    }).then((result) => {
      if (result.value) {
        this.apiRest.deletePatient(patientID).subscribe(
          (response: ResponseModule) => {
            if (response.statusCode === 0) {
              Swal.fire(
                successTitle,
                successMsj,
                'success'
              );
              this.getPatients();
            } else {

            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }




}
