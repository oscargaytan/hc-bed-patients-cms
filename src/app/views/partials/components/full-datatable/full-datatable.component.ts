import { AdminUserModule } from './../../../../models/admin-user/admin-user.module';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DataTableDataSource } from '../../content/widgets/general/data-table/data-table.data-source';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { DataTableItemModel, DataTableService } from '../../../../core/_base/layout';
import { merge } from 'rxjs';
import { tap } from 'lodash';
import { QueryParamsModel } from '../../../../core/_base/crud';

@Component({
	selector: 'kt-full-datatable',
	templateUrl: './full-datatable.component.html',
	styleUrls: ['./full-datatable.component.scss']
})
export class FullDatatableComponent implements OnInit {

	//displayedColumns = ['id', 'name', 'email', 'color'];
	dataSource: MatTableDataSource<any>;

	@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
	@ViewChild(MatSort, { static: true }) sort: MatSort;

	@Input() rows: any[];
	@Input() displayedColumns: string[];

	constructor() {
		// Create 100 users
		const users: UserData[] = [];
		for (let i = 1; i <= 100; i++) { users.push(createNewUser(i)); }

		// Assign the data to the data source for the table to render
		this.dataSource = new MatTableDataSource(this.rows);
	}

	ngOnInit(): void {
		console.log("YEI", this.rows);
	}
	/**
	* Set the paginator and sort after the view init since this component will
	* be able to query its view for the initialized paginator and sort.
	*/
	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	applyFilter(filterValue: string) {
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
		this.dataSource.filter = filterValue;
	}

}


/** Builds and returns a new User. */
function createNewUser(id: number): UserData {
	const name =
		NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
		NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
	const email =
		EMAILS[Math.round(Math.random() * (EMAILS.length - 1))];


	return {
		id: id.toString(),
		name: name,
		email: email,
		color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
	};
}


/** Constants used to fill up our data base. */
const COLORS = ['maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
	'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'];
const NAMES = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
	'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
	'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];
const EMAILS = ['tassam.dropg@lvshf.luk2.com', 'rshady103@camaradamail.club', '6saeed.kh@dota2bets.net', '3ram@fatezalez.cloudns.asia', 'bzazaa1550d@bapumojo.ga', 'mmarciosaad9@ilt.ctu.edu.gr', 'tabraheem@ai.aax.cloudns.asia', 'emeeazzadenn@aronsmoney.cloudns.asia', 'nfarhan.zeb.9069@kingyslmail.top', 'imich@luadao.club', '580ahlalx@pedregosamail.club', 'celvis.cardozo.1y@unite.cloudns.asia', 'nmargertu@hagiasophiagroup.com', 'danils8@upskrcnd.ml', 'galialam@thesophiaonline.com', 'skanad@doyouneedrenovation.net', '8mokra@myccscollection.com', 'llamissikramp@2mohamdah.defaultdomain.ml', 'ijayce.jayce.983@nakammoleb.xyz'];

export interface UserData {
	id: string;
	name: string;
	email: string;
	color: string;
}

