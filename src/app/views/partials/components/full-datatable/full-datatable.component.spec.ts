import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullDatatableComponent } from './full-datatable.component';

describe('FullDatatableComponent', () => {
  let component: FullDatatableComponent;
  let fixture: ComponentFixture<FullDatatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullDatatableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullDatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
