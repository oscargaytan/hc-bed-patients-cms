import { Component, OnInit, Input } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'kt-carousel-basic',
  templateUrl: './carousel-basic.component.html',
  styleUrls: ['./carousel-basic.component.scss']
})
export class CarouselBasicComponent implements OnInit {

  // images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
  @Input() images: string[];

  constructor(config: NgbCarouselConfig) {
    //https://ng-bootstrap.github.io/#/components/carousel/api
    // customize default values of carousels used by this component tree
    config.wrap = true;
    config.showNavigationIndicators = true;
  }

  ngOnInit() {
  }

}
