import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'kt-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit {

  constructor(private router: Router,@Inject(MAT_SNACK_BAR_DATA,) public data: any) { }

  ngOnInit() {
  }

  navigate(){
    this.router.navigate(['/consultations']);
  }

}
