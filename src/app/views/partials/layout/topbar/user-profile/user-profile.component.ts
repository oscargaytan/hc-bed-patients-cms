import { ApiRestService } from './../../../../../network/api-rest.service';
import { UserProfileModule } from './../../../../../models/user-profile/user-profile.module';
import { ResponseModule } from './../../../../../models/response/response.module';
import { UserModule } from './../../../../../models/user/user.module';
import { Router } from '@angular/router';
// Angular
import { Component, Input, OnInit } from '@angular/core';
// RxJS
import { Observable } from 'rxjs';
// NGRX
import { select, Store } from '@ngrx/store';
// State
import { AppState } from '../../../../../core/reducers';
import { currentUser, Logout, User } from '../../../../../core/auth';
import { NgxPermissionsService } from 'ngx-permissions';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'kt-user-profile',
  templateUrl: './user-profile.component.html',
})
//this
export class UserProfileComponent implements OnInit {
  // Public properties
  user: UserProfileModule;
  userDetail: UserModule;
  

  @Input() avatar: boolean = false;
  @Input() greeting: boolean = false;
  @Input() badge: boolean = true;
  @Input() icon: boolean = false;


	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 */
  constructor(private translateService: TranslateService, private router: Router, private apiRest: ApiRestService, private permissionsService: NgxPermissionsService) {
  }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
  ngOnInit(): void {
    this.user = this.apiRest.getDataToken();
    
    this.userDetail = {
      userID: 1,
      userName: '',
      userLName: '',
      userEmail: '',
      userType: 1
    };
      

    this.getUser(this.user.userID)
  }

  reload(){
    //this.router.navigateByUrl('/RefreshComponent');

    console.log("antes");
    console.log(this.userDetail);
    this.apiRest.getUser(1).subscribe(
      (response: ResponseModule) => {
        if (response.statusCode === 0) {
          this.userDetail = response.resultset;
        }
      }
    );

    console.log("despues");
    console.log(this.userDetail);
    
  }

  getUser(userID: number){
    this.apiRest.getUser(userID).subscribe(
      (response: ResponseModule) => {
        
        if (response.statusCode === 0) {
          this.userDetail = response.resultset; 
        }
      }
    );
  }

 


	/**
	 * Log out
	 */
  logout() {
    var messageTxt = '';
    var buttonTxt = '';
    this.translateService.get('USER_HEADER.ALERTS.LOGOUT_CONFIRM_TITLE').subscribe(
      translate => {
        messageTxt = translate;
      }
    );
    this.translateService.get('USER_HEADER.ALERTS.LOGOUT_CONFIRM_BUTTON').subscribe(
      translate => {
        buttonTxt = translate;
      }
    );
    Swal.fire({
      title: messageTxt,
      type: 'warning',
      confirmButtonText: buttonTxt,
      confirmButtonColor: '#77D2D3',
    }).then((result) => {
      if (result.value) {
        localStorage.clear();
        this.permissionsService.flushPermissions();
        this.router.navigate(['/auth/login']);
      }
    });
  }
} 


