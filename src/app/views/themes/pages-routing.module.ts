// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './base/base.component';
import { ErrorPageComponent } from './content/error-page/error-page.component';
// Auth
import { AuthGuard } from '../../guards/auth.guard';
import { PrivilegeAccessGuard } from '../../guards/privilege-access.guard';
import { NgxPermissionsGuard } from 'ngx-permissions';

const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    canActivateChild: [AuthGuard, PrivilegeAccessGuard, NgxPermissionsGuard],
    children: [
      {
        path: 'profile',
        loadChildren: '../pages/user-profile/user-profile.module#UserProfileModule',
        data: {
          permissions: {
            only: ['administrator', 'coordinator', 'reporter', 'doctor'],
            except: []
          }
        }
      },
      {
        path: 'dashboard',
        loadChildren: '../pages/dashboard/dashboard.module#DashboardModule',
        data: {
          permissions: {
            only: ['administrator', 'coordinator', 'reporter', 'doctor'],
            except: []
          }
        }
      },
      // {
      //   path: 'users',
      //   loadChildren: '../pages/users/users.module#UsersModule',
      //   data: {
      //     permissions: {
      //       only: ['administrator'],
      //       except: ['coordinator', 'reporter', 'doctor']
      //     }
      //   }
      // },
      // {
      //   path: 'doctors',
      //   loadChildren: '../pages/doctors/doctors.module#DoctorsModule',
      //   data: {
      //     permissions: {
      //       only: ['administrator','doctor'],
      //       except: []
      //     }
      //   }
      // },

     

      
      // {
      //   path: 'specialties',
      //   loadChildren: '../pages/specialties/specialties.module#SpecialtiesModule',
      //   data: {
      //     permissions: {
      //       only: ['administrator','doctor'],
      //       except: ['coordinator', 'reporter']
      //     }
      //   }
      // },
      // {
      //   path: 'medical-centers',
      //   loadChildren: '../pages/medical-centers/medical-centers.module#MedicalCentersModule',
      //   data: {
      //     permissions: {
      //       only: ['administrator','doctor'],
      //       except: ['coordinator', 'reporter']
      //     }
      //   }
      // },
      {
        path: 'patients',
        loadChildren: '../pages/patients/patients.module#PatientsModule',
        data: {
          permissions: {
            only: ['administrator','doctor'],
            except: ['coordinator', 'reporter']
          }
        }
      },
      {
        path: 'create-consultation',
        loadChildren: '../pages/create-consultation/create-consultation.module#CreateConsultationModule',
        data: {
          permissions: {
            only: ['administrator','doctor'],
            except: ['coordinator', 'reporter']
          }
        }
      },
      // {
      //   path: 'consultations',
      //   loadChildren: '../pages/consultation/consultation.module#ConsultationModule',
      //   data: {
      //     permissions: {
      //       only: ['administrator','doctor'],
      //       except: ['coordinator', 'reporter']
      //     }
      //   }
      // },
      // {
      //   path: 'catalogs',
      //   loadChildren: '../pages/catalogs/catalogs.module#CatalogsModule',
      //   data: {
      //     permissions: {
      //       only: ['administrator','doctor'],
      //       except: ['coordinator', 'reporter']
      //     }
      //   }
      // },
      {
        path: 'error/403',
        component: ErrorPageComponent,
        data: {
          type: 'error-v6',
          code: 403,
          title: '403... Access forbidden',
          desc: 'Looks like you don\'t have permission to access for requested page.<br> Please, contact administrator'
        }
      },
      { path: 'error/:type', component: ErrorPageComponent },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
