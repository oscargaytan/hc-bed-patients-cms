import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiRestService } from '../network/api-rest.service';
import { NgxPermissionsService } from 'ngx-permissions';

@Injectable({
  providedIn: 'root'
})
export class PrivilegeAccessGuard implements CanActivateChild {
  constructor(private router: Router, private apiRest: ApiRestService, private permissionsService: NgxPermissionsService) { }

  canActivateChild(next: ActivatedRouteSnapshot) {
    const data = this.apiRest.getDataToken();
    console.log(data)
    if (data) {
      const uData = this.apiRest.getDataToken();
      const perm = [];
      switch (uData.userType) {
        case 1:
          perm.push('administrator');
          break;
        case 2:
          perm.push('doctor');
          break;
        default:
          break;
      }
      this.permissionsService.loadPermissions(perm);
      return true;
    }
    this.router.navigate(['/auth/login']);
    return false;
  }
}
