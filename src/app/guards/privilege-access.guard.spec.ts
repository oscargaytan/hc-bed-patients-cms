import { TestBed, async, inject } from '@angular/core/testing';

import { PrivilegeAccessGuard } from './privilege-access.guard';

describe('PrivilegeAccessGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrivilegeAccessGuard]
    });
  });

  it('should ...', inject([PrivilegeAccessGuard], (guard: PrivilegeAccessGuard) => {
    expect(guard).toBeTruthy();
  }));
});
