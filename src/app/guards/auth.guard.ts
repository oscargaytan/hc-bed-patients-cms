
// Angular
import { ApiRestService } from './../network/api-rest.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import * as jwt_decode from 'jwt-decode';
// RxJS
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NgxRolesService, NgxPermissionsService } from 'ngx-permissions';
// NGRX

@Injectable()
export class AuthGuard implements CanActivateChild {
	constructor(private router: Router, private apiRest: ApiRestService, private permissionsService: NgxPermissionsService) { }

	canActivateChild() {
		if (!this.apiRest.isTokenExpired()) {
			return true;
		}
		localStorage.clear();
		this.permissionsService.flushPermissions();

		this.router.navigate(['/auth/login']);
		return false;
	}
}
