export class MenuConfig {
  public defaults: any = {
    header: {
      self: {},
      items: [
      ]
    },
    aside: {
      self: {},
      items: [
        {
          title: 'Dashboards',
          root: true,
          icon: 'flaticon2-architecture-and-city',
          alignment: 'left',
          page: 'dashboard',
          translate: 'MENU.DASHBOARD',
          permission: [
            'administrator',
            'doctor'
          ]
        },
        {
          title: 'Consultation',
          root: true,
          icon: 'flaticon2-medical-records',
          page: 'create-consultation',
          translate: 'Consulta',
          permission: [
            'administrator',
            'doctor'
          ]
        },
        {
          title: 'Users',
          root: true,
          icon: 'fa fa-user-tie',
          page: 'patients',
          translate: 'Pacientes',
          permission: [
            'administrator',
            'doctor'
          ]
        },



        
        // {
        //   title: 'MedicalCenters',
        //   root: true,
        //   translate: 'Centros médicos',
        //   bullet: 'dot',
        //   icon: 'flaticon2-hospital',
        //   page: 'medical-centers',
        //   permission: [
        //     'administrator',
        //     'doctor'
        //   ]
        // },
        
        // {
        //   title: 'Specialties',
        //   root: true,
        //   icon: 'flaticon2-list',
        //   page: 'specialties',
        //   translate: 'Especialidades',
        //   permission: [
        //     'administrator',
        //     'doctor'
        //   ]
        // },

        // {
        //   title: 'Doctors',
        //   root: true,
        //   icon: 'fa fa-user-md',
        //   page: 'doctors',
        //   translate: 'MENU.DOCTORS',
        //   permission: [
        //     'administrator',
        //     'doctor'
        //   ]
        // },
        
        // {
        //   title: 'Consultations',
        //   root: true,
        //   icon: 'flaticon2-crisp-icons',
        //   page: 'consultations',
        //   translate: 'Consultas',
        //   permission: [
        //   ]
        // },
        
        // {
        //   title: 'Catalogs',
        //   root: true,
        //   icon: 'flaticon2-layers',
        //   translate: 'MENU.CATALOGS.NAME',
        //   permission: [
        //   ],
        //   submenu: [
            
        //     {
        //       title: 'MedicalCenters',
        //       translate: 'Centros médicos',
        //       bullet: 'dot',
        //       icon: 'flaticon2-hospital',
        //       page: 'medical-centers',
        //     },
        //     {
        //       title: 'Specialties',
        //       translate: 'Especialidades',
        //       bullet: 'dot',
        //       icon: 'flaticon2-list',
        //       page: 'specialties',
        //     },
        //     {
        //       title: 'Doctors',
        //       translate: 'Doctores',
        //       bullet: 'dot',
        //       icon: 'fa fa-user-md',
        //       page: 'doctors',
        //     },
            
            
        //   ]
        // },
      ]
    },
  };

  public get configs(): any {
    return this.defaults;
  }
}
//
