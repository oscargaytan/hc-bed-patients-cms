// Spain
export const locale = {
  lang: 'en',
  data: {
    CATALOGS: {
      STATUS: {
        ENABLED: 'Active',
        DISABLED: 'Disabled',
        PENDING: 'Pending activation',
        NOT_ACEPTED: 'Rejected request',
        APPROVED: 'Approved'
      },
      PRIVILEGE: {
        SUPERADMIN: 'Superadmin',
        COORDINATOR: 'Coordinator',
        REPORT: 'Reporter'
      },
      USER_TYPE: {
        PATIENT: 'Patient',
        DOCTOR: 'Specialist doctor',
        ADMINISTRATOR: 'Administrator'
      },
      ALERTS: {
        CANCEL_BUTTON: 'Cancel',
      },
      SERVER_MESSAGE: {
        TITLE: 'The server is not available',
        MSJ: 'Could not contact the server, try again in a few minutes.',
      },
      EMPTY_FIELDS: {
        TITLE: 'Information is missing to enter',
        MSJ: 'Verify the information and try again.',
      }
    },
    USER_HEADER: {
      LOGOUT: 'Sign off',
      PROFILE: 'My profile',
      PROFILE_DETAIL: 'See my data and update information',
      ALERTS: {
        LOGOUT_CONFIRM_TITLE: 'Are you sure you want to log out?',
        LOGOUT_CONFIRM_BUTTON: 'Sign off'
      }
    },
    FOOTER: {
      CONDITIONS: 'Terms and Conditions',
      PRIVACY_MESSAGE: 'Notice of Privacy',
    },
    SERVER_MESSAGE: {
      TITLE: 'The server is not available',
      MESSAGE: 'We encountered problems communicating to the server, try again later.',
    },
    TRANSLATOR: {
      SELECT: 'Choose your language',
    },
    MENU: {
      NEW: 'new',
      ACTIONS: 'Behavior',
      CREATE_POST: 'Create new publication',
      PAGES: 'Pages',
      FEATURES: 'Features',
      APPS: 'Applications',
      DASHBOARD: 'Dashboard',
      ADMIN_USERS: 'Administrators',
      USERS: 'Users',
      PROJECTS: 'Projects',
      INVESTMENT_REQUEST: 'Investment requests',
      NEWS: 'News',
      EVENTS: 'Events',
      CATALOGS: {
        NAME: 'Catalogs',
        CATEGORY: 'Categories',
        SECTOR: 'Sectors',
        PROJECT_STATUS: 'Project status',
        MARKET: 'Markets',
      }
    },
    AUTH: {
      GENERAL: {
        OR: 'Or',
        SUBMIT_BUTTON: 'Submit',
        NO_ACCOUNT: 'Don\'t have an account?',
        SIGNUP_BUTTON: 'Sign Up',
        FORGOT_BUTTON: 'Forgot Password',
        BACK_BUTTON: 'Back',
        PRIVACY: 'Privacy',
        LEGAL: 'Legal',
        CONTACT: 'Contact',
      },
      LOGIN: {
        TITLE: 'Login Account',
        BUTTON: 'Sign In',
        MESSAGES: {
          SUCCESS: 'Successful access, welcome to iPulse',
          SERVER_DIE: 'The server is not available, try again later.',
          UNREGISTRED_EMAIL: 'This email is not registered yet.',
          DISABLED_USER: 'This user is disabled, contact the admin of iPulse.',
          INVALID_DATA: 'Invalid email or password, verifiy information.',
        }
      },
      FORGOT: {
        TITLE: 'Forgotten Password?',
        DESC: 'Enter your email to reset your password',
        SUCCESS: 'Your account has been successfully reset.',
        BUTTON: 'Request password',
      },
      REGISTER: {
        TITLE: 'Sign Up',
        DESC: 'Enter your details to create your account',
        SUCCESS: 'Your account has been successfuly registered.'
      },
      INPUT: {
        EMAIL: 'Email',
        FULLNAME: 'Fullname',
        PASSWORD: 'Password',
        CONFIRM_PASSWORD: 'Confirm Password',
        USERNAME: 'Username'
      },
      VALIDATION: {
        INVALID: '{{name}} is not valid',
        REQUIRED: '{{name}} is required',
        MIN_LENGTH: '{{name}} minimum length is {{min}}',
        AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
        NOT_FOUND: 'The requested {{name}} is not found',
        INVALID_LOGIN: 'The login detail is incorrect',
        REQUIRED_FIELD: 'Required field',
        MIN_LENGTH_FIELD: 'Minimum field length:',
        MAX_LENGTH_FIELD: 'Maximum field length:',
        INVALID_FIELD: 'Field is not valid',
      }
    },
    ADMINISTRATORS: {
      INDEX: {
        TITLE: 'Manage administrators',
        TABLE: {
          ID: 'ID',
          NAME: 'Name',
          EMAIL: 'Email',
          PRIVILEGE: 'Privilege',
          STATUS: 'Status',
          ACTIONS: 'Actions',
          FILTER: 'Search',
          HELP_DISABLE: 'Disable user',
          HELP_ENABLE: 'Enable user',
        },
        ACTIONS: {
          ADD: 'Add administrator',
          EXPORT: 'Export admin users'
        },
        ALERTS: {
          ENABLE_MESSAGE: 'The user will get access to the platform again, do you want to continue with the request?',
          ENABLE: 'Enable user',
          ENABLE_SUCCESS_TITLE: 'User enabled',
          ENABLE_SUCCESS_MESSAGE: 'The information will be updated',
          DISABLE_MESSAGE: 'The user loses access to the platform, do you want to continue with the request?',
          DISABLE: 'Disable user',
          DISABLE_SUCCESS_TITLE: 'User disabled',
          DISABLE_SUCCESS_MESSAGE: 'The information will be updated',
          DELETE_MESSAGE: 'The user will be removed from the system, do you want to continue with the request?',
          DELETE: 'Delete user',
          DELETE_SUCCESS_TITLE: 'User deleted',
          DELETE_SUCCESS_MESSAGE: 'The information will be updated',
        }
      },
      ADD: {
        TITLE: 'Add new administrator',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'First name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify the name.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Last name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify last names.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Email',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'You need to specify the email.',
              EMAIL_FORMAT: 'A valid email has not been entered.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'Administrator privilege',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'You must specify the type of administrator.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Add administrator',
          RETURN: 'Back to administrators list',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'Administrator created',
          ADD_SUCCESS_MESSAGE: 'The information was sent via email to the administrator.',
          ADD_UNSUCCESS_TITLE: 'Error adding new administrator',
          ADD_UNSUCCESS_MESSAGE: 'An error occurred while creating the administrator, try later.',
          ENABLE_MESSAGE: 'The administrator will recover access to the platform, do you want to continue?',
          ENABLE: 'Reactivate administrator',
          ENABLE_SUCCESS_TITLE: 'Administrator reactivated',
          ENABLE_SUCCESS_MESSAGE: 'The information will be updated.',
          DISABLE_MESSAGE: 'The user loses access to the platform, do you want to continue with the request?',
          DISABLE: 'Disable Administrator',
          DISABLE_SUCCESS_TITLE: 'User deactivated',
          DISABLE_SUCCESS_MESSAGE: 'The information will be updated.',
          DELETE_MESSAGE: 'The user will be removed from the system, do you want to continue with the request?',
          DELETE: 'Delete user',
          DELETE_SUCCESS_TITLE: 'User deleted',
          DELETE_SUCCESS_MESSAGE: 'The information will be updated.',
        }
      },
      EDIT: {
        TITLE: 'Edit administrator',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'First name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify the name.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Last name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify last names.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Email',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'You need to specify the email.',
              EMAIL_FORMAT: 'A valid email has not been entered.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'Administrator privilege',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'You must specify the type of administrator.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Update administrator',
          RETURN: 'Back to administrators list',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'Administrator updated',
          EDIT_SUCCESS_MESSAGE: 'The information will be updated',
          EDIT_UNSUCCESS_TITLE: 'Administrator update unsuccess',
          EDIT_UNSUCCESS_MESSAGE: 'An error occurred, try later.',
        }
      },
    },
    PROJECTS: {
      INDEX: {
        TITLE: 'Manage projects',
        TABLE: {
          ID: '#',
          NAME: 'Name',
          DESC: 'Description',
          USER: 'Emp',
          DATE: 'End date',
          STATUS: 'Status',
          ACTIONS: 'Actions',
          FILTER: 'Search',
          HELP_DISABLE: 'Disable project',
          HELP_ENABLE: 'Activate project',
        },
        ACTIONS: {
          ADD: 'Add project',
        },
        ALERTS: {
        }
      },
      DETAIL: {
        TITLE: 'Details of project',
        ACTIONS: {
          RETURN: 'Volver a proyectos'
        },
        STATUS: 'Status',
        SECTOR: 'Sector',
        MARKET: 'Market',
        LEFT_DAYS: 'Left days',
        DESCRIPTION: 'Description',
        VALUATION: 'Valoración de la empresa',
        PERCENT: 'Percentage offered',
        ESTIMATED_RETURN: 'Estimated return',
        ESTIMATED_END: 'Estimated end date',
        INVESTMENT: 'Investment'
      },
    },
    PROJECTS_REQUESTS: {
      INDEX: {
        TITLE: 'Manage investment requests',
        TABLE: {
          ID: '#',
          NAME: 'Project',
          ENTERPRENEUR_USER: 'Project Entrepreneur',
          INVESTMENT_USER: 'Investor',
          ADMIN_USER: 'Administrator',
          DATE: 'Request date',
          STATUS: 'Status',
          ACTIONS: 'Actions',
          FILTER: 'Search',
          HELP_DENY: 'Reject request',
          HELP_ACCEPT: 'Approve request',
          MESSAGES: {
            DETAIL_REQUEST: 'See request information',
            EMPTY_USER: 'Not assigned',
            STATUS: {
              PENDING: 'Pending request',
              ACEPTED: 'Approved request',
              DENIED: 'Rejected request'
            }
          },
        },
        ACTIONS: {
        },
        ALERTS: {
        }
      },
      DETAIL: {
        TITLE: 'Detailed information on project investment request',
        ACTIONS: {
          RETURN: 'Return to request list'
        },
        ENTREPRENEUR_TITLE: 'Entrepreneur information',
        COMMENT_RELATION_TITLE: 'Request comment',
        INVESTMENT_TITLE: 'Investor information',
        STATUS: 'Status',
        STATUS_SUCCESS: 'Approved request',
        STATUS_ERROR: 'Rejected request',
        COMMENT: 'Request comment',
        SECTOR: 'Sector',
        MARKET: 'Market',
        LEFT_DAYS: 'Left days',
        DESCRIPTION: 'Complete description',
        VALUATION: 'Company valuation',
        PERCENT: 'Percentage offered',
        ESTIMATED_RETURN: 'Estimated return',
        ESTIMATED_END: 'Estimated end date',
        INVESTMENT: 'Investment',
        HELP_DENY: 'Reject request',
        TITLE_DENY: 'To reject the request, you must specify the reason why it should not be approved.',
        HELP_ACCEPT: 'Approve request',
        TITLE_ACCEPT: 'If you wish, you can include notes of the application, as well as recommendations for future references.',
        ALERTS: {
          TITLE_EMPTY: 'The comment is necessary to continue',
          MSJ_EMPTY: 'Verify the information and try again.',
          TITLE_APPROVED: 'The investment request has been approved',
          MSJ_APPROVED: 'The entrepreneur and the investor will be informed via email.',
          TITLE_DENIED: 'The investment request has been rejected',
          MSJ_DENIED: 'The investor will be informed via email.',
        }
      },
    },
    NEWS: {
      INDEX: {
        TITLE: 'Manage news',
        TABLE: {
          ID: 'ID',
          TITLE: 'Title',
          RESUME: 'Resume',
          DATE: 'Date',
          ACTIONS: 'Actions',
          FILTER: 'Search',
          HELP_DETAIL: 'News detail',
          HELP_APPROVE: 'Approve news',
          HELP_DELETE: 'Delete news',
          HELP_REVOKE: 'Remove news',
        },
        ACTIONS: {
        },
        ALERTS: {
          APPROVE_MESSAGE: 'The news will be published in the web application and will be visible to users, do you want to continue with the request?',
          APPROVE: 'Approve news',
          APPROVE_SUCCESS_TITLE: 'The news has been published correctly',
          APPROVE_SUCCESS_MESSAGE: 'The information will be updated.',
          DISABLE_MESSAGE: 'La noticia dejará de aparecer a los usuarios de la plataforma, ¿deseas continuar con la petición?',
          DISABLE: 'Remove news',
          DISABLE_SUCCESS_TITLE: 'News removed',
          DISABLE_SUCCESS_MESSAGE: 'If you want to add the news again you can do it from the available news tab.',
          DELETE_MESSAGE: 'The news will be deleted, do you want to continue with the request?',
          DELETE: 'Delete news',
          DELETE_SUCCESS_TITLE: 'News deleted successfully',
          DELETE_SUCCESS_MESSAGE: 'The information will be updated.',
        }
      },
      ADD: {
        TITLE: 'Add news',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'News title',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The title of the news is very short.',
              NOT_EMPTY: 'It is necessary to specify the title of the news.',
            }
          },
          BUSINESS: {
            PLACEHOLDER: 'Business name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify who provides the information.',
            }
          },
          BODY: {
            PLACEHOLDER: 'News detail',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The news information is very short.',
              NOT_EMPTY: 'It is necessary to specify details of the news.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Add news',
          RETURN: 'Back to news list',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'News added',
          ADD_SUCCESS_MESSAGE: 'The information will be updated.',
          ADD_UNSUCCESS_TITLE: 'Error adding news',
          ADD_UNSUCCESS_MESSAGE: 'The information will be updated.',
        }
      },
      EDIT: {
        TITLE: 'Edit news',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'News title',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The title of the news is very short.',
              NOT_EMPTY: 'It is necessary to specify the title of the news.',
            }
          },
          BUSINESS: {
            PLACEHOLDER: 'Business name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify who provides the information.',
            }
          },
          BODY: {
            PLACEHOLDER: 'News detail',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The news information is very short.',
              NOT_EMPTY: 'It is necessary to specify details of the news.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Update information',
          RETURN: 'Back to news list',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'News edited',
          EDIT_SUCCESS_MESSAGE: 'The information will be updated',
          EDIT_UNSUCCESS_TITLE: 'User update unsuccess',
          EDIT_UNSUCCESS_MESSAGE: 'Verify the fields and try again.',
        }
      },
      DETAIL: {
        TITLE: 'New details',
        ACTIONS: {
          EDIT: 'Update information',
          RETURN: 'Back to news list',
          PUBLISH: 'Publish in webapp',
          REMOVE: 'Delete from webapp'
        },
        ALERTS: {
        }
      },
    },
    NEWS_APPROVED: {
      INDEX: {
        TITLE: 'Manage approved news',
        TABLE: {
          ID: 'ID',
          TITLE: 'Title',
          RESUME: 'Resume',
          DATE: 'Date',
          ACTIONS: 'Actions',
          FILTER: 'Search',
        },
        ACTIONS: {
          ADD: 'Add news',
        },
        ALERTS: {
        }
      },
      ADD: {
        TITLE: 'Add news',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'News title',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The title of the news is very short.',
              NOT_EMPTY: 'It is necessary to specify the title of the news.',
            }
          },
          BUSINESS: {
            PLACEHOLDER: 'Business name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify who provides the information.',
            }
          },
          BODY: {
            PLACEHOLDER: 'News detail',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The news information is very short.',
              NOT_EMPTY: 'It is necessary to specify details of the news.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Add news',
          RETURN: 'Back to news list',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'News added',
          ADD_SUCCESS_MESSAGE: 'The information will be updated.',
          ADD_UNSUCCESS_TITLE: 'Error adding news',
          ADD_UNSUCCESS_MESSAGE: 'The information will be updated.',
        }
      },
      EDIT: {
        TITLE: 'Edit news',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'News title',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The title of the news is very short.',
              NOT_EMPTY: 'It is necessary to specify the title of the news.',
            }
          },
          BUSINESS: {
            PLACEHOLDER: 'Business name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify who provides the information.',
            }
          },
          BODY: {
            PLACEHOLDER: 'News detail',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The news information is very short.',
              NOT_EMPTY: 'It is necessary to specify details of the news.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Update information',
          RETURN: 'Back to news list',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'News edited',
          EDIT_SUCCESS_MESSAGE: 'The information will be updated',
          EDIT_UNSUCCESS_TITLE: 'User update unsuccess',
          EDIT_UNSUCCESS_MESSAGE: 'Verify the fields and try again.',
        }
      },
      DETAIL: {
        TITLE: 'New details',
        ACTIONS: {
          EDIT: 'Update information',
          RETURN: 'Back to news list',
          PUBLISH: 'Publish in webapp',
          REMOVE: 'Delete from webapp'
        },
        ALERTS: {
        }
      },
    },
    EVENTS: {
      INDEX: {
        TITLE: 'Manage events',
        TABLE: {
          ID: '#',
          TITLE: 'Name',
          PLACE: 'Place',
          DATE: 'Date',
          ACTIONS: 'Actions',
          FILTER: 'Search',
          HELP_DETAIL: 'Event detail',
          HELP_APPROVE: 'Approve event',
          HELP_DELETE: 'Delete event',
          HELP_REVOKE: 'Remove web application event',
        },
        ACTIONS: {
          ADD: 'Add event',
          EXPORT: 'Export event'
        },
        ALERTS: {
          APPROVE_MESSAGE: 'The event will be published in the web application and will be visible to users, do you want to continue with the request?',
          APPROVE: 'Approve event',
          APPROVE_SUCCESS_TITLE: 'The event has been published successfully',
          APPROVE_SUCCESS_MESSAGE: 'The information will be updated below.',
          DISABLE_MESSAGE: 'The event will stop appearing to the users of the platform, do you want to continue with the request?',
          DISABLE: 'Remove event',
          DISABLE_SUCCESS_TITLE: 'Event removed',
          DISABLE_SUCCESS_MESSAGE: 'If you want to add the event again you can do it from the tab of available events.',
          DELETE_MESSAGE: 'The event will be deleted, do you want to continue with the request?',
          DELETE: 'Delete event',
          DELETE_SUCCESS_TITLE: 'Event successfully deleted',
          DELETE_SUCCESS_MESSAGE: 'The information will be updated.',
        }
      },
      ADD: {
        TITLE: 'Add new event',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Name of the event',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The name of the event is very short.',
              NOT_EMPTY: 'It is necessary to specify the name of the event.',
            }
          },
          PLACE: {
            PLACEHOLDER: 'Place of the event',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify where the event will be.',
            }
          },
          BODY: {
            PLACEHOLDER: 'Event detail',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'Event information is very short.',
              NOT_EMPTY: 'It is necessary to specify event details.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Add event',
          RETURN: 'Back to event list',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'Event added',
          ADD_SUCCESS_MESSAGE: 'The information will be updated.',
          ADD_UNSUCCESS_TITLE: 'Error adding event',
          ADD_UNSUCCESS_MESSAGE: 'Verify the information and try again or later.',
        }
      },
      EDIT: {
        TITLE: 'Edit event',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Name of the event',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The name of the event is very short.',
              NOT_EMPTY: 'It is necessary to specify the name of the event.',
            }
          },
          PLACE: {
            PLACEHOLDER: 'Place of the event',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify where the event will be.',
            }
          },
          BODY: {
            PLACEHOLDER: 'Event detail',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'Event information is very short.',
              NOT_EMPTY: 'It is necessary to specify event details.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Update information',
          RETURN: 'Back to events list',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'Event edited',
          EDIT_SUCCESS_MESSAGE: 'The information will be updated',
          EDIT_UNSUCCESS_TITLE: 'Event update unsuccess',
          EDIT_UNSUCCESS_MESSAGE: 'Verify the information and try again or later.',
        }
      },
      DETAIL: {
        TITLE: 'Event details',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Name of the event',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The name of the event is very short.',
              NOT_EMPTY: 'It is necessary to specify the name of the event.',
            }
          },
          PLACE: {
            PLACEHOLDER: 'Place of the event',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify where the event will be.',
            }
          },
          DATE: {
            PLACEHOLDER: 'Event date',
          },
          BODY: {
            PLACEHOLDER: 'Event detail',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'Event information is very short.',
              NOT_EMPTY: 'It is necessary to specify event details.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Event edit',
          RETURN: 'Back to events',
          PUBLISH: 'Publish event on webapp',
          REMOVE: 'Delete event from webapp'
        },
        ALERTS: {
        }
      },
    },
    EVENTS_APPROVED: {
      INDEX: {
        TITLE: 'Manage available events',
        TABLE: {
          ID: '#',
          TITLE: 'Name',
          PLACE: 'Place',
          DATE: 'Date',
          ACTIONS: 'Actions',
          FILTER: 'Search',
          HELP_DETAIL: 'Event detail',
          HELP_APPROVE: 'Approve event',
          HELP_DELETE: 'Delete event',
          HELP_REVOKE: 'Remove web application event',
        },
        ACTIONS: {
          ADD: 'Add event',
        },
        ALERTS: {
        }
      },
      ADD: {
        TITLE: 'Add new event',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Name of the event',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The name of the event is very short.',
              NOT_EMPTY: 'It is necessary to specify the name of the event.',
            }
          },
          PLACE: {
            PLACEHOLDER: 'Place of the event',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify where the event will be.',
            }
          },
          BODY: {
            PLACEHOLDER: 'Event detail',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'Event information is very short.',
              NOT_EMPTY: 'It is necessary to specify event details.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Add event',
          RETURN: 'Back to event list',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'Event added',
          ADD_SUCCESS_MESSAGE: 'The information will be updated.',
          ADD_UNSUCCESS_TITLE: 'Error adding event',
          ADD_UNSUCCESS_MESSAGE: 'Verify the information and try again or later.',
        }
      },
      EDIT: {
        TITLE: 'Edit event',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Name of the event',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The name of the event is very short.',
              NOT_EMPTY: 'It is necessary to specify the name of the event.',
            }
          },
          PLACE: {
            PLACEHOLDER: 'Place of the event',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify where the event will be.',
            }
          },
          BODY: {
            PLACEHOLDER: 'Event detail',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'Event information is very short.',
              NOT_EMPTY: 'It is necessary to specify event details.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Update information',
          RETURN: 'Back to events list',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'Event edited',
          EDIT_SUCCESS_MESSAGE: 'The information will be updated',
          EDIT_UNSUCCESS_TITLE: 'Event update unsuccess',
          EDIT_UNSUCCESS_MESSAGE: 'Verify the information and try again or later.',
        }
      },
      DETAIL: {
        TITLE: 'Event details',
        ACTIONS: {
          EDIT: 'Event edit',
          RETURN: 'Back to events',
          PUBLISH: 'Publish event on webapp',
          REMOVE: 'Delete event from webapp'
        },
        ALERTS: {
        }
      },
    },
    USERS: {
      INDEX: {
        TITLE: 'Manage users',
        TABLE: {
          ID: 'ID',
          NAME: 'Name',
          EMAIL: 'Email',
          TYPE: 'User type',
          STATUS: 'Status',
          ACTIONS: 'Actions',
          FILTER: 'Search',
          HELP_DISABLE: 'Disable user',
          HELP_ENABLE: 'Enable user',
        },
        ACTIONS: {
          ADD: 'Add user',
          EXPORT: 'Export users'
        },
        ALERTS: {
          ENABLE_MESSAGE: 'The user will get access to the platform again, do you want to continue with the request?',
          ENABLE: 'Enable user',
          ENABLE_SUCCESS_TITLE: 'User enabled',
          ENABLE_SUCCESS_MESSAGE: 'The information will be updated',
          DISABLE_MESSAGE: 'The user loses access to the platform, do you want to continue with the request?',
          DISABLE: 'Disable user',
          DISABLE_SUCCESS_TITLE: 'User disabled',
          DISABLE_SUCCESS_MESSAGE: 'The information will be updated',
          DELETE_MESSAGE: 'The user will be removed from the system, do you want to continue with the request?',
          DELETE: 'Delete user',
          DELETE_SUCCESS_TITLE: 'User deleted',
          DELETE_SUCCESS_MESSAGE: 'The information will be updated',
        }
      },
      ADD: {
        TITLE: 'Add new user',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'First name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify the name.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Last name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify last names.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Email',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'You need to specify the email.',
              EMAIL_FORMAT: 'A valid email has not been entered.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'User type',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'You must specify the type of user.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Add user',
          RETURN: 'Back to users list',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'User added',
          ADD_SUCCESS_MESSAGE: 'The information will be updated',
          ADD_UNSUCCESS_TITLE: 'User add unsuccess',
          ADD_UNSUCCESS_MESSAGE: 'The information will be updated',
        }
      },
      EDIT: {
        TITLE: 'Edit user',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'First name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify the name.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Last name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify last names.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Email',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'You need to specify the email.',
              EMAIL_FORMAT: 'A valid email has not been entered.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'User type',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'You must specify the type of user.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Update information',
          RETURN: 'Back to users list',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'User edited',
          EDIT_SUCCESS_MESSAGE: 'The information will be updated',
          EDIT_UNSUCCESS_TITLE: 'User update unsuccess',
          EDIT_UNSUCCESS_MESSAGE: 'The information will be updated',
        }
      },
    },
    USERS_REQUESTS: {
      INDEX: {
        TITLE: 'Manage users requests',
        TABLE: {
          ID: 'ID',
          EMAIL: 'Email',
          TYPE: 'User type',
          STATUS: 'Status',
          ACTIONS: 'Actions',
          FILTER: 'Search',
        },
        ACTIONS: {
        },
        ALERTS: {
          ENABLE_MESSAGE: 'The user will get access to the platform, do you want to continue with the request?',
          ENABLE: 'Accept user request',
          ENABLE_SUCCESS_TITLE: 'User enabled',
          ENABLE_SUCCESS_MESSAGE: 'The information will be updated',
          DISABLE_MESSAGE: 'The user request will not approve, do you want to continue with the request?',
          DISABLE: 'Disable user',
          DISABLE_SUCCESS_TITLE: 'User not approved',
          DISABLE_SUCCESS_MESSAGE: 'The information will be updated',
          SUCCESS_TITLE: 'Investment request accepted',
          SUCCESS_MESSAGE: 'The user will be notified.',
          UNSUCCESS_TITLE: 'Investment request rejected',
          UNSUCCESS_MESSAGE: 'The user will be notified.',
        }
      },
    },
    CATALOGS_LIST: {
      PROJECT_STATUS: {
        INDEX: {
          TITLE: 'Manage project status',
          TABLE: {
            ID: 'ID',
            NAME: 'Name',
            ACTIONS: 'Actions',
            FILTER: 'Search',
          },
          ACTIONS: {
            ADD: 'Add project status',
          },
          ALERTS: {
            DELETE_MESSAGE: 'The status will be removed from the system for projects, do you want to continue with the request?',
            DELETE: 'Delete status',
            DELETE_SUCCESS_TITLE: 'Status deleted',
            DELETE_SUCCESS_MESSAGE: 'The information will be updated',
          }
        },
        ADD: {
          TITLE: 'Add new project status',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Status',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'It is necessary to specify the name of status.',
              }
            },
          },
          ACTIONS: {
            ADD: 'Add project status',
            RETURN: 'Back to project status list',
          },
          ALERTS: {
            ADD_SUCCESS_TITLE: 'User added',
            ADD_SUCCESS_MESSAGE: 'The information will be updated',
            ADD_UNSUCCESS_TITLE: 'User add unsuccess',
            ADD_UNSUCCESS_MESSAGE: 'The information will be updated',
          }
        },
        EDIT: {
          TITLE: 'Edit project status',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Status',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'It is necessary to specify the name of status.',
              }
            },
          },
          ACTIONS: {
            EDIT: 'Update information',
            RETURN: 'Back to project status list',
          },
          ALERTS: {
            EDIT_SUCCESS_TITLE: 'Status edited',
            EDIT_SUCCESS_MESSAGE: 'The information will be updated',
            EDIT_UNSUCCESS_TITLE: 'Status update unsuccess',
            EDIT_UNSUCCESS_MESSAGE: 'The information will be updated',
          }
        },
      },
      PROJECT_CATEGORY: {
        INDEX: {
          TITLE: 'Manage project category',
          TABLE: {
            ID: 'ID',
            NAME: 'Name',
            ACTIONS: 'Actions',
            FILTER: 'Search',
          },
          ACTIONS: {
            ADD: 'Add project category',
          },
          ALERTS: {
            DELETE_MESSAGE: 'The category will be removed from the system for projects, do you want to continue with the request?',
            DELETE: 'Delete category',
            DELETE_SUCCESS_TITLE: 'Category deleted',
            DELETE_SUCCESS_MESSAGE: 'The information will be updated',
          }
        },
        ADD: {
          TITLE: 'Add new project category',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Category',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'It is necessary to specify the name of category.',
              }
            },
          },
          ACTIONS: {
            ADD: 'Add project category',
            RETURN: 'Back to project categories list',
          },
          ALERTS: {
            ADD_SUCCESS_TITLE: 'Category added',
            ADD_SUCCESS_MESSAGE: 'The information will be updated',
            ADD_UNSUCCESS_TITLE: 'Category add unsuccess',
            ADD_UNSUCCESS_MESSAGE: 'The information will be updated',
          }
        },
        EDIT: {
          TITLE: 'Edit project category',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Category',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'It is necessary to specify the name of category.',
              }
            },
          },
          ACTIONS: {
            EDIT: 'Update information',
            RETURN: 'Back to project categories list',
          },
          ALERTS: {
            EDIT_SUCCESS_TITLE: 'Category edited',
            EDIT_SUCCESS_MESSAGE: 'The information will be updated',
            EDIT_UNSUCCESS_TITLE: 'Category update unsuccess',
            EDIT_UNSUCCESS_MESSAGE: 'The information will be updated',
          }
        },
      },
      PROJECT_SECTOR: {
        INDEX: {
          TITLE: 'Manage project sector',
          TABLE: {
            ID: 'ID',
            NAME: 'Name',
            ACTIONS: 'Actions',
            FILTER: 'Search',
          },
          ACTIONS: {
            ADD: 'Add project sector',
          },
          ALERTS: {
            DELETE_MESSAGE: 'The sector will be removed from the system for projects, do you want to continue with the request?',
            DELETE: 'Delete sector',
            DELETE_SUCCESS_TITLE: 'Sector deleted',
            DELETE_SUCCESS_MESSAGE: 'The information will be updated',
          }
        },
        ADD: {
          TITLE: 'Add new project sector',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Sector',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'It is necessary to specify the name of sector.',
              }
            },
          },
          ACTIONS: {
            ADD: 'Add project sector',
            RETURN: 'Back to project sectors list',
          },
          ALERTS: {
            ADD_SUCCESS_TITLE: 'Sector added',
            ADD_SUCCESS_MESSAGE: 'The information will be updated',
            ADD_UNSUCCESS_TITLE: 'Sector add unsuccess',
            ADD_UNSUCCESS_MESSAGE: 'The information will be updated',
          }
        },
        EDIT: {
          TITLE: 'Edit project sector',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Sector',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'It is necessary to specify the name of sector.',
              }
            },
          },
          ACTIONS: {
            EDIT: 'Update information',
            RETURN: 'Back to project sectors list',
          },
          ALERTS: {
            EDIT_SUCCESS_TITLE: 'Sector edited',
            EDIT_SUCCESS_MESSAGE: 'The information will be updated',
            EDIT_UNSUCCESS_TITLE: 'Sector update unsuccess',
            EDIT_UNSUCCESS_MESSAGE: 'The information will be updated',
          }
        },
      },
      PROJECT_MARKET: {
        INDEX: {
          TITLE: 'Manage project market',
          TABLE: {
            ID: 'ID',
            NAME: 'Name',
            ACTIONS: 'Actions',
            FILTER: 'Search',
          },
          ACTIONS: {
            ADD: 'Add project market',
          },
          ALERTS: {
            DELETE_MESSAGE: 'The market will be removed from the system for projects, do you want to continue with the request?',
            DELETE: 'Delete market',
            DELETE_SUCCESS_TITLE: 'Market deleted',
            DELETE_SUCCESS_MESSAGE: 'The information will be updated',
          }
        },
        ADD: {
          TITLE: 'Add new project market',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Market',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'It is necessary to specify the name of market.',
              }
            },
          },
          ACTIONS: {
            ADD: 'Add project market',
            RETURN: 'Back to project markets list',
          },
          ALERTS: {
            ADD_SUCCESS_TITLE: 'Market added',
            ADD_SUCCESS_MESSAGE: 'The information will be updated',
            ADD_UNSUCCESS_TITLE: 'Market add unsuccess',
            ADD_UNSUCCESS_MESSAGE: 'The information will be updated',
          }
        },
        EDIT: {
          TITLE: 'Edit project market',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Market',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'It is necessary to specify the name of market.',
              }
            },
          },
          ACTIONS: {
            EDIT: 'Update information',
            RETURN: 'Back to project markets list',
          },
          ALERTS: {
            EDIT_SUCCESS_TITLE: 'Market edited',
            EDIT_SUCCESS_MESSAGE: 'The information will be updated',
            EDIT_UNSUCCESS_TITLE: 'Market update unsuccess',
            EDIT_UNSUCCESS_MESSAGE: 'The information will be updated',
          }
        },
      },
    },
    PROFILE: {
      BUTTONS: {
        RESET: 'Reset',
        SAVE: 'Update information',
        SAVE_EXIT: 'Update user and back to dashboard',
        SAVE_CONTINUE: 'Update information'
      },
      DETAIL: {
        TITLE: 'Update user details',
        INPUTS: {
          FIRST_NAME: {
            PLACEHOLDER: 'First name',
            EMPTY_FIELD: 'Please enter the first name.'
          },
          LAST_NAME: {
            PLACEHOLDER: 'Last name',
            EMPTY_FIELD: 'Please enter the last name.'
          },
          EMAIL: {
            PLACEHOLDER: 'Email',
            EMPTY_FIELD: 'Please enter the email.'
          },
          TYPE: {
            PLACEHOLDER: 'Privilege',
            EMPTY_FIELD: 'Privilege for your user.'
          },
          CREATED: {
            PLACEHOLDER: 'Member created date',
            EMPTY_FIELD: 'Member created date.'
          }
        }
      },
      CHANGE_PASSWORD: {
        TITLE: 'Update password',
        INPUTS: {
          PASSWORD: {
            NAME: 'First name',
            PLACEHOLDER: 'Please enter the first name.'
          },
          PASSWORD_CHECK: {
            NAME: 'Last name',
            PLACEHOLDER: 'Please enter the last name.'
          }
        },
        ALERTS: {
          PASSWORDS_MATCH_TITLE: 'Check the passwords',
          PASSWORDS_MATCH_MSJ: 'Passwords not match.',
          SERVER_ERROR_TITLE: 'Check the passwords',
          SERVER_ERROR_MSJ: 'Passwords not match.',
          PASSWORD_INVALID_TITLE: 'Check the passwords',
          PASSWORD_INVALID_MSJ: 'Passwords not match.'
        }
      }
    },
    PLANS: {
      INDEX: {
        TITLE: 'Manage subscription plans',
        TABLE: {
          ID: '#',
          NAME: 'Name',
          AMOUNT: 'Cost',
          SUBSCRIBED_USERS: 'Subscribed users',
          DURATION: 'Subscription duration',
          STATUS: 'Status',
          ACTIONS: 'Actions',
          FILTER: 'Search',
          HELP_DISABLE: 'Deactivate plan',
          HELP_ENABLE: 'Reactivate plan',
        },
        ACTIONS: {
          ADD: 'Add new plan',
        },
        ALERTS: {
          EXIST_PLAN: 'The plan already exists, try a different name.',
          AMOUNT_MSJ: 'The amount must be less than or equal to 3000.',
          AMOUNT_TITLE: 'An error has occurred',
          ENABLE_MESSAGE: 'The plan will can use on the platform again, do you want to continue with the request?',
          ENABLE: 'Reactivate plan',
          ENABLE_SUCCESS_TITLE: 'Plan reactivated',
          ENABLE_SUCCESS_MESSAGE: 'The information will be updated.',
          DISABLE_MESSAGE: 'The plan can not use on the platform, do you want to continue with the request?',
          DISABLE: 'Deactivate plan',
          DISABLE_SUCCESS_TITLE: 'Plan deactivated',
          DISABLE_SUCCESS_MESSAGE: 'The information will be updated.',
          DELETE_MESSAGE: 'The plan will be removed from the system, do you want to continue with the request?',
          DELETE: 'Delete plan',
          DELETE_SUCCESS_TITLE: 'Plan removed',
          DELETE_SUCCESS_MESSAGE: 'The information will be updated.',
        }
      },
      ADD: {
        TITLE: 'Add new plan',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Plan name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'The name of the plan is very short.',
              NOT_EMPTY: 'You need to specify the name for the plan.',
            }
          },
          CURRENCY: {
            PLACEHOLDER: 'Currency to use',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar la divisa para el plan.',
            }
          },
          AMOUNT: {
            PLACEHOLDER: 'Costo de plan',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify the cost that the plan will have.',
              NOT_NUMBER: 'The value must be an integer or decimal number.',
            }
          },
          FRECUENCY: {
            PLACEHOLDER: 'Billing frequency',
            HELP_TEXT: '',
            PERIOD: 'Month',
            PERIODS: 'Months',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify the frequency of collection of the plan.',
            }
          },
          DURATION: {
            PLACEHOLDER: 'Subscription duration',
            DESC_INIT: 'The subscription will be charged',
            DESC_END: 'times.',
          },
        },
        ACTIONS: {
          ADD: 'Add plan',
          RETURN: 'Return to list of plans',
        },
        ALERTS: {
          EXIST_PLAN: 'The plan already exists, try a different name.',
          AMOUNT_MSJ: 'The amount must be less than or equal to 3000.',
          AMOUNT_TITLE: 'An error has occurred',
          ADD_SUCCESS_TITLE: 'Plan created successfully',
          ADD_SUCCESS_MESSAGE: 'The information will be updated.',
          ADD_UNSUCCESS_TITLE: 'An error occurred while creating the plan',
          ADD_UNSUCCESS_MESSAGE: 'Verify the information and try later.',
        }
      },
      DETAIL: {
        TITLE: 'Plan information',
        ACTIONS: {
          RETURN: 'Back to plans'
        },
        STATUS: 'Stage',
        SECTOR: 'Sector',
        MARKET: 'Market',
        LEFT_DAYS: 'Remaining days',
        DESCRIPTION: 'Complete description',
        VALUATION: 'Company valuation',
        PERCENT: 'Percentage offered',
        ESTIMATED_RETURN: 'Estimated return',
        ESTIMATED_END: 'Estimated end date',
        INVESTMENT: 'Investment'
      },
    },
  }
};
