// Spain
export const locale = {
  lang: 'es',
  data: {
    CATALOGS: {
      STATUS: {
        ENABLED: 'Activo',
        DISABLED: 'Desactivado',
        PENDING: 'Pendiente activación',
        NOT_ACEPTED: 'Solicitud rechazada',
        APPROVED: 'Aceptado'
      },
      PRIVILEGE: {
        SUPERADMIN: 'Superadministrador',
        COORDINATOR: 'Coordinador',
        REPORT: 'Reporteador'
      },
      USER_TYPE: {
        PATIENT: 'Paciente',
        DOCTOR: 'Doctor Especialista',
        ADMINISTRATOR: 'Administrador'
      },
      ALERTS: {
        CANCEL_BUTTON: 'Cancelar',
      },
      SERVER_MESSAGE: {
        TITLE: 'El servidor no está disponible',
        MSJ: 'No se ha podido contactar con el servidor, intenta nuevamente en unos minutos.',
      },
      EMPTY_FIELDS: {
        TITLE: 'Falta información por ingresar',
        MSJ: 'Verifica la información e intenta nuevamente.',
      }
    },
    USER_HEADER: {
      LOGOUT: 'Cerrar sesión',
      PROFILE: 'Mi perfil',
      PROFILE_DETAIL: 'Ver mis datos y actualizar información',
      ALERTS: {
        LOGOUT_CONFIRM_TITLE: '¿Estás seguro que deseas cerrar la sesión?',
        LOGOUT_CONFIRM_BUTTON: 'Cerrar sesión'
      }
    },
    FOOTER: {
      CONDITIONS: 'Términos y condiciones',
      PRIVACY_MESSAGE: 'Aviso de privacidad',
    },
    SERVER_MESSAGE: {
      TITLE: 'El servidor no esta disponible.',
      MESSAGE: 'Nos encontramos con problemas para comunicarnos al servidor, intenta nuevamente más tarde.',
    },
    TRANSLATOR: {
      SELECT: 'Elige tu idioma',
    },
    MENU: {
      NEW: 'nuevo',
      ACTIONS: 'Comportamiento',
      CREATE_POST: 'Crear nueva publicación',
      PAGES: 'Pages',
      FEATURES: 'Caracteristicas',
      APPS: 'Aplicaciones',
      DASHBOARD: 'Tablero',
      ADMIN_USERS: 'Administradores',
      USERS: 'Usuarios',
      DOCTORS: 'Doctores',
      PATIENTS: 'Pacientes',
      PROJECTS: 'Proyectos',
      INVESTMENT_REQUEST: 'Solicitudes de inversión',
      NEWS: 'Noticias',
      EVENTS: 'Eventos',
      PLANS: 'Planes',
      CATALOGS: {
        NAME: 'Catálogos',
        CATEGORY: 'Categorías',
        SECTOR: 'Sectores',
        PROJECT_STATUS: 'Estatus de proyecto',
        MARKET: 'Mercados',
      }
    },
    AUTH: {
      GENERAL: {
        OR: 'O',
        SUBMIT_BUTTON: 'Enviar',
        NO_ACCOUNT: 'No tienes una cuenta?',
        SIGNUP_BUTTON: 'Regístrate',
        FORGOT_BUTTON: 'Recuperar contraseña',
        BACK_BUTTON: 'Volver',
        PRIVACY: 'Intimidad',
        LEGAL: 'Legal',
        CONTACT: 'Contacto',
      },
      LOGIN: {
        TITLE: 'Iniciar sesión',
        BUTTON: 'Acceder',
        MESSAGES: {
          SUCCESS: 'Acceso exitoso, bienvenido a iPulse',
          SERVER_DIE: 'El servidor no se encuentra disponible, intenta más tarde.',
          UNREGISTRED_EMAIL: 'El correo electrónico no se encuentra registrado.',
          DISABLED_USER: 'El usuario se encuentra desactivado, contacta al administrador.',
          INVALID_DATA: 'Usuario o contraseña incorrectos, verifica la información.',
        }
      },
      FORGOT: {
        TITLE: '¿Olvidaste tu contraseña?',
        DESC: 'Ingrese su correo electrónico para restablecer su contraseña',
        SUCCESS: 'Tu contraseña se ha restablecido correctamente.',
        BUTTON: 'Solicitar contraseña',
      },
      REGISTER: {
        TITLE: 'Sign Up',
        DESC: 'Enter your details to create your account',
        SUCCESS: 'Your account has been successfuly registered.'
      },
      INPUT: {
        EMAIL: 'Correo electrónico',
        FULLNAME: 'Nombre',
        PASSWORD: 'Contraseña',
        CONFIRM_PASSWORD: 'Confirmar contraseña',
        USERNAME: 'Usuario'
      },
      VALIDATION: {
        INVALID: '{{name}} is not valid',
        REQUIRED: '{{name}} is required',
        MIN_LENGTH: '{{name}} minimum length is {{min}}',
        AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
        NOT_FOUND: 'The requested {{name}} is not found',
        INVALID_LOGIN: 'The login detail is incorrect',
        REQUIRED_FIELD: 'Required field',
        MIN_LENGTH_FIELD: 'Minimum field length:',
        MAX_LENGTH_FIELD: 'Maximum field length:',
        INVALID_FIELD: 'Field is not valid',
      }
    },
    ADMINISTRATORS: {
      INDEX: {
        TITLE: 'Gestionar administradores',
        TABLE: {
          ID: '#',
          NAME: 'Nombre',
          EMAIL: 'Correo',
          PRIVILEGE: 'Privilegio',
          STATUS: 'Estatus',
          ACTIONS: 'Acciones',
          FILTER: 'Buscar',
          HELP_DISABLE: 'Desactivar usuario',
          HELP_ENABLE: 'Activar usuario',
        },
        ACTIONS: {
          ADD: 'Añadir administrador',
          EXPORT: 'Exportar usuarios administradores'
        },
        ALERTS: {
          ENABLE_MESSAGE: 'El usuario obtendrá acceso nuevamente a la plataforma, ¿desea continuar con la solicitud?',
          ENABLE: 'Reactivar usuario',
          ENABLE_SUCCESS_TITLE: 'Usuario reactivado',
          ENABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DISABLE_MESSAGE: 'El usuario perderá el acceso a la plataforma, ¿desea continuar con la solicitud?',
          DISABLE: 'Desactivar usuario',
          DISABLE_SUCCESS_TITLE: 'Usuario desactivado',
          DISABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DELETE_MESSAGE: 'El usuario será eliminado del sistema, ¿desea continuar con la solicitud?',
          DELETE: 'Eliminar usuario',
          DELETE_SUCCESS_TITLE: 'Usuario eliminado',
          DELETE_SUCCESS_MESSAGE: 'La información será actualizada.',
        }
      },
      ADD: {
        TITLE: 'Agregar nuevo administrador',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Nombres',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar el nombre.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Apellidos',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar los apellidos.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Correo electrónico',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'Correo no cumple con la longitud mínima requerida.',
              NOT_EMPTY: 'Es necesario especificar el correo electrónico.',
              EMAIL_FORMAT: 'No se ha ingresado un correo válido.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'Tipo de administrador',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'Debes especificar el tipo de administrador.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Añadir administrador',
          RETURN: 'Volver a lista de administradores',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'Administrador dado de alta',
          ADD_SUCCESS_MESSAGE: 'Se envió la información vía email al administrador.',
          ADD_UNSUCCESS_TITLE: 'Error al añadir el nuevo administrador',
          ADD_UNSUCCESS_MESSAGE: 'Ocurrio un error al crear el administrador, intenta más tarde.',
          ENABLE_MESSAGE: 'El administrador recuperará el acceso a la plataforma, ¿deseas continuar?',
          ENABLE: 'Reactivar administrador',
          ENABLE_SUCCESS_TITLE: 'Administrador reactivado',
          ENABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DISABLE_MESSAGE: 'The user loses access to the platform, do you want to continue with the request?',
          DISABLE: 'Desactivar administrador',
          DISABLE_SUCCESS_TITLE: 'Usuario desactivado',
          DISABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DELETE_MESSAGE: 'The user will be removed from the system, do you want to continue with the request?',
          DELETE: 'Delete user',
          DELETE_SUCCESS_TITLE: 'User deleted',
          DELETE_SUCCESS_MESSAGE: 'La información será actualizada.',
        }
      },
      EDIT: {
        TITLE: 'Editar usuario',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Nombres',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario ingresar el nombre.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Apellidos',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario ingresar los apellidos.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Correo',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'El correo es necesario.',
              EMAIL_FORMAT: 'El formato del correo electrónico no es válido.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'Tipo de usuario',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'You must specify the type of user.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Actualizar usuario',
          RETURN: 'Volver a lista de usuarios',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'User edited',
          EDIT_SUCCESS_MESSAGE: 'The information will be updated',
          EDIT_UNSUCCESS_TITLE: 'User update unsuccess',
          EDIT_UNSUCCESS_MESSAGE: 'The information will be updated',
        }
      },
    },
    NEWS: {
      INDEX: {
        TITLE: 'Gestionar noticias disponibles',
        TABLE: {
          ID: '#',
          TITLE: 'Título',
          RESUME: 'Resumen',
          DATE: 'Fecha',
          ACTIONS: 'Acciones',
          FILTER: 'Buscar',
          HELP_DETAIL: 'Detalle de noticia',
          HELP_APPROVE: 'Aprobar noticia',
          HELP_DELETE: 'Borrar noticia',
          HELP_REVOKE: 'Eliminar noticia de aplicación web',
        },
        ACTIONS: {
        },
        ALERTS: {
          APPROVE_MESSAGE: 'La noticia será publicada en la aplicación web y será visible para los usuarios, ¿deseas continuar con la petición?',
          APPROVE: 'Aprobar noticia',
          APPROVE_SUCCESS_TITLE: 'La noticia se ha publicado correctamente',
          APPROVE_SUCCESS_MESSAGE: 'La información será actualizada a continuación',
          DISABLE_MESSAGE: 'La noticia dejará de aparecer a los usuarios de la plataforma, ¿deseas continuar con la petición?',
          DISABLE: 'Retirar noticia',
          DISABLE_SUCCESS_TITLE: 'Noticia removida',
          DISABLE_SUCCESS_MESSAGE: 'Si desea volver a añadir la noticia puede hacerlo desde la pestaña de noticias disponibles.',
          DELETE_MESSAGE: 'La noticia será eliminada, ¿deseas continuar con la petición?',
          DELETE: 'Eliminar noticia',
          DELETE_SUCCESS_TITLE: 'Noticia eliminada correctamente',
          DELETE_SUCCESS_MESSAGE: 'La información será actualizada.',
        }
      },
      ADD: {
        TITLE: 'Agregar noticia',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Título de la noticia',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'El título de la noticia es muy corto.',
              NOT_EMPTY: 'Es necesario especificar el título de la noticia.',
            }
          },
          BUSINESS: {
            PLACEHOLDER: 'Fuente',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar quien provee la información.',
            }
          },
          BODY: {
            PLACEHOLDER: 'Detalle de la noticia',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'La información de la noticia es muy corta.',
              NOT_EMPTY: 'Es necesario especificar detalles de la noticia.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Añadir noticia',
          RETURN: 'Volver a listado de noticias',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'Noticia añadida',
          ADD_SUCCESS_MESSAGE: 'La información será actualizada.',
          ADD_UNSUCCESS_TITLE: 'Error al añadir noticia',
          ADD_UNSUCCESS_MESSAGE: 'La información será actualizada.',
        }
      },
      EDIT: {
        TITLE: 'Editar noticia',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Título de la noticia',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'El título de la noticia es muy corto.',
              NOT_EMPTY: 'Es necesario especificar el título de la noticia.',
            }
          },
          BUSINESS: {
            PLACEHOLDER: 'Fuente',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar quien provee la información.',
            }
          },
          BODY: {
            PLACEHOLDER: 'Detalle de la noticia',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'La información de la noticia es muy corta.',
              NOT_EMPTY: 'Es necesario especificar detalles de la noticia.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Actualizar información',
          RETURN: 'Volver a listado de noticias',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'Noticia editada correctamente',
          EDIT_SUCCESS_MESSAGE: 'La infrmación será actualizada.',
          EDIT_UNSUCCESS_TITLE: 'Error al actualizar información',
          EDIT_UNSUCCESS_MESSAGE: 'Verifica la información e intenta más tarde.',
        }
      },
      DETAIL: {
        TITLE: 'Detalles de noticia',
        ACTIONS: {
          EDIT: 'Update information',
          RETURN: 'Volver a noticias',
          PUBLISH: 'Publicar noticia en webapp',
          REMOVE: 'Eliminar noticia de webapp'
        },
        ALERTS: {
        }
      },
    },
    NEWS_APPROVED: {
      INDEX: {
        TITLE: 'Gestionar noticias aprobadas',
        TABLE: {
          ID: '#',
          TITLE: 'Título',
          RESUME: 'Resumen',
          DATE: 'Fecha',
          ACTIONS: 'Acciones',
          FILTER: 'Buscar',
        },
        ACTIONS: {
          ADD: 'Añadir noticia',
        },
        ALERTS: {
        }
      },
      ADD: {
        TITLE: 'Agregar noticia',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Título de la noticia',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'El título de la noticia es muy corto.',
              NOT_EMPTY: 'Es necesario especificar el título de la noticia.',
            }
          },
          BUSINESS: {
            PLACEHOLDER: 'Fuente',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar quien provee la información.',
            }
          },
          BODY: {
            PLACEHOLDER: 'Detalle de la noticia',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'La información de la noticia es muy corta.',
              NOT_EMPTY: 'Es necesario especificar detalles de la noticia.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Añadir noticia',
          RETURN: 'Volver a listado de noticias',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'Noticia añadida',
          ADD_SUCCESS_MESSAGE: 'La información será actualizada.',
          ADD_UNSUCCESS_TITLE: 'Error al añadir noticia',
          ADD_UNSUCCESS_MESSAGE: 'La información será actualizada.',
        }
      },
      EDIT: {
        TITLE: 'Editar noticia',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Título de la noticia',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'El título de la noticia es muy corto.',
              NOT_EMPTY: 'Es necesario especificar el título de la noticia.',
            }
          },
          BUSINESS: {
            PLACEHOLDER: 'Fuente',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar quien provee la información.',
            }
          },
          BODY: {
            PLACEHOLDER: 'Detalle de la noticia',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'La información de la noticia es muy corta.',
              NOT_EMPTY: 'Es necesario especificar detalles de la noticia.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Actualizar información',
          RETURN: 'Volver a listado de noticias',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'Noticia editada correctamente',
          EDIT_SUCCESS_MESSAGE: 'La infrmación será actualizada.',
          EDIT_UNSUCCESS_TITLE: 'Error al actualizar información',
          EDIT_UNSUCCESS_MESSAGE: 'Verifica la información e intenta más tarde.',
        }
      },
      DETAIL: {
        TITLE: 'Detalles de noticia',
        ACTIONS: {
          EDIT: 'Update information',
          RETURN: 'Volver a noticias',
          PUBLISH: 'Publicar noticia en webapp',
          REMOVE: 'Eliminar noticia de webapp'
        },
        ALERTS: {
        }
      },
    },
    EVENTS: {
      INDEX: {
        TITLE: 'Gestionar eventos disponibles',
        TABLE: {
          ID: '#',
          TITLE: 'Nombre',
          PLACE: 'Lugar',
          DATE: 'Fecha',
          ACTIONS: 'Acciones',
          FILTER: 'Buscar',
          HELP_DETAIL: 'Detalle del evento',
          HELP_APPROVE: 'Aprobar evento',
          HELP_DELETE: 'Borrar evento',
          HELP_REVOKE: 'Eliminar evento de aplicación web',
        },
        ACTIONS: {
          ADD: 'Añadir evento',
          EXPORT: 'Exportar evento'
        },
        ALERTS: {
          APPROVE_MESSAGE: 'El evento será publicado en la aplicación web y será visible para los usuarios, ¿deseas continuar con la petición?',
          APPROVE: 'Aprobar evento',
          APPROVE_SUCCESS_TITLE: 'El evento se ha publicado correctamente',
          APPROVE_SUCCESS_MESSAGE: 'La información será actualizada a continuación',
          DISABLE_MESSAGE: 'El evento dejará de aparecer a los usuarios de la plataforma, ¿deseas continuar con la petición?',
          DISABLE: 'Retirar evento',
          DISABLE_SUCCESS_TITLE: 'Evento removido',
          DISABLE_SUCCESS_MESSAGE: 'Si desea volver a añadir el evento puede hacerlo desde la pestaña de eventos disponibles.',
          DELETE_MESSAGE: 'El evento será eliminado, ¿deseas continuar con la petición?',
          DELETE: 'Eliminar evento',
          DELETE_SUCCESS_TITLE: 'Evento eliminado correctamente',
          DELETE_SUCCESS_MESSAGE: 'La información será actualizada.',
        }
      },
      ADD: {
        TITLE: 'Agregar nuevo evento',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Nombre del evento',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'El nombre del evento es muy corto.',
              NOT_EMPTY: 'Es necesario especificar el nombre del evento.',
            }
          },
          PLACE: {
            PLACEHOLDER: 'Lugar del evento',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar donde será el evento.',
            }
          },
          BODY: {
            PLACEHOLDER: 'Detalle del evento',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'La información del evento es muy corta.',
              NOT_EMPTY: 'Es necesario especificar detalles del evento.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Añadir evento',
          RETURN: 'Volver a listado de eventos',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'Evento añadido',
          ADD_SUCCESS_MESSAGE: 'La información será actualizada',
          ADD_UNSUCCESS_TITLE: 'Error al añadir evento',
          ADD_UNSUCCESS_MESSAGE: 'Verifica la información e intenta nuevamente o más tarde.',
        }
      },
      EDIT: {
        TITLE: 'Edit user',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'First name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify the name.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Last name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify last names.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Email',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'You need to specify the email.',
              EMAIL_FORMAT: 'A valid email has not been entered.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'User type',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'You must specify the type of user.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Update information',
          RETURN: 'Back to users list',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'User edited',
          EDIT_SUCCESS_MESSAGE: 'The information will be updated',
          EDIT_UNSUCCESS_TITLE: 'User update unsuccess',
          EDIT_UNSUCCESS_MESSAGE: 'The information will be updated',
        }
      },
      DETAIL: {
        TITLE: 'Detalles del evento',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Nombre del evento',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'El nombre del evento es muy corto.',
              NOT_EMPTY: 'Es necesario especificar el nombre del evento.',
            }
          },
          PLACE: {
            PLACEHOLDER: 'Lugar del evento',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar donde será el evento.',
            }
          },
          DATE: {
            PLACEHOLDER: 'Fecha del evento',
          },
          BODY: {
            PLACEHOLDER: 'Detalle del evento',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'La información del evento es muy corta.',
              NOT_EMPTY: 'Es necesario especificar detalles del evento.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Editar evento',
          RETURN: 'Volver a eventos',
          PUBLISH: 'Publicar evento en webapp',
          REMOVE: 'Eliminar evento de webapp'
        },
        ALERTS: {
        }
      },
    },
    EVENTS_APPROVED: {
      INDEX: {
        TITLE: 'Gestionar eventos aprobados',
        TABLE: {
          ID: '#',
          TITLE: 'Nombre',
          PLACE: 'Lugar',
          DATE: 'Fecha',
          ACTIONS: 'Acciones',
          FILTER: 'Buscar',
          HELP_DISABLE: 'Disable user',
          HELP_ENABLE: 'Enable user',
        },
        ACTIONS: {
          ADD: 'Añadir evento',
        },
        ALERTS: {
          ENABLE_MESSAGE: 'The user will get access to the platform again, do you want to continue with the request?',
          ENABLE: 'Enable user',
          ENABLE_SUCCESS_TITLE: 'User enabled',
          ENABLE_SUCCESS_MESSAGE: 'The information will be updated',
          DISABLE_MESSAGE: 'The user loses access to the platform, do you want to continue with the request?',
          DISABLE: 'Disable user',
          DISABLE_SUCCESS_TITLE: 'User disabled',
          DISABLE_SUCCESS_MESSAGE: 'The information will be updated',
          DELETE_MESSAGE: 'The user will be removed from the system, do you want to continue with the request?',
          DELETE: 'Delete user',
          DELETE_SUCCESS_TITLE: 'User deleted',
          DELETE_SUCCESS_MESSAGE: 'The information will be updated',
        }
      },
      ADD: {
        TITLE: 'Agregar nuevo evento',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Nombre del evento',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'El nombre del evento es muy corto.',
              NOT_EMPTY: 'Es necesario especificar el nombre del evento.',
            }
          },
          PLACE: {
            PLACEHOLDER: 'Lugar del evento',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar donde será el evento.',
            }
          },
          BODY: {
            PLACEHOLDER: 'Detalle del evento',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'La información del evento es muy corta.',
              NOT_EMPTY: 'Es necesario especificar detalles del evento.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Añadir evento',
          RETURN: 'Volver a listado de eventos',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'Evento añadido',
          ADD_SUCCESS_MESSAGE: 'La información será actualizada',
          ADD_UNSUCCESS_TITLE: 'Error al añadir evento',
          ADD_UNSUCCESS_MESSAGE: 'Verifica la información e intenta nuevamente o más tarde.',
        }
      },
      EDIT: {
        TITLE: 'Edit user',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'First name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify the name.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Last name',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'It is necessary to specify last names.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Email',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'You need to specify the email.',
              EMAIL_FORMAT: 'A valid email has not been entered.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'User type',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'You must specify the type of user.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Update information',
          RETURN: 'Back to users list',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'User edited',
          EDIT_SUCCESS_MESSAGE: 'The information will be updated',
          EDIT_UNSUCCESS_TITLE: 'User update unsuccess',
          EDIT_UNSUCCESS_MESSAGE: 'The information will be updated',
        }
      },
      DETAIL: {
        TITLE: 'Detalles del evento',
        ACTIONS: {
          EDIT: 'Editar evento',
          RETURN: 'Volver a eventos',
          PUBLISH: 'Publicar evento en webapp',
          REMOVE: 'Eliminar evento de webapp'
        },
        ALERTS: {
        }
      },
    },
    USERS: {
      INDEX: {
        TITLE: 'Gestionar usuarios',
        TABLE: {
          ID: 'ID',
          NAME: 'Nombre',
          EMAIL: 'Correo',
          TYPE: 'Tipo de usuario',
          STATUS: 'Estatus',
          ACTIONS: 'Acciones',
          FILTER: 'Buscar',
          HELP_DISABLE: 'Desactivar usuario',
          HELP_ENABLE: 'Activar usuario',
        },
        ACTIONS: {
          ADD: 'Añadir usuario',
          EXPORT: 'Exportar usuarios'
        },
        ALERTS: {
          ENABLE_MESSAGE: 'El usuario obtendrá acceso nuevamente a la plataforma, ¿desea continuar con la solicitud?',
          ENABLE: 'Reactivar usuario',
          ENABLE_SUCCESS_TITLE: 'Usuario reactivado',
          ENABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DISABLE_MESSAGE: 'El usuario perderá el acceso a la plataforma, ¿desea continuar con la solicitud?',
          DISABLE: 'Desactivar usuario',
          DISABLE_SUCCESS_TITLE: 'Usuario desactivado',
          DISABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DELETE_MESSAGE: 'El usuario será eliminado del sistema, ¿desea continuar con la solicitud?',
          DELETE: 'Eliminar usuario',
          DELETE_SUCCESS_TITLE: 'Usuario eliminado',
          DELETE_SUCCESS_MESSAGE: 'La información será actualizada.',
        }
      },
      ADD: {
        TITLE: 'Agregar nuevo usuario',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Nombres',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar el nombre.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Apellidos',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar los apellidos.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Correo electrónico',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'Correo no cumple con la longitud mínima requerida.',
              NOT_EMPTY: 'Es necesario especificar el correo electrónico.',
              EMAIL_FORMAT: 'No se ha ingresado un correo válido.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'Tipo de usuario',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'Debes especificar el tipo de usuario.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Añadir usuario',
          RETURN: 'Volver a lista de usuarios',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'Usuario invitado',
          ADD_SUCCESS_MESSAGE: 'Se envió la invitación vía email al usuario.',
          ADD_UNSUCCESS_TITLE: 'Error al añadir usuario',
          ADD_UNSUCCESS_MESSAGE: 'Ocurrio un error al enviar la invitación, intenta más tarde.',
          ADD_UNSUCCESS_EMAIL_MESSAGE: 'El correo electrónico ya se encuentra en uso.',
          ENABLE_MESSAGE: 'The user will get access to the platform again, do you want to continue with the request?',
          ENABLE: 'Enable user',
          ENABLE_SUCCESS_TITLE: 'User enabled',
          ENABLE_SUCCESS_MESSAGE: 'The information will be updated',
          DISABLE_MESSAGE: 'The user loses access to the platform, do you want to continue with the request?',
          DISABLE: 'Disable user',
          DISABLE_SUCCESS_TITLE: 'User disabled',
          DISABLE_SUCCESS_MESSAGE: 'The information will be updated',
          DELETE_MESSAGE: 'The user will be removed from the system, do you want to continue with the request?',
          DELETE: 'Delete user',
          DELETE_SUCCESS_TITLE: 'User deleted',
          DELETE_SUCCESS_MESSAGE: 'The information will be updated',
        }
      },
      EDIT: {
        TITLE: 'Editar usuario',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Nombres',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario ingresar el nombre.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Apellidos',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario ingresar los apellidos.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Correo',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'El correo es necesario.',
              EMAIL_FORMAT: 'El formato del correo electrónico no es válido.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'Tipo de usuario',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'You must specify the type of user.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Actualizar usuario',
          RETURN: 'Volver a lista de usuarios',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'User edited',
          EDIT_SUCCESS_MESSAGE: 'The information will be updated',
          EDIT_UNSUCCESS_TITLE: 'User update unsuccess',
          EDIT_UNSUCCESS_MESSAGE: 'The information will be updated',
        }
      },
    },
    DOCTORS: {
      INDEX: {
        TITLE: 'Gestionar doctores',
        TABLE: {
          ID: 'ID',
          NAME: 'Nombre',
          EMAIL: 'Correo',
          TYPE: 'Especialidad',
          STATUS: 'Estatus',
          ACTIONS: 'Acciones',
          FILTER: 'Buscar',
          HELP_DISABLE: 'Desactivar doctor',
          HELP_ENABLE: 'Activar doctor',
        },
        ACTIONS: {
          ADD: 'Añadir doctor',
          EXPORT: 'Exportar doctor'
        },
        ALERTS: {
          ENABLE_MESSAGE: 'El doctor obtendrá acceso nuevamente a la plataforma, ¿desea continuar con la solicitud?',
          ENABLE: 'Reactivar doctor',
          ENABLE_SUCCESS_TITLE: 'Doctor reactivado',
          ENABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DISABLE_MESSAGE: 'El doctor perderá el acceso a la plataforma, ¿desea continuar con la solicitud?',
          DISABLE: 'Desactivar doctor',
          DISABLE_SUCCESS_TITLE: 'Doctor desactivado',
          DISABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DELETE_MESSAGE: 'El doctor será eliminado del sistema, ¿desea continuar con la solicitud?',
          DELETE: 'Eliminar doctor',
          DELETE_SUCCESS_TITLE: 'Doctor eliminado',
          DELETE_SUCCESS_MESSAGE: 'La información será actualizada.',
        }
      },
      ADD: {
        TITLE: 'Agregar nuevo doctor',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Nombres',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar el nombre.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Apellidos',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar los apellidos.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Correo electrónico',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'Correo no cumple con la longitud mínima requerida.',
              NOT_EMPTY: 'Es necesario especificar el correo electrónico.',
              EMAIL_FORMAT: 'No se ha ingresado un correo válido.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'Tipo de especialidad',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'Debes especificar el tipo de especialidad.',
            }
          },
        },
        ACTIONS: {
          ADD: 'Añadir doctor',
          RETURN: 'Volver a lista de doctores',
        },
        ALERTS: {
          ADD_SUCCESS_TITLE: 'Usuario invitado',
          ADD_SUCCESS_MESSAGE: 'Se envió la invitación vía email al usuario.',
          ADD_UNSUCCESS_TITLE: 'Error al añadir usuario',
          ADD_UNSUCCESS_MESSAGE: 'Ocurrio un error al enviar la invitación, intenta más tarde.',
          ADD_UNSUCCESS_EMAIL_MESSAGE: 'El correo electrónico ya se encuentra en uso.',
          ENABLE_MESSAGE: 'The user will get access to the platform again, do you want to continue with the request?',
          ENABLE: 'Enable user',
          ENABLE_SUCCESS_TITLE: 'User enabled',
          ENABLE_SUCCESS_MESSAGE: 'The information will be updated',
          DISABLE_MESSAGE: 'The user loses access to the platform, do you want to continue with the request?',
          DISABLE: 'Disable user',
          DISABLE_SUCCESS_TITLE: 'User disabled',
          DISABLE_SUCCESS_MESSAGE: 'The information will be updated',
          DELETE_MESSAGE: 'The user will be removed from the system, do you want to continue with the request?',
          DELETE: 'Delete user',
          DELETE_SUCCESS_TITLE: 'User deleted',
          DELETE_SUCCESS_MESSAGE: 'The information will be updated',
        }
      },
      EDIT: {
        TITLE: 'Editar doctor',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Nombres',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario ingresar el nombre.',
            }
          },
          LAST_NAME: {
            PLACEHOLDER: 'Apellidos',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario ingresar los apellidos.',
            }
          },
          EMAIL: {
            PLACEHOLDER: 'Correo',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'El correo es necesario.',
              EMAIL_FORMAT: 'El formato del correo electrónico no es válido.',
            }
          },
          TYPE: {
            PLACEHOLDER: 'Tipo de especialidad',
            HELP_TEXT: '',
            ERRORS: {
              NOT_EMPTY: 'Debes especificar el tipo de especialidad.',
            }
          },
        },
        ACTIONS: {
          EDIT: 'Actualizar doctor',
          RETURN: 'Volver a lista de doctores',
        },
        ALERTS: {
          EDIT_SUCCESS_TITLE: 'User edited',
          EDIT_SUCCESS_MESSAGE: 'The information will be updated',
          EDIT_UNSUCCESS_TITLE: 'User update unsuccess',
          EDIT_UNSUCCESS_MESSAGE: 'The information will be updated',
        }
      },
    },
    CATALOGS_LIST: {
      PROJECT_STATUS: {
        INDEX: {
          TITLE: 'Administrar estatus de proyectos',
          TABLE: {
            ID: '#',
            NAME: 'Nombre',
            ACTIONS: 'Acciones',
            FILTER: 'Buscar',
          },
          ACTIONS: {
            ADD: 'Añadir estatus de proyecto',
          },
          ALERTS: {
            DELETE_MESSAGE: 'El estatus de proyecto sera eliminado del sistema, ¿deseas continuar con la solicitud?',
            DELETE: 'Eliminar estatus',
            DELETE_SUCCESS_TITLE: 'Estatus de proyecto eliminado',
            DELETE_SUCCESS_MESSAGE: 'La información será actualizada',
          }
        },
        ADD: {
          TITLE: 'Añadir nuevo estatus de proyecto',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Estatus',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'Es necesario especificar el estatus.',
              }
            },
          },
          ACTIONS: {
            ADD: 'Añadir estatus',
            RETURN: 'Volver a lista de estatus',
          },
          ALERTS: {
            ADD_SUCCESS_TITLE: 'Estatus añadido exitosamente',
            ADD_SUCCESS_MESSAGE: 'La información será actualizada',
            ADD_UNSUCCESS_TITLE: 'No fue posible añadir el estatus',
            ADD_UNSUCCESS_MESSAGE: 'Intenta nuevamente',
          }
        },
        EDIT: {
          TITLE: 'Editar estatus de proyectos',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Estatus',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'Es necesario especificar el estatus.',
              }
            },
          },
          ACTIONS: {
            EDIT: 'Actualizar información',
            RETURN: 'Volver a lista de estatus',
          },
          ALERTS: {
            ADD_SUCCESS_TITLE: 'Estatus actualizado exitosamente',
            ADD_SUCCESS_MESSAGE: 'La información será actualizada',
            ADD_UNSUCCESS_TITLE: 'No fue posible editar el estatus',
            ADD_UNSUCCESS_MESSAGE: 'Intenta nuevamente',
          }
        },
      },
      PROJECT_CATEGORY: {
        INDEX: {
          TITLE: 'Administrar categorías de proyectos',
          TABLE: {
            ID: '#',
            NAME: 'Nombre',
            ACTIONS: 'Acciones',
            FILTER: 'Buscar',
          },
          ACTIONS: {
            ADD: 'Añadir categoría de proyecto',
          },
          ALERTS: {
            DELETE_MESSAGE: 'La categoría de proyecto sera eliminado del sistema, ¿deseas continuar con la solicitud?',
            DELETE: 'Eliminar categoría',
            DELETE_SUCCESS_TITLE: 'Categoría de proyecto eliminado',
            DELETE_SUCCESS_MESSAGE: 'La información será actualizada',
          }
        },
        ADD: {
          TITLE: 'Añadir nueva categoría de proyecto',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Categoría',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'Es necesario especificar la categoría.',
              }
            },
          },
          ACTIONS: {
            ADD: 'Añadir categoría',
            RETURN: 'Volver a lista de categorías',
          },
          ALERTS: {
            ADD_SUCCESS_TITLE: 'Categoría añadida',
            ADD_SUCCESS_MESSAGE: 'La información será actualizada.',
            ADD_UNSUCCESS_TITLE: 'La categoría no fue añadida',
            ADD_UNSUCCESS_MESSAGE: 'Intenta nuevamente',
          }
        },
        EDIT: {
          TITLE: 'Editar categoría de proyecto',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Categoría',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'Es necesario especificar la categoría.',
              }
            },
          },
          ACTIONS: {
            EDIT: 'Actualizar información',
            RETURN: 'Volver a lista de categorías',
          },
          ALERTS: {
            EDIT_SUCCESS_TITLE: 'Categoría editada',
            EDIT_SUCCESS_MESSAGE: 'La información será actualizada.',
            EDIT_UNSUCCESS_TITLE: 'La categoría no fue editada',
            EDIT_UNSUCCESS_MESSAGE: 'Intenta nuevamente',
          }
        },
      },
      PROJECT_SECTOR: {
        INDEX: {
          TITLE: 'Administrar sectores de proyectos',
          TABLE: {
            ID: '#',
            NAME: 'Nombre',
            ACTIONS: 'Acciones',
            FILTER: 'Buscar',
          },
          ACTIONS: {
            ADD: 'Añadir sector de proyecto',
          },
          ALERTS: {
            DELETE_MESSAGE: 'El sector de proyecto sera eliminado del sistema, ¿deseas continuar con la solicitud?',
            DELETE: 'Eliminar sector',
            DELETE_SUCCESS_TITLE: 'Sector de proyecto eliminado',
            DELETE_SUCCESS_MESSAGE: 'La información será actualizada',
          }
        },
        ADD: {
          TITLE: 'Añadir nuevo sector de proyecto',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Sector',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'Es necesario especificar el sector.',
              }
            },
          },
          ACTIONS: {
            ADD: 'Añadir sector',
            RETURN: 'Volver a lista de sectores',
          },
          ALERTS: {
            ADD_SUCCESS_TITLE: 'Sector añadida',
            ADD_SUCCESS_MESSAGE: 'La información será actualizada.',
            ADD_UNSUCCESS_TITLE: 'El sector no fue añadida',
            ADD_UNSUCCESS_MESSAGE: 'Intenta nuevamente',
          }
        },
        EDIT: {
          TITLE: 'Editar sector de proyecto',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Sector',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'Es necesario especificar el sector.',
              }
            },
          },
          ACTIONS: {
            EDIT: 'Actualizar información',
            RETURN: 'Volver a lista de sectores',
          },
          ALERTS: {
            EDIT_SUCCESS_TITLE: 'Sector editado',
            EDIT_SUCCESS_MESSAGE: 'La información será actualizada.',
            EDIT_UNSUCCESS_TITLE: 'El sector no fue editada',
            EDIT_UNSUCCESS_MESSAGE: 'Intenta nuevamente',
          }
        },
      },
      PROJECT_MARKET: {
        INDEX: {
          TITLE: 'Administrar mercados de proyectos',
          TABLE: {
            ID: '#',
            NAME: 'Nombre',
            ACTIONS: 'Acciones',
            FILTER: 'Buscar',
          },
          ACTIONS: {
            ADD: 'Añadir mercado de proyecto',
          },
          ALERTS: {
            DELETE_MESSAGE: 'El mercado de proyecto sera eliminado del sistema, ¿deseas continuar con la solicitud?',
            DELETE: 'Eliminar mercado',
            DELETE_SUCCESS_TITLE: 'Mercado de proyecto eliminado',
            DELETE_SUCCESS_MESSAGE: 'La información será actualizada',
          }
        },
        ADD: {
          TITLE: 'Añadir nuevo mercado de proyecto',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Mercado',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'Es necesario especificar el mercado.',
              }
            },
          },
          ACTIONS: {
            ADD: 'Añadir mercado',
            RETURN: 'Volver a lista de mercados',
          },
          ALERTS: {
            ADD_SUCCESS_TITLE: 'Mercado añadido',
            ADD_SUCCESS_MESSAGE: 'La información será actualizada.',
            ADD_UNSUCCESS_TITLE: 'El mercado no fue añadido',
            ADD_UNSUCCESS_MESSAGE: 'Intenta nuevamente',
          }
        },
        EDIT: {
          TITLE: 'Editar mercado de proyecto',
          INPUTS: {
            NAME: {
              PLACEHOLDER: 'Mercado',
              HELP_TEXT: '',
              ERRORS: {
                MIN_LENGTH: '',
                NOT_EMPTY: 'Es necesario especificar el mercado.',
              }
            },
          },
          ACTIONS: {
            EDIT: 'Actualizar información',
            RETURN: 'Volver a lista de mercado',
          },
          ALERTS: {
            EDIT_SUCCESS_TITLE: 'Mercado editado',
            EDIT_SUCCESS_MESSAGE: 'La información será actualizada.',
            EDIT_UNSUCCESS_TITLE: 'El mercado no fue editada',
            EDIT_UNSUCCESS_MESSAGE: 'Intenta nuevamente',
          }
        },
      }
    },
    PROFILE: {
      BUTTONS: {
        RESET: 'Reiniciar cambios',
        SAVE: 'Actualizar información',
        SAVE_EXIT: 'Actualizar datos y volver al dashboard',
        SAVE_CONTINUE: 'Actualizar información'
      },
      DETAIL: {
        TITLE: 'Actulizar información de usuario',
        INPUTS: {
          FIRST_NAME: {
            PLACEHOLDER: 'Nombres',
            EMPTY_FIELD: 'Please enter the first name.'
          },
          LAST_NAME: {
            PLACEHOLDER: 'Apellidos',
            EMPTY_FIELD: 'Please enter the last name.'
          },
          EMAIL: {
            PLACEHOLDER: 'Correo electrónico',
            EMPTY_FIELD: 'Please enter the email.'
          },
          TYPE: {
            PLACEHOLDER: 'Privilegio',
            EMPTY_FIELD: 'Privilege for your user.'
          },
          CREATED: {
            PLACEHOLDER: 'Fecha de creación',
            EMPTY_FIELD: 'Fecha de registro a la plataforma'
          }
        },
        ALERTS: {
          SERVER_ERROR_TITLE: 'Ha ocurrido un error',
          SERVER_ERROR_MSJ: 'Hay problemas con la comunicación al servidor, intenta más tarde.',
          ERROR_IN_UPDATE_TITLE: 'Contraseña actual inválida',
          ERROR_IN_UPDATE_MSJ: 'La contraseña actual es incorrecta, verifica la información e intenta nuevamente.',
          SUCCESS_TITLE: 'Tu información se ha actualizado',
          SUCCESS_MSJ: 'Los cambios se verán reflejados a continuación.'
        }
      },
      CHANGE_PASSWORD: {
        TITLE: 'Actualizar contraseña',
        INPUTS: {
          PASSWORD: {
            PLACEHOLDER: 'Contraseña actual',
            EMPTY_FIELD: 'No se ha ingresado la contraseña actual.'
          },
          NEW_PASSWORD: {
            PLACEHOLDER: 'Nueva contraseña',
            EMPTY_FIELD: 'Es necesario ingresar la nueva contraseña.',
            MIN_LENGTH: 'La contraseña debe tener por lo menos 8 caracteres alfanuméricos.'
          },
          NEW_PASSWORD_CHECK: {
            PLACEHOLDER: 'Repetir nueva contraseña',
            EMPTY_FIELD: 'Es necesario ingresar la nueva contraseña.',
            MIN_LENGTH: 'La contraseña debe tener por lo menos 8 caracteres alfanuméricos.'
          },
        },
        ALERTS: {
          PASSWORDS_MATCH_TITLE: 'Verifica la información',
          PASSWORDS_MATCH_MSJ: 'Las contraseñas no coinciden, verifica e intenta nuevamente.',
          SERVER_ERROR_TITLE: 'Ha ocurrido un error',
          SERVER_ERROR_MSJ: 'Hay problemas con la comunicación al servidor, intenta más tarde.',
          PASSWORD_INVALID_TITLE: 'Contraseña actual inválida',
          PASSWORD_INVALID_MSJ: 'La contraseña actual es incorrecta, verifica la información e intenta nuevamente.',
          SUCCESS_TITLE: 'Se ha actualizado la contraseña',
          SUCCESS_MSJ: 'La contraseña se ha actualizado, será necesario ingresarla la próxima vez que ingreses.'
        }
      }
    },
    PLANS: {
      INDEX: {
        TITLE: 'Gestionar planes de suscripción',
        TABLE: {
          ID: '#',
          NAME: 'Nombre',
          AMOUNT: 'Costo',
          SUBSCRIBED_USERS: 'Usuarios suscritos',
          DURATION: 'Duración de suscripción',
          STATUS: 'Estatus',
          ACTIONS: 'Acciones',
          FILTER: 'Buscar',
          HELP_DISABLE: 'Desactivar plan',
          HELP_ENABLE: 'Reactivar plan',
        },
        ACTIONS: {
          ADD: 'Añadir nuevo plan',
        },
        ALERTS: {
          EXIST_PLAN: 'El plan ya existe, intenta con un nombre diferente.',
          AMOUNT_MSJ: 'El monto debe ser menor o igual a 3000.',
          AMOUNT_TITLE: 'Ha ocurrido un error',
          ENABLE_MESSAGE: 'El usuario obtendrá acceso nuevamente a la plataforma, ¿desea continuar con la solicitud?',
          ENABLE: 'Reactivar usuario',
          ENABLE_SUCCESS_TITLE: 'Usuario reactivado',
          ENABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DISABLE_MESSAGE: 'El usuario perderá el acceso a la plataforma, ¿desea continuar con la solicitud?',
          DISABLE: 'Desactivar usuario',
          DISABLE_SUCCESS_TITLE: 'Usuario desactivado',
          DISABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DELETE_MESSAGE: 'El usuario será eliminado del sistema, ¿desea continuar con la solicitud?',
          DELETE: 'Eliminar usuario',
          DELETE_SUCCESS_TITLE: 'Usuario eliminado',
          DELETE_SUCCESS_MESSAGE: 'La información será actualizada.',
        }
      },
      ADD: {
        TITLE: 'Agregar nuevo plan',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Nombre del plan',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'El nombre del plan es muy corto.',
              NOT_EMPTY: 'Es necesario especificar el nombre para el plan.',
            }
          },
          CURRENCY: {
            PLACEHOLDER: 'Divisa a utilizar',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar la divisa para el plan.',
            }
          },
          AMOUNT: {
            PLACEHOLDER: 'Costo de plan',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar el costo que tendrá el plan.',
              NOT_NUMBER: 'El valor debe ser una número entero ó decimal.',
            }
          },
          FRECUENCY: {
            PLACEHOLDER: 'Frecuencia de cobro',
            HELP_TEXT: '',
            PERIOD: 'Mes',
            PERIODS: 'Meses',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar la frecuencia de cobro del plan.',
            }
          },
          DURATION: {
            PLACEHOLDER: 'Duración de la suscripción',
            DESC_INIT: 'La suscripción se cobrará',
            DESC_END: 'veces.',
          },
        },
        ACTIONS: {
          ADD: 'Añadir plan',
          RETURN: 'Volver a listado de planes',
        },
        ALERTS: {
          EXIST_PLAN: 'El plan ya existe, intenta con un nombre diferente.',
          AMOUNT_MSJ: 'El monto debe ser menor o igual a 3000.',
          AMOUNT_TITLE: 'Ha ocurrido un error',
          ADD_SUCCESS_TITLE: 'Plan creado correctamente',
          ADD_SUCCESS_MESSAGE: 'La información será actualizada',
          ADD_UNSUCCESS_TITLE: 'Ha ocurrido un error al crear el plan',
          ADD_UNSUCCESS_MESSAGE: 'Verifica la información e intenta más tarde',
        }
      },
      DETAIL: {
        TITLE: 'Información del plan',
        ACTIONS: {
          RETURN: 'Volver a planes'
        },
        STATUS: 'Etapa',
        SECTOR: 'Sector',
        MARKET: 'Mercado',
        LEFT_DAYS: 'Días restantes',
        DESCRIPTION: 'Descripción completa',
        VALUATION: 'Valoración de la empresa',
        PERCENT: 'Porcentaje ofrecido',
        ESTIMATED_RETURN: 'Retorno estimado',
        ESTIMATED_END: 'Salida estimada',
        INVESTMENT: 'Inversión'
      },
    },

    SPECIALTIES: {
      INDEX: {
        TITLE: 'Gestionar especialidades',
        TABLE: {
          ID: '#',
          NAME: 'Nombre',
          AMOUNT: 'Costo',
          SUBSCRIBED_USERS: 'Usuarios suscritos',
          DURATION: 'Duración de suscripción',
          STATUS: 'Estatus',
          ACTIONS: 'Acciones',
          FILTER: 'Buscar',
          HELP_DISABLE: 'Desactivar especialidad',
          HELP_ENABLE: 'Reactivar especialidad',
        },
        ACTIONS: {
          ADD: 'Añadir nueva especialidad',
        },
        ALERTS: {
          EXIST_PLAN: 'El plan ya existe, intenta con un nombre diferente.',
          AMOUNT_MSJ: 'El monto debe ser menor o igual a 3000.',
          AMOUNT_TITLE: 'Ha ocurrido un error',
          ENABLE_MESSAGE: 'El usuario obtendrá acceso nuevamente a la plataforma, ¿desea continuar con la solicitud?',
          ENABLE: 'Reactivar usuario',
          ENABLE_SUCCESS_TITLE: 'Usuario reactivado',
          ENABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DISABLE_MESSAGE: 'El usuario perderá el acceso a la plataforma, ¿desea continuar con la solicitud?',
          DISABLE: 'Desactivar usuario',
          DISABLE_SUCCESS_TITLE: 'Usuario desactivado',
          DISABLE_SUCCESS_MESSAGE: 'La información será actualizada.',
          DELETE_MESSAGE: 'El usuario será eliminado del sistema, ¿desea continuar con la solicitud?',
          DELETE: 'Eliminar usuario',
          DELETE_SUCCESS_TITLE: 'Usuario eliminado',
          DELETE_SUCCESS_MESSAGE: 'La información será actualizada.',
        }
      },
      ADD: {
        TITLE: 'Agregar nueva especialidad',
        INPUTS: {
          NAME: {
            PLACEHOLDER: 'Nombre del plan',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: 'El nombre del plan es muy corto.',
              NOT_EMPTY: 'Es necesario especificar el nombre para el plan.',
            }
          },
          CURRENCY: {
            PLACEHOLDER: 'Divisa a utilizar',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar la divisa para el plan.',
            }
          },
          AMOUNT: {
            PLACEHOLDER: 'Costo de plan',
            HELP_TEXT: '',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar el costo que tendrá el plan.',
              NOT_NUMBER: 'El valor debe ser una número entero ó decimal.',
            }
          },
          FRECUENCY: {
            PLACEHOLDER: 'Frecuencia de cobro',
            HELP_TEXT: '',
            PERIOD: 'Mes',
            PERIODS: 'Meses',
            ERRORS: {
              MIN_LENGTH: '',
              NOT_EMPTY: 'Es necesario especificar la frecuencia de cobro del plan.',
            }
          },
          DURATION: {
            PLACEHOLDER: 'Duración de la suscripción',
            DESC_INIT: 'La suscripción se cobrará',
            DESC_END: 'veces.',
          },
        },
        ACTIONS: {
          ADD: 'Añadir plan',
          RETURN: 'Volver a listado de planes',
        },
        ALERTS: {
          EXIST_PLAN: 'El plan ya existe, intenta con un nombre diferente.',
          AMOUNT_MSJ: 'El monto debe ser menor o igual a 3000.',
          AMOUNT_TITLE: 'Ha ocurrido un error',
          ADD_SUCCESS_TITLE: 'Plan creado correctamente',
          ADD_SUCCESS_MESSAGE: 'La información será actualizada',
          ADD_UNSUCCESS_TITLE: 'Ha ocurrido un error al crear el plan',
          ADD_UNSUCCESS_MESSAGE: 'Verifica la información e intenta más tarde',
        }
      },
      DETAIL: {
        TITLE: 'Información del plan',
        ACTIONS: {
          RETURN: 'Volver a planes'
        },
        STATUS: 'Etapa',
        SECTOR: 'Sector',
        MARKET: 'Mercado',
        LEFT_DAYS: 'Días restantes',
        DESCRIPTION: 'Descripción completa',
        VALUATION: 'Valoración de la empresa',
        PERCENT: 'Porcentaje ofrecido',
        ESTIMATED_RETURN: 'Retorno estimado',
        ESTIMATED_END: 'Salida estimada',
        INVESTMENT: 'Inversión'
      },
    },



  }
};
