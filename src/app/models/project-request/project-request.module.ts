import { AdminUserModule } from '../admin-user/admin-user.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectModule } from '../project/project.module';
import { UserModule } from '../user/user.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ProjectRequestModule {
  projectRequestID: number;
  projectRequestUser: UserModule;
  projectRequestInfo: ProjectModule;
  projectRequestStatus: number;
  projectRequestComment: string;
  projectRequestAdminUser: AdminUserModule;
  projectRequestUpdated: Date;
  projectRequestCreated: Date;
  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
