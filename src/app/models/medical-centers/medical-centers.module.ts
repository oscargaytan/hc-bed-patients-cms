import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionModule } from '../subscription/subscription.module';
import { StateModule } from './../state/state.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class MedicalCentersModule {
  medicalCenterID: number;
  medicalCenterName: string;
  medicalCenterStatus?: number;
  medicalCenterAddress: string;
  medicalCenterPhone: number;
  medicalCenterCreated?: Date;
  medicalCenterUpdated?: Date;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}