import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
	declarations: [],
	imports: [
		CommonModule
	]
})
export class ProjectCategoryModule {
	categoryID?: number;
	categoryName: string;
	categoryUpdated?: Date;
	categoryCreated?: Date;
	deserialize(input: any) {
		Object.assign(this, input);
		return this;
	}
}
