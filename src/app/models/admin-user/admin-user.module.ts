import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
	declarations: [],
	imports: [
		CommonModule
	]
})
export class AdminUserModule {
	adminID?: number;
	adminName: string;
	adminLName: string;
	adminEmail: string;
	adminType: number;
	adminStatus?: number;
	adminCreated?: Date;
	deserialize(input: any) {
		Object.assign(this, input);
		return this;
	}
}
