import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
	declarations: [],
	imports: [
		CommonModule
	]
})
export class ProjectSectorModule {
	sectorID?: number;
	sectorName: string;
	sectorUpdated?: Date;
	sectorCreated?: Date;
	deserialize(input: any) {
		Object.assign(this, input);
		return this;
	}
}
