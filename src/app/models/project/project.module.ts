import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserModule } from '../user/user.module';
import { ProjectStatusModule } from '../project-status/project-status.module';
import { ProjectSectorModule } from '../project-sector/project-sector.module';
import { ProjectCategoryModule } from '../project-category/project-category.module';
import { ProjectMarketModule } from '../project-market/project-market.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ProjectModule {
  projectID?: number;
  projectName: string;
  projectDesc: string;
  projectFDesc?: string;
  projectImages?: Array<string>;
  projectDate: Date;
  projectInvestment?: string;
  projectValuation?: string;
  projectPercent?: number;
  projectEstimatedEnd?: string;
  projectEstimatedReturn?: string;
  projectUser: UserModule;
  projectStatus: ProjectStatusModule;
  projectCategories?: ProjectCategoryModule;
  projectMarket?: ProjectMarketModule;
  projectSector?: ProjectSectorModule;
  projectActive?: boolean;
  projectUpdated?: Date;
  projectCreated?: Date;
  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
