import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class AddNewModule { 	
	newTitle: string;
	newDate?: Date;
	newBody: string;
	newImage: Array<string>;
	newBusiness: string;
	deserialize(input: any) {
		Object.assign(this, input);
		return this;
	}
}