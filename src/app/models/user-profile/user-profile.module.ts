import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class UserProfileModule {
  userID?: number;
  userName: string;
  userLastName: string;
  userEmail: string;
  userType: number;
  userCreated: Date;
  userCreatedString?: string;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
