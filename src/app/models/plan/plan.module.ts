import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionModule } from '../subscription/subscription.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class PlanModule {
  planID?: number;
  planInternalID?: string;
  planParent?: PlanModule;
  planName: string;
  planAmount: number;
  planCurrency: string;
  planInterval: string;
  planFrecuency: number;
  planExpiredCount: number;
  planTrialDays: number;
  planSubscriptions?: Array<SubscriptionModule>;
  planCreated?: Date;
  planUpdated?: Date;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
