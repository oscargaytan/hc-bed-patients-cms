import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
	declarations: [],
	imports: [
		CommonModule
	]
})
export class ResponseModule {
	statusCode: number;
	statusMessage: string;
	resultset: any;

	deserialize(input: any) {
		Object.assign(this, input);
		return this;
	}
}
