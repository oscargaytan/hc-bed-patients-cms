import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class AddProjectRequestModule {
  projectRequestID: number;
  projectRequestComment: string;
  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
