import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserModule } from '../user/user.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class SubscriptionModule {
  subscriptionID: number;
  subscriptionUser: UserModule;
  subscriptionStart: number;
  subscriptionBillingCircleStart: number;
  subscriptionBillingCircleEnd: number;
  subscriptionConektaChargeID: string;
  subscriptionConektaCustomerID: string;
  subscriptionConektaCardID: string;
  subscriptionStatus: string;
  subscriptionCreated: Date;
  subscriptionUpdated: Date;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
