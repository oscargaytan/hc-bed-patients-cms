import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class AddEventModule { 
  eventTitle: string;
	eventDate?: Date;
	eventBody: string;
	eventImages: Array<string>;
	eventPlace: string;
	deserialize(input: any) {
		Object.assign(this, input);
		return this;
	}
}