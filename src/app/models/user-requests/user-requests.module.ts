import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
	declarations: [],
	imports: [
		CommonModule
	]
})
export class UserRequestsModule {
	userRequestID: number;
	userRequestEmail: string;
	userRequestUrl: string;
	userRequestType: number;
	userRequestStatus: number;
	userRequestUpdated: Date;
	userRequestCreated: Date;
	deserialize(input: any) {
		Object.assign(this, input);
		return this;
	}
}
