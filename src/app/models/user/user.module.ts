import { StateModule } from './../state/state.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class UserModule {
  userID: number;
  userName: string;
  userLName: string;
  userEmail: string;
  userPhone?: string;
  userType: number;
  userStatus?: number;
  userState?: StateModule;
  userSpecialty?: number;
  userCreated?: Date;
  deserialize?(input: any) {
    Object.assign(this, input);
    return this;
  }
}
