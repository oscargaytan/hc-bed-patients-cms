import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionModule } from '../subscription/subscription.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class Doctors2Module {
  doctors2ID?: number;
  doctors2InternalID?: string;
  doctors2Parent?: Doctors2Module;
  doctors2Name: string;
  doctors2LName: string;
  doctors2Specialty: number;
  doctors2Currency: string;
  doctors2Interval: string;
  doctors2Frecuency: number;
  doctors2ExpiredCount: number;
  doctors2TrialDays: number;
  doctors2Subscriptions?: Array<SubscriptionModule>;
  doctors2Status: number;
  doctors2Created?: Date;
  doctors2Updated?: Date;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}