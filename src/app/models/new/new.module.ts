import { AdminUserModule } from '../admin-user/admin-user.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class NewModule {
  newID?: number;
  newTitle: string;
  newDate: Date;
  newBody: string;
  newImage: Array<string>;
  newCmsUser: AdminUserModule;
  newBusiness: AdminUserModule;
  newStatus: number;
  newExternalID?: string;
  newUpdated?: Date;
  newCreated?: Date;
  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
