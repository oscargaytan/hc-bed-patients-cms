import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminUserModule } from '../admin-user/admin-user.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class EventModule {
  eventID?: number;
  eventName: string;
  eventDate: Date;
  eventBody: string;
  eventImage: Array<string>;
  eventCmsUser: AdminUserModule;
  eventPlace: string;
  eventStatus: number;
  eventExternalID?: string;
  eventUpdated?: Date;
  eventCreated?: Date;
  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
