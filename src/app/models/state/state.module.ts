import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryModule } from '../country/country.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class StateModule {
  stateID: number;
  stateName: string;
  stateShortName: string;
  stateCountry: CountryModule;
  stateActive: boolean;
  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
