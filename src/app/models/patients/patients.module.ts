import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionModule } from '../subscription/subscription.module';
import { StateModule } from './../state/state.module';

@NgModule({
    declarations: [],
    imports: [
      CommonModule
    ]
  })
  export class PatientModule {
    patientID: number;
  
    patientName: string;
    patientLName: string;
    patientEmail: string;
    patientPhone: number;
    patientState: string;
    patientBloodType: string;
    patientAllergies: string;
    patientGender: number;
    patientStatus?: number;
    patientCreated?: Date;
    patientUpdated?: Date;
  
    deserialize(input: any) {
      Object.assign(this, input);
      return this;
    }
  }