import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionModule } from '../subscription/subscription.module';
import { StateModule } from './../state/state.module';

@NgModule({
    declarations: [],
    imports: [
      CommonModule
    ]
  })
  export class ConsultationModule {
    consultationID: number;
  
    consultationPriority: string;
    patientComment: string;
    patientHeight: number;
    patientWeight: number;
    patientBloodPressure: string;
    consultationStatus: number;
    consultationPin: number;
    medicalCenterCreated?: Date;
    medicalCenterUpdated?: Date;
  
    deserialize(input: any) {
      Object.assign(this, input);
      return this;
    }
  }