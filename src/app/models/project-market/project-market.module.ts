import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
	declarations: [],
	imports: [
		CommonModule
	]
})
export class ProjectMarketModule {
	marketID?: number;
	marketName: string;
	marketUpdated?: Date;
	marketCreated?: Date;
	deserialize(input: any) {
		Object.assign(this, input);
		return this;
	}
}
