import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionModule } from '../subscription/subscription.module';
import { StateModule } from './../state/state.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class SpecialtiesModule {
  specialtiesID: number;

  specialtiesName: string;
  specialtiesStatus?: number;
  specialtiesCreated?: Date;
  specialtiesUpdated?: Date;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}