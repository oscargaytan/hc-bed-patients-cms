import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
	declarations: [],
	imports: [
		CommonModule
	]
})
export class ProjectStatusModule {
	statusID?: number;
	statusName: string;
	statusUpdated?: Date;
	statusCreated?: Date;
	deserialize(input: any) {
		Object.assign(this, input);
		return this;
	}
}
